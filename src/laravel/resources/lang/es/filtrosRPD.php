<?php

return [
	'filtrosRPD'=>[
		'folioCBP'       =>'Folio CBP'
		,'folioFIPEDE'   =>'Folio FIPEDE'
		,'folioLocatel'  =>'Folio LOCATEL'
		,'nombrePersona' =>'Nombre persona desaparecida'
	]
];