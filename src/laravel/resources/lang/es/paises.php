<?php

return  [
  'countries' => 
   [
    0 => 
     [
      'id' => 144,
      'name' => 'Afganistán',
    ],
    1 => 
     [
      'id' => 114,
      'name' => 'Albania',
    ],
    2 => 
     [
      'id' => 18,
      'name' => 'Alemania',
    ],
    3 => 
     [
      'id' => 98,
      'name' => 'Algeria',
    ],
    4 => 
     [
      'id' => 145,
      'name' => 'Andorra',
    ],
    5 => 
     [
      'id' => 119,
      'name' => 'Angola',
    ],
    6 => 
     [
      'id' => 4,
      'name' => 'Anguilla',
    ],
    7 => 
     [
      'id' => 147,
      'name' => 'Antigua y Barbuda',
    ],
    8 => 
     [
      'id' => 207,
      'name' => 'Antillas Holandesas',
    ],
    9 => 
     [
      'id' => 91,
      'name' => 'Arabia Saudita',
    ],
    10 => 
     [
      'id' => 5,
      'name' => 'Argentina',
    ],
    11 => 
     [
      'id' => 6,
      'name' => 'Armenia',
    ],
    12 => 
     [
      'id' => 142,
      'name' => 'Aruba',
    ],
    13 => 
     [
      'id' => 1,
      'name' => 'Australia',
    ],
    14 => 
     [
      'id' => 2,
      'name' => 'Austria',
    ],
    15 => 
     [
      'id' => 3,
      'name' => 'Azerbaiyán',
    ],
    16 => 
     [
      'id' => 80,
      'name' => 'Bahamas',
    ],
    17 => 
     [
      'id' => 127,
      'name' => 'Bahrein',
    ],
    18 => 
     [
      'id' => 149,
      'name' => 'Bangladesh',
    ],
    19 => 
     [
      'id' => 128,
      'name' => 'Barbados',
    ],
    20 => 
     [
      'id' => 9,
      'name' => 'Bélgica',
    ],
    21 => 
     [
      'id' => 8,
      'name' => 'Belice',
    ],
    22 => 
     [
      'id' => 151,
      'name' => 'Benín',
    ],
    23 => 
     [
      'id' => 10,
      'name' => 'Bermudas',
    ],
    24 => 
     [
      'id' => 7,
      'name' => 'Bielorrusia',
    ],
    25 => 
     [
      'id' => 123,
      'name' => 'Bolivia',
    ],
    26 => 
     [
      'id' => 79,
      'name' => 'Bosnia y Herzegovina',
    ],
    27 => 
     [
      'id' => 100,
      'name' => 'Botsuana',
    ],
    28 => 
     [
      'id' => 12,
      'name' => 'Brasil',
    ],
    29 => 
     [
      'id' => 155,
      'name' => 'Brunéi',
    ],
    30 => 
     [
      'id' => 11,
      'name' => 'Bulgaria',
    ],
    31 => 
     [
      'id' => 156,
      'name' => 'Burkina Faso',
    ],
    32 => 
     [
      'id' => 157,
      'name' => 'Burundi',
    ],
    33 => 
     [
      'id' => 152,
      'name' => 'Bután',
    ],
    34 => 
     [
      'id' => 159,
      'name' => 'Cabo Verde',
    ],
    35 => 
     [
      'id' => 158,
      'name' => 'Camboya',
    ],
    36 => 
     [
      'id' => 31,
      'name' => 'Camerún',
    ],
    37 => 
     [
      'id' => 32,
      'name' => 'Canadá',
    ],
    38 => 
     [
      'id' => 130,
      'name' => 'Chad',
    ],
    39 => 
     [
      'id' => 81,
      'name' => 'Chile',
    ],
    40 => 
     [
      'id' => 35,
      'name' => 'China',
    ],
    41 => 
     [
      'id' => 33,
      'name' => 'Chipre',
    ],
    42 => 
     [
      'id' => 82,
      'name' => 'Colombia',
    ],
    43 => 
     [
      'id' => 164,
      'name' => 'Comores',
    ],
    44 => 
     [
      'id' => 112,
      'name' => 'Congo [Brazzaville]',
    ],
    45 => 
     [
      'id' => 165,
      'name' => 'Congo [Kinshasa]',
    ],
    46 => 
     [
      'id' => 166,
      'name' => 'Cook, Islas',
    ],
    47 => 
     [
      'id' => 84,
      'name' => 'Corea del Norte',
    ],
    48 => 
     [
      'id' => 69,
      'name' => 'Corea del Sur',
    ],
    49 => 
     [
      'id' => 168,
      'name' => 'Costa de Marfil',
    ],
    50 => 
     [
      'id' => 36,
      'name' => 'Costa Rica',
    ],
    51 => 
     [
      'id' => 71,
      'name' => 'Croacia',
    ],
    52 => 
     [
      'id' => 113,
      'name' => 'Cuba',
    ],
    53 => 
     [
      'id' => 22,
      'name' => 'Dinamarca',
    ],
    54 => 
     [
      'id' => 169,
      'name' => 'Djibouti, Yibuti',
    ],
    55 => 
     [
      'id' => 103,
      'name' => 'Ecuador',
    ],
    56 => 
     [
      'id' => 23,
      'name' => 'Egipto',
    ],
    57 => 
     [
      'id' => 51,
      'name' => 'El Salvador',
    ],
    58 => 
     [
      'id' => 93,
      'name' => 'Emiratos Árabes Unidos',
    ],
    59 => 
     [
      'id' => 173,
      'name' => 'Eritrea',
    ],
    60 => 
     [
      'id' => 52,
      'name' => 'Eslovaquia',
    ],
    61 => 
     [
      'id' => 53,
      'name' => 'Eslovenia',
    ],
    62 => 
     [
      'id' => 28,
      'name' => 'España',
    ],
    63 => 
     [
      'id' => 55,
      'name' => 'Estados Unidos',
    ],
    64 => 
     [
      'id' => 68,
      'name' => 'Estonia',
    ],
    65 => 
     [
      'id' => 121,
      'name' => 'Etiopía',
    ],
    66 => 
     [
      'id' => 175,
      'name' => 'Feroe, Islas',
    ],
    67 => 
     [
      'id' => 90,
      'name' => 'Filipinas',
    ],
    68 => 
     [
      'id' => 63,
      'name' => 'Finlandia',
    ],
    69 => 
     [
      'id' => 176,
      'name' => 'Fiyi',
    ],
    70 => 
     [
      'id' => 64,
      'name' => 'Francia',
    ],
    71 => 
     [
      'id' => 180,
      'name' => 'Gabón',
    ],
    72 => 
     [
      'id' => 181,
      'name' => 'Gambia',
    ],
    73 => 
     [
      'id' => 21,
      'name' => 'Georgia',
    ],
    74 => 
     [
      'id' => 105,
      'name' => 'Ghana',
    ],
    75 => 
     [
      'id' => 143,
      'name' => 'Gibraltar',
    ],
    76 => 
     [
      'id' => 184,
      'name' => 'Granada',
    ],
    77 => 
     [
      'id' => 20,
      'name' => 'Grecia',
    ],
    78 => 
     [
      'id' => 94,
      'name' => 'Groenlandia',
    ],
    79 => 
     [
      'id' => 17,
      'name' => 'Guadalupe',
    ],
    80 => 
     [
      'id' => 185,
      'name' => 'Guatemala',
    ],
    81 => 
     [
      'id' => 186,
      'name' => 'Guernsey',
    ],
    82 => 
     [
      'id' => 187,
      'name' => 'Guinea',
    ],
    83 => 
     [
      'id' => 172,
      'name' => 'Guinea Ecuatorial',
    ],
    84 => 
     [
      'id' => 188,
      'name' => 'Guinea-Bissau',
    ],
    85 => 
     [
      'id' => 189,
      'name' => 'Guyana',
    ],
    86 => 
     [
      'id' => 16,
      'name' => 'Haiti',
    ],
    87 => 
     [
      'id' => 137,
      'name' => 'Honduras',
    ],
    88 => 
     [
      'id' => 73,
      'name' => 'Hong Kong',
    ],
    89 => 
     [
      'id' => 14,
      'name' => 'Hungría',
    ],
    90 => 
     [
      'id' => 25,
      'name' => 'India',
    ],
    91 => 
     [
      'id' => 74,
      'name' => 'Indonesia',
    ],
    92 => 
     [
      'id' => 140,
      'name' => 'Irak',
    ],
    93 => 
     [
      'id' => 26,
      'name' => 'Irán',
    ],
    94 => 
     [
      'id' => 27,
      'name' => 'Irlanda',
    ],
    95 => 
     [
      'id' => 215,
      'name' => 'Isla Pitcairn',
    ],
    96 => 
     [
      'id' => 83,
      'name' => 'Islandia',
    ],
    97 => 
     [
      'id' => 228,
      'name' => 'Islas Salomón',
    ],
    98 => 
     [
      'id' => 58,
      'name' => 'Islas Turcas y Caicos',
    ],
    99 => 
     [
      'id' => 154,
      'name' => 'Islas Virgenes Británicas',
    ],
    100 => 
     [
      'id' => 24,
      'name' => 'Israel',
    ],
    101 => 
     [
      'id' => 29,
      'name' => 'Italia',
    ],
    102 => 
     [
      'id' => 132,
      'name' => 'Jamaica',
    ],
    103 => 
     [
      'id' => 70,
      'name' => 'Japón',
    ],
    104 => 
     [
      'id' => 193,
      'name' => 'Jersey',
    ],
    105 => 
     [
      'id' => 75,
      'name' => 'Jordania',
    ],
    106 => 
     [
      'id' => 30,
      'name' => 'Kazajstán',
    ],
    107 => 
     [
      'id' => 97,
      'name' => 'Kenia',
    ],
    108 => 
     [
      'id' => 34,
      'name' => 'Kirguistán',
    ],
    109 => 
     [
      'id' => 195,
      'name' => 'Kiribati',
    ],
    110 => 
     [
      'id' => 37,
      'name' => 'Kuwait',
    ],
    111 => 
     [
      'id' => 196,
      'name' => 'Laos',
    ],
    112 => 
     [
      'id' => 197,
      'name' => 'Lesotho',
    ],
    113 => 
     [
      'id' => 38,
      'name' => 'Letonia',
    ],
    114 => 
     [
      'id' => 99,
      'name' => 'Líbano',
    ],
    115 => 
     [
      'id' => 198,
      'name' => 'Liberia',
    ],
    116 => 
     [
      'id' => 39,
      'name' => 'Libia',
    ],
    117 => 
     [
      'id' => 126,
      'name' => 'Liechtenstein',
    ],
    118 => 
     [
      'id' => 40,
      'name' => 'Lituania',
    ],
    119 => 
     [
      'id' => 41,
      'name' => 'Luxemburgo',
    ],
    120 => 
     [
      'id' => 85,
      'name' => 'Macedonia',
    ],
    121 => 
     [
      'id' => 134,
      'name' => 'Madagascar',
    ],
    122 => 
     [
      'id' => 76,
      'name' => 'Malasia',
    ],
    123 => 
     [
      'id' => 125,
      'name' => 'Malawi',
    ],
    124 => 
     [
      'id' => 200,
      'name' => 'Maldivas',
    ],
    125 => 
     [
      'id' => 133,
      'name' => 'Malí',
    ],
    126 => 
     [
      'id' => 86,
      'name' => 'Malta',
    ],
    127 => 
     [
      'id' => 131,
      'name' => 'Man, Isla de',
    ],
    128 => 
     [
      'id' => 104,
      'name' => 'Marruecos',
    ],
    129 => 
     [
      'id' => 201,
      'name' => 'Martinica',
    ],
    130 => 
     [
      'id' => 202,
      'name' => 'Mauricio',
    ],
    131 => 
     [
      'id' => 108,
      'name' => 'Mauritania',
    ],
    132 => 
     [
      'id' => 42,
      'name' => 'México',
    ],
    133 => 
     [
      'id' => 43,
      'name' => 'Moldavia',
    ],
    134 => 
     [
      'id' => 44,
      'name' => 'Mónaco',
    ],
    135 => 
     [
      'id' => 139,
      'name' => 'Mongolia',
    ],
    136 => 
     [
      'id' => 117,
      'name' => 'Mozambique',
    ],
    137 => 
     [
      'id' => 205,
      'name' => 'Myanmar',
    ],
    138 => 
     [
      'id' => 102,
      'name' => 'Namibia',
    ],
    139 => 
     [
      'id' => 206,
      'name' => 'Nauru',
    ],
    140 => 
     [
      'id' => 107,
      'name' => 'Nepal',
    ],
    141 => 
     [
      'id' => 209,
      'name' => 'Nicaragua',
    ],
    142 => 
     [
      'id' => 210,
      'name' => 'Níger',
    ],
    143 => 
     [
      'id' => 115,
      'name' => 'Nigeria',
    ],
    144 => 
     [
      'id' => 212,
      'name' => 'Norfolk Island',
    ],
    145 => 
     [
      'id' => 46,
      'name' => 'Noruega',
    ],
    146 => 
     [
      'id' => 208,
      'name' => 'Nueva Caledonia',
    ],
    147 => 
     [
      'id' => 45,
      'name' => 'Nueva Zelanda',
    ],
    148 => 
     [
      'id' => 213,
      'name' => 'Omán',
    ],
    149 => 
     [
      'id' => 19,
      'name' => 'Países Bajos, Holanda',
    ],
    150 => 
     [
      'id' => 87,
      'name' => 'Pakistán',
    ],
    151 => 
     [
      'id' => 124,
      'name' => 'Panamá',
    ],
    152 => 
     [
      'id' => 88,
      'name' => 'Papúa-Nueva Guinea',
    ],
    153 => 
     [
      'id' => 110,
      'name' => 'Paraguay',
    ],
    154 => 
     [
      'id' => 89,
      'name' => 'Perú',
    ],
    155 => 
     [
      'id' => 178,
      'name' => 'Polinesia Francesa',
    ],
    156 => 
     [
      'id' => 47,
      'name' => 'Polonia',
    ],
    157 => 
     [
      'id' => 48,
      'name' => 'Portugal',
    ],
    158 => 
     [
      'id' => 246,
      'name' => 'Puerto Rico',
    ],
    159 => 
     [
      'id' => 216,
      'name' => 'Qatar',
    ],
    160 => 
     [
      'id' => 13,
      'name' => 'Reino Unido',
    ],
    161 => 
     [
      'id' => 65,
      'name' => 'República Checa',
    ],
    162 => 
     [
      'id' => 138,
      'name' => 'República Dominicana',
    ],
    163 => 
     [
      'id' => 49,
      'name' => 'Reunión',
    ],
    164 => 
     [
      'id' => 217,
      'name' => 'Ruanda',
    ],
    165 => 
     [
      'id' => 72,
      'name' => 'Rumanía',
    ],
    166 => 
     [
      'id' => 50,
      'name' => 'Rusia',
    ],
    167 => 
     [
      'id' => 242,
      'name' => 'Sáhara Occidental',
    ],
    168 => 
     [
      'id' => 223,
      'name' => 'Samoa',
    ],
    169 => 
     [
      'id' => 219,
      'name' => 'San Cristobal y Nevis',
    ],
    170 => 
     [
      'id' => 224,
      'name' => 'San Marino',
    ],
    171 => 
     [
      'id' => 221,
      'name' => 'San Pedro y Miquelón',
    ],
    172 => 
     [
      'id' => 225,
      'name' => 'San Tomé y Príncipe',
    ],
    173 => 
     [
      'id' => 222,
      'name' => 'San Vincente y Granadinas',
    ],
    174 => 
     [
      'id' => 218,
      'name' => 'Santa Elena',
    ],
    175 => 
     [
      'id' => 220,
      'name' => 'Santa Lucía',
    ],
    176 => 
     [
      'id' => 135,
      'name' => 'Senegal',
    ],
    177 => 
     [
      'id' => 226,
      'name' => 'Serbia y Montenegro',
    ],
    178 => 
     [
      'id' => 109,
      'name' => 'Seychelles',
    ],
    179 => 
     [
      'id' => 227,
      'name' => 'Sierra Leona',
    ],
    180 => 
     [
      'id' => 77,
      'name' => 'Singapur',
    ],
    181 => 
     [
      'id' => 106,
      'name' => 'Siria',
    ],
    182 => 
     [
      'id' => 229,
      'name' => 'Somalia',
    ],
    183 => 
     [
      'id' => 120,
      'name' => 'Sri Lanka',
    ],
    184 => 
     [
      'id' => 141,
      'name' => 'Sudáfrica',
    ],
    185 => 
     [
      'id' => 232,
      'name' => 'Sudán',
    ],
    186 => 
     [
      'id' => 67,
      'name' => 'Suecia',
    ],
    187 => 
     [
      'id' => 66,
      'name' => 'Suiza',
    ],
    188 => 
     [
      'id' => 54,
      'name' => 'Surinam',
    ],
    189 => 
     [
      'id' => 234,
      'name' => 'Swazilandia',
    ],
    190 => 
     [
      'id' => 56,
      'name' => 'Tadjikistan',
    ],
    191 => 
     [
      'id' => 92,
      'name' => 'Tailandia',
    ],
    192 => 
     [
      'id' => 78,
      'name' => 'Taiwan',
    ],
    193 => 
     [
      'id' => 101,
      'name' => 'Tanzania',
    ],
    194 => 
     [
      'id' => 171,
      'name' => 'Timor Oriental',
    ],
    195 => 
     [
      'id' => 136,
      'name' => 'Togo',
    ],
    196 => 
     [
      'id' => 235,
      'name' => 'Tokelau',
    ],
    197 => 
     [
      'id' => 236,
      'name' => 'Tonga',
    ],
    198 => 
     [
      'id' => 237,
      'name' => 'Trinidad y Tobago',
    ],
    199 => 
     [
      'id' => 122,
      'name' => 'Túnez',
    ],
    200 => 
     [
      'id' => 57,
      'name' => 'Turkmenistan',
    ],
    201 => 
     [
      'id' => 59,
      'name' => 'Turquía',
    ],
    202 => 
     [
      'id' => 239,
      'name' => 'Tuvalu',
    ],
    203 => 
     [
      'id' => 62,
      'name' => 'Ucrania',
    ],
    204 => 
     [
      'id' => 60,
      'name' => 'Uganda',
    ],
    205 => 
     [
      'id' => 111,
      'name' => 'Uruguay',
    ],
    206 => 
     [
      'id' => 61,
      'name' => 'Uzbekistán',
    ],
    207 => 
     [
      'id' => 240,
      'name' => 'Vanuatu',
    ],
    208 => 
     [
      'id' => 95,
      'name' => 'Venezuela',
    ],
    209 => 
     [
      'id' => 15,
      'name' => 'Vietnam',
    ],
    210 => 
     [
      'id' => 241,
      'name' => 'Wallis y Futuna',
    ],
    211 => 
     [
      'id' => 243,
      'name' => 'Yemen',
    ],
    212 => 
     [
      'id' => 116,
      'name' => 'Zambia',
    ],
    213 => 
     [
      'id' => 96,
      'name' => 'Zimbabwe',
    ],
  ],
];