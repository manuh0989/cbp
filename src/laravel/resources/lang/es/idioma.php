<?php
return [
	'message'=>[
		'usuario'=>[
			'registro'=>[
				'ok'=>'Usuario guardado correctamente'
			]
			,'actualizar'=>[
				'ok'=>'Usuario modificado correctamente'
			]
			,'restore'=>[
				'ok'=>'Usuario restaurado correctamente'
			]
			,'email'=>[
				'error'=>'No existe plantilla para :tipoPlantilla'
			]
		]
	]
	,'transaction'=>[
		'usuarios'=>[
			'update'=>[
				'mensaje'=>'Usuario :nombreCompleto modificado exitosamente'
			]
		]
	]
	,'menu'=>[
		'usuarios'=>[
			'usuarios'  =>'Usuarios'
			,'verTodos' =>'Ver todos'
			,'registro' =>'Registrar usuario'
		]
		,'terceros'=>[
			'terceros'      =>'Terceros'
			,'verTodos'     =>'Ver todos'
			,'tipoTerceros' =>'Tipos'
		]
		,'oportunidad'=>[
			'oportunidad'      =>'Oportunidad'
			,'verTodos'     =>'Ver todos'
			,'tipoOportunidad' =>'Tipos'
		]
		,'empresa'=>[
			'empresa'      =>'Empresa'
			,'verTodos'     =>'Ver todos'
			,'tipoEmpresa' =>'Tipos'
		]
		,'contacto'=>[
			'contacto'      =>'Contacto'
			,'verTodos'     =>'Ver todos'
			,'tipoContacto' =>'Tipos'
		]
		,'documento'=>[
			'documento'      =>'Documento'
			,'verTodos'     =>'Ver todos'
		]
		,'poliza'=>[
			'poliza'      =>'Poliza'
			,'verTodos'     =>'Ver todos'
		]
		,'seguro'=>[
			'seguro'      =>'Seguro'
			,'verTodos'     =>'Ver todos'
			,'tipoSeguro' =>'Tipos'
		]
		,'modPoliza'=>[
			'modPoliza'      =>'Modalidad poliza'
			,'verTodos'     =>'Ver todos'
		]
		,'siniestro'=>[
			'siniestro'      =>'Sinisestro'
			,'verTodos'     =>'Ver todos'
			,'tipoSiniestro' =>'Tipos'
		]
		,'inmueble'=>[
			'inmueble'      =>'Inmueble'
			,'verTodos'     =>'Ver todos'
			,'tipoInmueble' =>'Tipos'
		]
		,'role'=>[
			'role'      =>'Roles'
			,'verTodos'     =>'Ver todos'
			,'tipoInmueble' =>'Tipos'
		]
		,'plantilla'=>[
			'plantilla' =>'Plantillas'
			,'email'    =>'Ver plantillas'
		]
		,'consentimiento'=>[
			'consentimiento' =>'Consentimiento'
			,'email'    =>'Ver Consentimientos'
		]
	]
	,'login'=>[
		'login'              =>'Inicio de sesión'
		,'registro'          =>'Registrar usuario'
		,'bienvenida'        =>'Bienvenid@'
		,'emailAdress'       =>'Correo electronico'
		,'password'          =>'Contraseña'
		,'confirmPassword'   =>'Confirmar contraseña'
		,'olvidoPassword'    =>'¿Olvidó su contraseña?'
		,'recuperarPassword' =>'Recuperar contraseña'
		,'sendPassword'      =>'Enviar correo'
		,'resetPassword'     =>'Reinicar contraseña'
		,'verifyemail'		 =>'Verifica tu correo electrónico'
	]
	,'admin'=>[
		'usuarios'=>[
			'vista'=>[
				'header'      =>'Usuarios'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'          =>'#'
					,'nombre'     =>'Nombre'
					,'DNI'        =>'DNI'
					,'username'   =>'Usuario'
					,'email'      =>'Correo'
					,'tipo'       =>'Tipo'
					,'created_at' =>'Fecha registro'
					,'status'     =>'Status'
					,'actions'    =>'Acciones'
				]
				,'mensajeInsertOK'  =>'Usuario :username creado correctamente'
				,'mensajeUpdateOK'  =>'Usuario :username modificado correctamente'
				,'mensajeTrashOK'   =>'Usuario :nombre dado de baja correctamente'
				,'mensajeRestoreOK' =>'Usuario :nombre restaurado correctamente'
			]
			,'registro'=>[
				'titulo'           =>'Registro de usuarios'
				,'tipo'            =>'Tipo de usuario'
				,'nombre'          =>'Nombre:'
				,'apellidoPaterno' =>'Apellido paterno:'
				,'apellidoMaterno' =>'Apellido materno:'
				,'username'        =>'Usuario:'
				,'email'           =>'Correo:'
				,'password'        =>'Contraseña:'
				,'passwordConfirm' =>'Confirmar contraseña:'
				,'rememberToken'   =>'Recordar contraseña:'
				,'btnGuardar'      =>'Guardar'
				,'btnEditar'       =>'Editar'
				,'DNI'             =>'DNI'
			]
		]
		,'terceros'=>[
			'vista'=>[
				'header'      =>'Terceros'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'          =>'#'
					,'nombre'     =>'Nombre'
					,'email'      =>'Correo'
					,'telefono'   =>'Teléfono'
					,'tipo'       =>'Tipo'
					,'created_at' =>'Fecha registro'
					,'status'     =>'Status'
					,'actions'    =>'Acciones'
				]
				,'mensajeInsertOK'  =>'Tercero :nombre creado correctamente'
				,'mensajeUpdateOK'  =>'Tercero :nombre modificado correctamente'
				,'mensajeTrashOK'   =>'Tercero :nombre dado de baja correctamente'
				,'mensajeRestoreOK' =>'Tercero :nombre restaurado correctamente'
			]
			,'registro'=>[
				'titulo'           =>'Registro de terceros'
				,'nombre'          =>'Nombre:'
				,'apellido1'       =>'Apellido 1:'
				,'apellido2'       =>'Apellido 2:'
				,'fechaNacimiento' =>'Fecha de nacimiento:'
				,'codPostal'       =>'Codigo postal:'
				,'direccion'       =>'Direccion:'
				,'provincia'       =>'Provincia:'
				,'poblacion'       =>'Población:'
				,'telefono'        =>'Teléfono:'
				,'email'           =>'Correo:'
				,'btnGuardar'      =>'Guardar'
				,'btnEditar'       =>'Editar'
				,'tipo'            =>'Tipo tercero'
			]
		]
		,'oportunidad'=>[
			'vista'=>[
				'header'      =>'Oportunidades'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'          =>'#'
					,'nombre'     =>'Nombre'
					,'tipo' 	  =>'Tipo'
					,'created_at' =>'Fecha registro'
					,'status'     =>'Status'
					,'actions'    =>'Acciones'
				]
				,'mensajeInsertOK'  =>'Oportunidad :nombre creada correctamente'
				,'mensajeUpdateOK'  =>'Oportunidad :nombre modificado correctamente'
				,'mensajeTrashOK'   =>'Oportunidad :nombre dado de baja correctamente'
				,'mensajeRestoreOK' =>'Oportunidad :nombre restaurado correctamente'
			]
			,'registro'=>[
				'titulo'           =>'Oportunidades'
				,'tipo' 		   =>'Tipo oportunidad'
				,'nombre'          =>'Nombre:'
				,'btnGuardar'      =>'Guardar'
				,'btnEditar'       =>'Editar'
			]
		]
		,'empresa'=>[
			'vista'=>[
				'header'      =>'Empresas'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'          =>'#'
					,'nombre'     =>'Nombre'
					,'tipo' 	  =>'Tipo'
					,'created_at' =>'Fecha registro'
					,'status'     =>'Status'
					,'actions'    =>'Acciones'
				]
				,'mensajeInsertOK'  =>'Empresa :nombre creada correctamente'
				,'mensajeUpdateOK'  =>'Empresa :nombre modificada correctamente'
				,'mensajeTrashOK'   =>'Empresa :nombre dado de baja correctamente'
				,'mensajeRestoreOK' =>'Empresa :nombre restaurado correctamente'
			]
			,'registro'=>[
				'titulo'           =>'Empresa'
				,'tipo' 		   =>'Tipo Empresa'
				,'nombre'          =>'Nombre:'
				,'btnGuardar'      =>'Guardar'
				,'btnEditar'       =>'Editar'
			]
		]
		,'contacto'=>[
			'vista'=>[
				'header'      =>'Contactos'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'          =>'#'
					,'nombre'     =>'Nombre'
					,'tipo' 	  =>'Tipo'
					,'created_at' =>'Fecha registro'
					,'status'     =>'Status'
					,'actions'    =>'Acciones'
				]
				,'mensajeInsertOK'  =>'Contacto :nombre creado correctamente'
				,'mensajeUpdateOK'  =>'Contacto :nombre modificado correctamente'
				,'mensajeTrashOK'   =>'Contacto :nombre dado de baja correctamente'
				,'mensajeRestoreOK' =>'Contacto :nombre restaurado correctamente'
			]
			,'registro'=>[
				'titulo'           =>'Contactos'
				,'tipo' 		   =>'Tipo Contactos'
				,'nombre'          =>'Nombre:'
				,'btnGuardar'      =>'Guardar'
				,'btnEditar'       =>'Editar'
			]
		]
		,'documento'=>[
			'vista'=>[
				'header'      =>'Documentos'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'          =>'#'
					,'nombre'     =>'Nombre'
					,'created_at' =>'Fecha registro'
					,'status'     =>'Status'
					,'actions'    =>'Acciones'
				]
				,'mensajeInsertOK'  =>'Documento :nombre creado correctamente con circuito :idCircuito'
				,'mensajeUpdateOK'  =>'Documento :nombre modificado correctamente'
				,'mensajeTrashOK'   =>'Documento :nombre dado de baja correctamente'
				,'mensajeRestoreOK' =>'Documento :nombre restaurado correctamente'
			]
			,'registro'=>[
				'titulo'           =>'Documentos'
				,'tipo' 		   =>'Tipo Contactos'
				,'nombre'          =>'Nombre:'
				,'btnGuardar'      =>'Guardar'
				,'btnEditar'       =>'Editar'
			]
		]
		,'seguro'=>[
			'vista'=>[
				'header'      =>'Seguros'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'          =>'#'
					,'nombre'     =>'Nombre'
					,'tipo' 	  =>'Tipo'
					,'created_at' =>'Fecha registro'
					,'status'     =>'Status'
					,'actions'    =>'Acciones'
				]
				,'mensajeInsertOK'  =>'Seguro :nombre creado correctamente'
				,'mensajeUpdateOK'  =>'Seguro :nombre modificado correctamente'
				,'mensajeTrashOK'   =>'Seguro :nombre dado de baja correctamente'
				,'mensajeRestoreOK' =>'Seguro :nombre restaurado correctamente'
			]
			,'registro'=>[
				'titulo'           =>'Seguros'
				,'tipo' 		   =>'Tipo Seguros'
				,'nombre'          =>'Nombre:'
				,'btnGuardar'      =>'Guardar'
				,'btnEditar'       =>'Editar'
			]
		]
		,'poliza'=>[
			'vista'=>[
				'header'      =>'Polizas'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'          =>'#'
					,'nombre'     =>'Nombre'
					,'created_at' =>'Fecha registro'
					,'status'     =>'Status'
					,'actions'    =>'Acciones'
				]
				,'mensajeInsertOK'  =>'Poliza :nombre creada correctamente'
				,'mensajeUpdateOK'  =>'Poliza :nombre modificada correctamente'
				,'mensajeTrashOK'   =>'Poliza :nombre dado de baja correctamente'
				,'mensajeRestoreOK' =>'Poliza :nombre restaurado correctamente'
			]
			,'registro'=>[
				'titulo'           =>'Polizas'
				,'tipo' 		   =>'Tipo polizas'
				,'nombre'          =>'Nombre:'
				,'btnGuardar'      =>'Guardar'
				,'btnEditar'       =>'Editar'
			]
		]
		,'modPoliza'=>[
			'vista'=>[
				'header'      =>'Modalidad poliza'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'          =>'#'
					,'nombre'     =>'Nombre'
					,'created_at' =>'Fecha registro'
					,'status'     =>'Status'
					,'actions'    =>'Acciones'
				]
				,'mensajeInsertOK'  =>'Modalidad poliza :nombre creada correctamente'
				,'mensajeUpdateOK'  =>'Modalidad :nombre modificada correctamente'
				,'mensajeTrashOK'   =>'Modalidad poliza :nombre dado de baja correctamente'
				,'mensajeRestoreOK' =>'Modalidad poliza :nombre restaurado correctamente'
			]
			,'registro'=>[
				'titulo'           =>'Modalidad polizas'
				,'tipo' 		   =>'Tipo polizas'
				,'nombre'          =>'Nombre:'
				,'btnGuardar'      =>'Guardar'
				,'btnEditar'       =>'Editar'
			]
		]
		,'siniestro'=>[
			'vista'=>[
				'header'      =>'Siniestro'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'          =>'#'
					,'nombre'     =>'Nombre'
					,'tipo' 	  =>'Tipo'
					,'created_at' =>'Fecha registro'
					,'status'     =>'Status'
					,'actions'    =>'Acciones'
				]
				,'mensajeInsertOK'  =>'Siniestro :nombre creado correctamente'
				,'mensajeUpdateOK'  =>'Siniestro :nombre modificado correctamente'
				,'mensajeTrashOK'   =>'Siniestro :nombre dado de baja correctamente'
				,'mensajeRestoreOK' =>'Siniestro :nombre restaurado correctamente'
			]
			,'registro'=>[
				'titulo'           =>'Siniestro'
				,'tipo' 		   =>'Tipo Siniestro'
				,'nombre'          =>'Nombre:'
				,'btnGuardar'      =>'Guardar'
				,'btnEditar'       =>'Editar'
			]
		]
		,'inmueble'=>[
			'vista'=>[
				'header'      =>'Inmueble'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'          =>'#'
					,'nombre'     =>'Nombre'
					,'tipo' 	  =>'Tipo'
					,'created_at' =>'Fecha registro'
					,'status'     =>'Status'
					,'actions'    =>'Acciones'
				]
				,'mensajeInsertOK'  =>'Inmueble :nombre creado correctamente'
				,'mensajeUpdateOK'  =>'Inmueble :nombre modificado correctamente'
				,'mensajeTrashOK'   =>'Inmueble :nombre dado de baja correctamente'
				,'mensajeRestoreOK' =>'Inmueble :nombre restaurado correctamente'
			]
			,'registro'=>[
				'titulo'           =>'Inmueble'
				,'tipo' 		   =>'Tipo Inmueble'
				,'nombre'          =>'Nombre:'
				,'btnGuardar'      =>'Guardar'
				,'btnEditar'       =>'Editar'
			]
		]
		,'role'=>[
			'vista'=>[
				'header'      =>'Role'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'          =>'#'
					,'nombre'     =>'Nombre'
					,'titulo' 	  =>'Titulo'
					,'created_at' =>'Fecha registro'
					,'status'     =>'Status'
					,'actions'    =>'Acciones'
					]
			]
			,'registro'=>[
				'titulo'           =>'Role'
				,'tipo' 		   =>'Tipo Inmueble'
				,'nombre'          =>'Nombre:'
				,'title'		   =>'Titulo:'
				,'btnGuardar'      =>'Guardar'
				,'btnEditar'       =>'Editar'
			]
		]
		,'tiposUsuarios'=>[
			'vista'=>[
				'header'      =>'Tipos usuarios'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'           =>'#'
					,'nombre'      =>'Nombre'
					,'descripcion' =>'Descripción'
					,'created_at'  =>'Fecha registro'
					,'status'      =>'Status'
					,'actions'     =>'Acciones'
				]
				,'mensajeInsertOK'=>'Tipo usuario :nombre creado correctamente'
				,'mensajeUpdateOK'=>'Tipo usuario :nombre modificado correctamente'
			]
			,'registro'=>[
				'titulo'       =>'Registro de tipos de usuarios'
				,'nombre'      =>'Nombre:'
				,'descripcion' =>'Descripción:'
				,'btnGuardar'  =>'Guardar'
				,'btnEditar'   =>'Editar'
			]
		]
		,'tiposTerceros'=>[
			'vista'=>[
				'header'      =>'Tipos terceros'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'           =>'#'
					,'nombre'      =>'Nombre'
					,'descripcion' =>'Descripción'
					,'created_at'  =>'Fecha registro'
					,'status'      =>'Status'
					,'actions'     =>'Acciones'
				]
				,'mensajeInsertOK'=>'Tipo tercero :nombre creado correctamente'
				,'mensajeUpdateOK'=>'Tipo tercero :nombre modificado correctamente'
			]
			,'registro'=>[
				'titulo'       =>'Registro de tipos de terceros'
				,'nombre'      =>'Nombre:'
				,'descripcion' =>'Descripción:'
				,'btnGuardar'  =>'Guardar'
				,'btnEditar'   =>'Editar'
			]
		]
		,'tiposOportunidad'=>[
			'vista'=>[
				'header'      =>'Tipos oportunidad'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'           =>'#'
					,'nombre'      =>'Nombre'
					,'descripcion' =>'Descripción'
					,'created_at'  =>'Fecha registro'
					,'status'      =>'Status'
					,'actions'     =>'Acciones'
				]
				,'mensajeInsertOK'=>'Tipo oportunidad :nombre creado correctamente'
				,'mensajeUpdateOK'=>'Tipo oportunidad :nombre modificado correctamente'
			]
			,'registro'=>[
				'titulo'       =>'Registro de tipos de terceros'
				,'nombre'      =>'Nombre:'
				,'descripcion' =>'Descripción:'
				,'btnGuardar'  =>'Guardar'
				,'btnEditar'   =>'Editar'
			]
		]
		,'tiposEmpresa'=>[
			'vista'=>[
				'header'      =>'Tipos empresa'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'           =>'#'
					,'nombre'      =>'Nombre'
					,'descripcion' =>'Descripción'
					,'created_at'  =>'Fecha registro'
					,'status'      =>'Status'
					,'actions'     =>'Acciones'
				]
				,'mensajeInsertOK'=>'Tipo empresa :nombre creado correctamente'
				,'mensajeUpdateOK'=>'Tipo empresa :nombre modificada correctamente'
			]
			,'registro'=>[
				'titulo'       =>'Registro de tipos de empresa'
				,'nombre'      =>'Nombre:'
				,'descripcion' =>'Descripción:'
				,'btnGuardar'  =>'Guardar'
				,'btnEditar'   =>'Editar'
			]
		]
		,'tiposContacto'=>[
			'vista'=>[
				'header'      =>'Tipos contacto'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'           =>'#'
					,'nombre'      =>'Nombre'
					,'descripcion' =>'Descripción'
					,'created_at'  =>'Fecha registro'
					,'status'      =>'Status'
					,'actions'     =>'Acciones'
				]
				,'mensajeInsertOK'=>'Tipo contacto :nombre creado correctamente'
				,'mensajeUpdateOK'=>'Tipo contacto :nombre modificado correctamente'
			]
			,'registro'=>[
				'titulo'       =>'Registro de tipos de contacto'
				,'nombre'      =>'Nombre:'
				,'descripcion' =>'Descripción:'
				,'btnGuardar'  =>'Guardar'
				,'btnEditar'   =>'Editar'
			]
		]
		,'tiposSeguro'=>[
			'vista'=>[
				'header'      =>'Tipos seguro'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'           =>'#'
					,'nombre'      =>'Nombre'
					,'descripcion' =>'Descripción'
					,'created_at'  =>'Fecha registro'
					,'status'      =>'Status'
					,'actions'     =>'Acciones'
				]
				,'mensajeInsertOK'=>'Tipo seguro :nombre creado correctamente'
				,'mensajeUpdateOK'=>'Tipo seguro :nombre modificado correctamente'
			]
			,'registro'=>[
				'titulo'       =>'Registro de tipos de seguro'
				,'nombre'      =>'Nombre:'
				,'descripcion' =>'Descripción:'
				,'btnGuardar'  =>'Guardar'
				,'btnEditar'   =>'Editar'
			]
		]
		,'tiposSiniestro'=>[
			'vista'=>[
				'header'      =>'Tipos siniestro'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'           =>'#'
					,'nombre'      =>'Nombre'
					,'descripcion' =>'Descripción'
					,'created_at'  =>'Fecha registro'
					,'status'      =>'Status'
					,'actions'     =>'Acciones'
				]
				,'mensajeInsertOK'=>'Tipo siniestro :nombre creado correctamente'
				,'mensajeUpdateOK'=>'Tipo siniestro :nombre modificado correctamente'
			]
			,'registro'=>[
				'titulo'       =>'Registro de tipos de siniestro'
				,'nombre'      =>'Nombre:'
				,'descripcion' =>'Descripción:'
				,'btnGuardar'  =>'Guardar'
				,'btnEditar'   =>'Editar'
			]
		]
		,'tiposInmueble'=>[
			'vista'=>[
				'header'      =>'Tipos inmueble'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'           =>'#'
					,'nombre'      =>'Nombre'
					,'descripcion' =>'Descripción'
					,'created_at'  =>'Fecha registro'
					,'status'      =>'Status'
					,'actions'     =>'Acciones'
				]
				,'mensajeInsertOK'=>'Tipo inmueble :nombre creado correctamente'
				,'mensajeUpdateOK'=>'Tipo inmueble :nombre modificado correctamente'
			]
			,'registro'=>[
				'titulo'       =>'Registro de tipos de inmueble'
				,'nombre'      =>'Nombre:'
				,'descripcion' =>'Descripción:'
				,'btnGuardar'  =>'Guardar'
				,'btnEditar'   =>'Editar'
			]
		]

		,'plantilla'=>[
			'vista'=>[
				'header'      =>'Plantillas'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'          =>'#'
					,'nombre'     =>'Nombre'
					,'tipo' 	  =>'Tipo'
					,'created_at' =>'Fecha registro'
					,'status'     =>'Status'
					,'actions'    =>'Acciones'
				]
				,'mensajeInsertOK'  =>'Plantilla :nombre creado correctamente'
				,'mensajeUpdateOK'  =>'Plantilla :nombre modificado correctamente'
				,'mensajeTrashOK'   =>'Plantilla :nombre dado de baja correctamente'
				,'mensajeRestoreOK' =>'Plantilla :nombre restaurado correctamente'
			]
			,'registro'=>[
				'titulo'      =>'Plantilla:'
				,'tipo'       =>'Tipo plantilla:'
				,'texto_html' =>'Texto de plantilla:'
				,'descripcion'=>'Descripción:'
				,'nombre'     =>'Nombre:'
				,'btnGuardar' =>'Guardar'
				,'btnEditar'  =>'Editar'
			]

		]
		,'consentimiento'=>[
			'vista'=>[
				'header'      =>'Consentimientos'
				,'activo'       =>'Activo'
				,'bajaTemporal' =>'Baja'
				,'headerTable'=>[
					'id'          =>'#'
					,'nombre'     =>'Ruta'
					,'plantilla'  =>'Nombre plantilla'
					,'created_at' =>'Fecha registro'
					,'status'     =>'Status'
					,'actions'    =>'Acciones'
				]
				,'mensajeInsertOK'  =>'Consentimiento :nombre creado correctamente'
				,'mensajeUpdateOK'  =>'Consentimiento :nombre modificado correctamente'
				,'mensajeTrashOK'   =>'Consentimiento :nombre dado de baja correctamente'
				,'mensajeRestoreOK' =>'Consentimiento :nombre restaurado correctamente'
			]
			,'registro'=>[
				'titulo'      =>'Consentimiento:'
				,'tipo'       =>'Tipo consentimiento:'
				,'texto_html' =>'Texto de plantilla:'
				,'descripcion'=>'Descripción:'
				,'nombre'     =>'Nombre:'
				,'btnGuardar' =>'Guardar'
				,'btnEditar'  =>'Editar'
			]

		]
	]
	,'empleado'=>[
		'registro'=>[
			'titulo'                 =>'Completar datos personales'
			,'nombre'                =>'Nombre:'
			,'apellidoPaterno'       =>'Apellido 1:'
			,'apellidoMaterno'       =>'Apellido 2:'
			,'fechaNacimiento'       =>'Fecha nacimiento:'
			,'password'              =>'Contraseña:'
			,'password_confirmation' =>'Confirmar contraseña:'
			,'telefonoFijo'          =>'Teléfono fijo:'
			,'telefonoMovil'         =>'Teléfono movil:'
			,'DNI_NIE'               =>'DNI-NIE:'
			,'nacionalidad'          =>'Nacionalidad:'
			,'emailPersonal'         =>'Correo personal:'
			,'btnGuardar'            =>'Guardar'
			,'btnEditar'             =>'Editar'
			,'direccion'             =>'Direccion'
		]
		,'mensajes'=>[
			'editar'=>'Datos actualizados exitosamente'
		]
	]
	,'tercero'=>[
		'registro'=>[
			'titulo'           =>'Completar datos personales'
			,'nombre'          =>'Nombre:'
			,'apellidoPaterno' =>'Apellido 1:'
			,'apellidoMaterno' =>'Apellido 2:'
			,'fechaNacimiento' =>'Fecha de nacimiento:'
			
			,'telefonoFijo'    =>'Teléfono fijo:'
			,'telefonoMovil'   =>'Teléfono movil:'
			,'DNI_NIE'         =>'DNI-NIE:'
			,'nacionalidad'    =>'Nacionalidad:'
			,'emailPersonal'   =>'Correo personal:'
			,'btnGuardar'      =>'Guardar'
			,'btnEditar'       =>'Editar'
			,'direccion'       =>'Direccion'
		]
		,'mensajes'=>[
			'editar'=>'Datos actualizados exitosamente'
		]
	]
	,'email'=>[
		'verificacionEmail'=>[
			'header'        =>'header prueba'
			,'btnVerificar' =>'Verificar correo'
		]
	]
	,'direccion'=>[
		'tipoVia'    =>'Tipo via:'
		,'nombreVia' =>'Nombre de via:'
		,'numeroVia' =>'Numero de via:'
		,'portal'    =>'Portal:'
		,'piso'      =>'Piso:'
		,'puerta'    =>'Puerta:'
		,'cp'        =>'Codigo Postal'
		,'poblacion' =>'Población:'
		,'provincia' =>'Provincia:'
	]
	,'colaborador'=>[
		'headerTable'=>[
			'id'          =>'#'
			,'nombre'     =>'Nombre'
			,'DNI'        =>'DNI'
			,'username'   =>'Usuario'
			,'email'      =>'Correo'
			,'tipo'       =>'Tipo'
			,'created_at' =>'Fecha registro'
			,'status'     =>'Status'
			,'actions'    =>'Acciones'
		]
	]

];