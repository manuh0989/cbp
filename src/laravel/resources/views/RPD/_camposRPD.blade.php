<div class="row">
	<div class="col-12">
		<div class="form-group row">
			<label for="folioCBP" class="col-sm-2 col-form-label col-form-label-sm">
				Folio CBP:
			</label>
			<div class="col-sm-3">
			 	<input id="folioCBP" type="text" class="toUpperCase form-control  form-control-sm @error('RPD.folioCBP') is-invalid @enderror" name="RPD[folioCBP]" 
			 	value="{{ old('RPD.folioCBP',$RPD->folioCBP ?? $folioCBP ) }}" readonly="true">
			 	@inputErrors(['input'=>'RPD.folioCBP'])
			</div>
			<label for="idFuente" class="col-sm-2 col-form-label col-form-label-sm">
				Fuente;
			</label>
			<div class="col-sm-3">	
			 	<select 
					name     ="RPD[idFuente]" 
					id       ="idFuente" 
					class    ="form-control form-control-sm"
					url      ="{{ route('ajax.folioCBP') }}"
			 	>
			 		@foreach($fuentes as $key =>$fuente)
			 			<option value="{{ $fuente->idFuente }}" 
			 				{{ selected(old('RPD.idFuente',$RPD->idFuente),$fuente->idFuente) }}
			 			>
			 				{{ $fuente->nombreFuente }}
			 			</option>
			 		@endforeach
			 	</select>
			 	@inputErrors(['input'=>'RPD.idFuente'])
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-12">
		<div class="form-group row">
			<label for="statusLocalizacion" class="col-sm-2 col-form-label col-form-label-sm">
				Estatus localizacion:
			</label>
			<div class="col-sm-3">
			 	<select name="RPD[statusLocalizacion]" id="statusLocalizacion" class="form-control form-control-sm">
			 		<option value="">--sin opción--</option>
		 			<option 
		 				value="DESAPARECIDO" 
		 				{{ selected(old('RPD.statusLocalizacion',$RPD->statusLocalizacion),'DESAPARECIDO') }}
		 				>
		 				DESAPARECIDO
		 			</option>
		 			<option 
		 				value="LOCALIZADO" 
		 				{{ selected(old('RPD.statusLocalizacion',$RPD->statusLocalizacion),'LOCALIZADO') }}
		 				>
		 				LOCALIZADO
		 			</option>
			 	</select>
			 	@inputErrors(['input'=>'RPD.statusLocalizacion'])
			</div>
			<label for="funcionarioRegistro" class="col-sm-2 col-form-label col-form-label-sm">
				Funcionario registro:
			</label>
			<div class="col-sm-3">
			 	<input id="funcionarioRegistro" type="text" class="toUpperCase form-control form-control-sm @error('funcionarioRegistro') is-invalid @enderror" name="RPD[funcionarioRegistro]" 
			 	value="{{ Str::upper(old('RPD.funcionarioRegistro',$RPD->funcionarioRegistro ?? auth()->user()->nombreCompleto)) }}" readonly="true">
			 	@inputErrors(['input'=>'RPD.funcionarioRegistro'])
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-12">
		<div class="form-group row">
			<label for="folioFIPEDE" class="col-sm-2 col-form-label col-form-label-sm">
				Carpeta de Investigaci&oacute;n:
			</label>
			<div class="col-sm-3">
			 	<input id="folioFIPEDE" name="RPD[folioFIPEDE]" type="text" class="toUpperCase form-control form-control-sm @error('RPD.folioFIPEDE') is-invalid @enderror"  value="{{ old('RPD.folioFIPEDE',$RPD->folioFIPEDE) }}">
			 	@inputErrors(['input'=>'RPD.folioFIPEDE'])
			</div>
			<label for="folioLocatel" class="col-sm-2 col-form-label col-form-label-sm">
				Folio LOCATEL:
			</label>
			<div class="col-sm-3">
			 	<input id="folioLocatel" type="text" 
			 	class="toUpperCase form-control form-control-sm @error('RPD.folioLocatel') is-invalid @enderror" name="RPD[folioLocatel]" value="{{ old('RPD.folioLocatel',$RPD->folioLocatel) }}">
			 	@inputErrors(['input'=>'RPD.folioLocatel'])
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-12">
		<div class="form-group row">
			<label for="fechaAlta" class="col-sm-2 col-form-label col-form-label-sm">
				Fecha de alta:
			</label>
			<div class="col-sm-10">
			 	{{ $RPD->fechaRegistroFormat }}
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-12">
		<div class="form-group row">
			<label for="idOrigenNoticia" class="col-sm-2 col-form-label col-form-label-sm">
				Origen noticia:
			</label>
			<div class="col-sm-10">
			 	<select name="personaDesaparecida[idOrigenNoticia]" id="idOrigenNoticia" class="form-control form-control-sm">
			 		<option value="">--sin opción--</option>
			 		@foreach($origenNoticias as $key =>$origenNoticia)
			 			<option value="{{ $origenNoticia->idOrigenNoticia }}" 
			 				{{ selected(old('personaDesaparecida.idOrigenNoticia',optional($RPD->personaDesaparecida)->idOrigenNoticia) ,$origenNoticia->idOrigenNoticia)}}
			 			>
			 				{{ $origenNoticia->nombreOrigenNoticia }}
			 			</option>
			 		@endforeach
			 	</select>
			 	@inputErrors(['input'=>'personaDesaparecida.idOrigenNoticia'])
			</div>
		</div>
	</div>
</div>