<form action="{{ route('RPD.index') }}" method="GET" autocomplete="off">
    <div class="row row-filters">
        <div class="col-md-6">
            <div class="form-inline form-search">
                <input type="search" name="buscar" value="{{ request('buscar') }}" class="form-control form-control-sm toUpperCase" placeholder="Buscar...">
                
                <div class="btn-group mr-2">
                    <select name="opcionBusqueda" id="opcionBusqueda" class="form-control form-control-sm">
                        @foreach(trans('filtrosRPD.filtrosRPD') as $key => $filtro)
                            <option value="{{ $key }}" {{ selected(request('opcionBusqueda'),$key) }}>
                                {{ $filtro }}
                            </option>
                        @endforeach
                    </select>

                </div>
                

            </div>
        </div>

        <div class="col-md-6 text-right">
            <div class="form-inline form-dates">
                <label for="from" class="form-label-sm">
                    Fecha desaparición:
                </label>&nbsp;
                
                <div class="input-group">
                    <input type="text"  size="15"
                            class="form-control form-control-sm fechasDesaparicion"  
                            id="fechaDesaparicionIni" 
                            name="fechaDesaparicionIni"
                            @if(!request('chkFecha')) {{ 'disabled' }} @endif 
                            value="{{ request('fechaDesaparicionIni',\Carbon\Carbon::now()->format('d-m-yy')) }}"
                    >
                </div>

                <div class="input-group">
                    <input type="text" size="15" 
                            class="form-control form-control-sm fechasDesaparicion"  
                            id="fechaDesaparicionFin"
                            name="fechaDesaparicionFin"
                            @if(!request('chkFecha')) {{ 'disabled' }} @endif 
                            value="{{ request('fechaDesaparicionFin',\Carbon\Carbon::now()->format('d-m-yy')) }}" 
                    >
                </div>
                
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" 
                            id="chkFecha" 
                            name="chkFecha" 
                            type="checkbox" 
                            onclick="disabledFechas(this);" 
                            @if(request('chkFecha')) {{ 'checked' }} @endif
                    >
                    <label class="custom-control-label" for="chkFecha"></label>
                    <button type="submit" class="btn btn-sm btn-primary btn-submit">
                        <i data-feather="search"></i>
                    </button>
                    <a href="{{ route('RPD.exportExcel') }}"  class="btn btn-primary btn-sm">
            <i data-feather="file-text"></i>
        </a>
                </div>
            

                <script>
                    $('#fechaDesaparicionIni').datepicker({
                        uLibrary:'bootstrap4'
                        ,format: 'dd-mm-yyyy'
                    });
                    $('#fechaDesaparicionFin').datepicker({
                        uLibrary:'bootstrap4'
                        ,format: 'dd-mm-yyyy'
                    });
                </script>

            </div>
        </div>
    </div>
</form>
