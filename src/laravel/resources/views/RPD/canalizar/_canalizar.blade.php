<form action="{{ route('RPD.storeCanalizacion',$RPD) }}" method="POST" accept-charset="utf-8" autocomplete="off">
    @csrf @method('POST')

    <div class="form-group row">
        <label for="solicitudApoyo" class="col-sm-3 col-form-label col-form-label-sm">
            Envio de solicitud apoyo a dependencias FIPED:
        </label>
        <div class="col-sm-3">
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="solicitudApoyo" name="solicitudApoyo" value="1"
                    {{ checked(old('solicitudApoyo',optional($RPD->canalizacionRPD)->solicitudApoyo)) }}
                >
                <label class="custom-control-label" for="solicitudApoyo"></label>
            </div>
        </div>
        <label for="busquedaSirilo" class="col-sm-3 col-form-label col-form-label-sm">
            Busqueda sirilo:
        </label>
        <div class="col-sm-2">
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="busquedaSirilo" name="busquedaSirilo" value="1"
                    {{ checked(old('busquedaSirilo',optional($RPD->canalizacionRPD)->busquedaSirilo)) }}
                >
                <label class="custom-control-label" for="busquedaSirilo"></label>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="aperturaCarpeta" class="col-sm-3 col-form-label col-form-label-sm">
            Apertura de carpeta:
        </label>
        <div class="col-sm-3">
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="aperturaCarpeta" name="aperturaCarpeta" value="1"
                    {{ checked(old('aperturaCarpeta',optional($RPD->canalizacionRPD)->aperturaCarpeta)) }}
                >
                <label class="custom-control-label" for="aperturaCarpeta"></label>
            </div>
        </div>
         <label for="planBusqueda" class="col-sm-3 col-form-label col-form-label-sm">
            Planes de busqueda:
        </label>
        <div class="col-sm-3">
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="planBusqueda" name="planBusqueda" value="1"
                    {{ checked(old('planBusqueda',optional($RPD->canalizacionRPD)->planBusqueda)) }}
                >
                <label class="custom-control-label" for="planBusqueda"></label>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="entrevistaFamiliares" class="col-sm-3 col-form-label col-form-label-sm">
            Entrevista a familiares:
        </label>
        <div class="col-sm-3">
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="entrevistaFamiliares" name="entrevistaFamiliares" value="1"
                    {{ checked(old('entrevistaFamiliares',optional($RPD->canalizacionRPD)->entrevistaFamiliares)) }}
                >
                <label class="custom-control-label" for="entrevistaFamiliares"></label>
            </div>
        </div>
         <label for="AMPM" class="col-sm-3 col-form-label col-form-label-sm">
            AM/PM:
        </label>
        <div class="col-sm-3">
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="AMPM" name="AMPM" value=1
                    {{ checked(old('AMPM',optional($RPD->canalizacionRPD)->AMPM)) }}
                >
                <label class="custom-control-label" for="AMPM"></label>
            </div>
        </div>
    </div>

    <button class="btn  btn-primary btn-block btn-submit"  
            type="button"  
    >
        Guardar
    </button>
</form>