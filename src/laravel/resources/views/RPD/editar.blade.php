@extends('layouts.app')
@section('content')

	<script>
		$('#collapseDatosBasicos').collapse('show');
		$('#accordionDatoGeneral').show();
	</script>


    <div class="form-group row">
    	<div class="col-sm-6">
    		
    		<a href="{{ route('pdf.reporteRPD',$RPD->idRPD) }}" class="btn btn-success btn-block btn-sm" target="_new">
				Imprimir reporte
			</a>
    	</div>

    	<div class="col-sm-6 ">
    		<a href="{{ route('pdf.volante',$RPD->idRPD) }}" class="btn btn-success btn-block btn-sm {{ $RPD->disabledVolante() }}" target="_new"  >
				Imprimir volante
			</a>
    	</div>
    </div>

    <form action="{{ route('RPD.editar',$RPD) }}" method="POST" enctype="multipart/form-data" autocomplete="off">
        @csrf @method('PUT')
        @include('RPD._frmRPD')
        <button class="btn btn-submit btn-primary btn-block">Editar</button>
    </form>


@endsection