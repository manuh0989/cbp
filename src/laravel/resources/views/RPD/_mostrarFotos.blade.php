 <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
  <div class="carousel-inner">
  	@foreach($RPD->fotografias as $key => $foto)
	    <div class="carousel-item {{ ($key==0)?'active':'' }}">
	    	<img src="{{ Storage::disk('public')->url("RPD/{$RPD->idRPD}/$foto->foto") }}" class="d-block w-100" alt="...">
	    </div>
    @endforeach
  </div>
  <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>