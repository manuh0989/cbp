<div class="form-group row">
	<label for="nombrePD" class="col-sm-2 col-form-label col-form-label-sm">
		Nombre completo:
	</label>
	<div class="col-sm-2">
	 	<input id="nombrePD" type="text" class="toUpperCase form-control  form-control-sm @error('personaDesaparecida.nombrePD') is-invalid @enderror" name="personaDesaparecida[nombrePD]" 
	 	value="{{ old('personaDesaparecida.nombrePD',optional($RPD->personaDesaparecida)->nombrePD ) }}">
	 	@inputErrors(['input'=>'personaDesaparecida.nombrePD'])
	</div>

	<label for="primerApellidoPD" class="col-sm-2 col-form-label col-form-label-sm">
		Primer apellido:
	</label>
	<div class="col-sm-2">
	 	<input id="primerApellidoPD" type="text" class="toUpperCase form-control  form-control-sm @error('personaDesaparecida.primerApellidoPD') is-invalid @enderror" name="personaDesaparecida[primerApellidoPD]" 
	 	value="{{ old('personaDesaparecida.primerApellidoPD',optional($RPD->personaDesaparecida)->primerApellidoPD) }}">
	 	@inputErrors(['input'=>'personaDesaparecida.primerApellidoPD'])
	</div>

	<label for="segundoApellidoPD" class="col-sm-2 col-form-label col-form-label-sm">
		Segundo apellido:
	</label>
	<div class="col-sm-2">
	 	<input id="segundoApellidoPD" type="text" class="toUpperCase form-control  form-control-sm @error('segundoApellidoPD') is-invalid @enderror" name="personaDesaparecida[segundoApellidoPD]" 
	 	value="{{ old('personaDesaparecida.segundoApellidoPD',optional($RPD->personaDesaparecida)->segundoApellidoPD) }}">
	 	@inputErrors(['input'=>'personaDesaparecida.segundoApellidoPD'])
	</div>
</div>

