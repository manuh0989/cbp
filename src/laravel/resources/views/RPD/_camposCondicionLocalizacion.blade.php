    <div class="form-group row">
        <label for="idEstatusLocalizacion" class="col-sm-2 col-form-label col-form-label-sm">
            Estatus de localización:
        </label>
        <div class="col-sm-3">
            <select 
                    name             ="localizacion[idEstatusLocalizacion]" 
                    id               ="idEstatusLocalizacion" 
                    class            ="form-control form-control-sm"
                    url              ="{{ route('ajax.estatusLocalizacion',$RPD) }}"
                    onchange         ="camposEstatusLocalizacion(this)"
                    
                    idMotivoReferido ="{{ 
                        old('localizacion.idMotivoReferido',optional($RPD->condicionLocalizacion)->idMotivoReferido) }}"
                    idEstadoPersona  ="{{ old('localizacion.idEstadoPersona',optional($RPD->condicionLocalizacion)->idEstadoPersona) }}"
                    idTipoLugar      ="{{ old('localizacion.idTipoLugar',optional($RPD->condicionLocalizacion)->idTipoLugar) }}"
                    idConstancia     ="{{ old('localizacion.idConstancia',optional($RPD->condicionLocalizacion)->idConstancia) }}"
                    idDelito         ="{{ old('localizacion.idDelito',optional($RPD->condicionLocalizacion)->idDelito) }}"
                    idCausaDefuncion ="{{ old('localizacion.idCausaDefuncion',optional($RPD->condicionLocalizacion)->idCausaDefuncion) }}"
            >
                <option value="">--sin opción--</option>
                @foreach($estatusLocalizaciones as $key => $estatusLocalizacion)
                    <option value="{{ $estatusLocalizacion->idEstatusLocalizacion }}"
                        {{ selected(old('localizacion.idEstatusLocalizacion',optional($RPD->condicionLocalizacion)->idEstatusLocalizacion),$estatusLocalizacion->idEstatusLocalizacion) }}
                    >
                        {{ $estatusLocalizacion->nombreEstatusLocalizacion }}
                    </option>
                @endforeach
            </select>
            @inputErrors(['input'=>'localizacion.idEstatusLocalizacion'])
        </div>

        <label for="fechaLocalizacion" class="col-sm-2 col-form-label col-form-label-sm">
            Fecha localización:
        </label>
        <div class="col-sm-3">
           <input id="fechaLocalizacion" 
            name="localizacion[fechaLocalizacion]"
            type="text" 
            class="toUpperCase form-control  form-control-sm @error('fechaLocalizacion') is-invalid @enderror text-center" 
            value="{{ old('localizacion.fechaLocalizacion',optional($RPD->condicionLocalizacion)->fechaLocalizacionFormat) }}"
            readonly 
            >
            <script>
                $('#fechaLocalizacion').datepicker({
                    uLibrary:'bootstrap4'
                    ,format: 'dd-mm-yyyy'
                });
            </script>
            @inputErrors(['input'=>'localizacion.fechaLocalizacion'])
        </div>
    </div>

    <div id="conVidaSinVida">
        @include('RPD.condicionLocalizacion._ajaxCamposEstatusLocalizacion',[
            'RPD'                    =>$RPD
            ,'idEstatusLocalizacion' =>old('localizacion.idCondicionLocalizacion')
            ,'idTipoLugar'           =>old('localizacion.idTipoLugar')
        ])
    </div>
    
    <div class="form-group row">
        <label for="idEstadoLocalizacion" class="col-sm-2 col-form-label col-form-label-sm">
            Entidad:
        </label>
        
        <div class="col-sm-3">

            <select 
                    name              ="localizacion[idEstado]" 
                    id                ="idEstadoLocalizacion" 
                    class             ="form-control form-control-sm"
                    url               ="{{ route('ajax.municipios') }}"
                    idMunicipio       ="{{ old('localizacion.idMunicipio',optional($RPD->condicionLocalizacion)->idMunicipio) }}"
                    idLocalidad       ="{{old('localizacion.idLocalidad',optional($RPD->condicionLocalizacion)->idLocalidad)  }}"
                    idAttrLocalidad   ="idLocalidadLocalizacion"
                    idAttrMunicipio   ="idMunicipioLocalizacion"
                    nameAttrLocalidad ="localizacion[idLocalidad]"
                    nameAttrMunicipio ="localizacion[idMunicipio]"
                    divMunicipio      ="divMunicipiosLocalizacion"
                    divLocalidad      ="divLocalidadesLocalizacion"
            >
                <option value="">--sin opción--</option>

                @foreach($estados as $keyLocalizacion =>$estadoLocalizacion)
                    <option value="{{$estadoLocalizacion->idEstado }}"
                        {{ selected(old('localizacion.idEstado',optional($RPD->condicionLocalizacion)->idEstado),$estadoLocalizacion->idEstado) }}
                    >
                        {{ Str::upper($estadoLocalizacion->nombreEstado) }}
                    </option>
                @endforeach
            </select>
            @inputErrors(['input'=>'localizacion.idEstado'])
        </div>

        <label for="idMunicipioLocalizacion" class="col-sm-2 col-form-label col-form-label-sm">
            Municipio:
        </label>    

        <div class="col-sm-3" id="divMunicipiosLocalizacion">
            @include('RPD.ajax._selectMunicipios',[
                'idLocalidad'        =>old('localizacion.idLocalidad',optional($RPD->condicionLocalizacion)->idLocalidad)
                ,'idMunicipio'       =>optional($RPD->condicionLocalizacion)->idMunicipio
                ,'idAttrLocalidad'   =>"idLocalidadLocalizacion"
                ,'idAttrMunicipio'   =>'idMunicipioLocalizacion'
                ,'nameAttrLocalidad' =>"localizacion[idLocalidad]"
                ,'nameAttrMunicipio' =>"localizacion[idMunicipio]"
                ,'divMunicipio'      =>'divMunicipiosLocalizacion'
                ,'divLocalidad'      =>'divLocalidadesLocalizacion'
                ])
        </div>
    </div>

    <div class="form-group row">
        <!--<label for="idLocalidadLocalizacion" class="col-sm-2 col-form-label col-form-label-sm">
            Colonia:
        </label>
        <div class="col-sm-3" id="divLocalidadesLocalizacion">
            @include('RPD.ajax._selectLocalidades',[
                'idLocalidad'        =>old('localizacion.idLocalidad',optional($RPD->condicionLocalizacion)->idLocalidad)
                ,'idAttrLocalidad'   =>'idLocalidadLocalizacion'
                ,'nameAttrLocalidad' =>"localizacion[idLocalidad]"
            ])
            @inputErrors(['input'=>"localizacion[idLocalidad]"])
        </div>
        <label for="calleLugar" class="col-sm-2 col-form-label col-form-label-sm">
            Calle y número:
        </label>
        <div class="col-sm-3">
            <input id="calleLugar" type="text" class="toUpperCase form-control  form-control-sm @error('calleLugar') is-invalid @enderror" name="localizacion[calleLugar]" value="{{ old('localizacion.calleLugar',optional($RPD->condicionLocalizacion)->calleLugar) }}">
            @inputErrors(['input'=>'localizacion.calleLugar'])
            
        </div>-->
    </div>
    
    <div class="form-group row">
        <label for="bajaReporte" class="col-sm-2 col-form-label col-form-label-sm">
            Baja reporte:
        </label>
        <div class="col-sm-3">
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="bajaReporte" name="localizacion[bajaReporte]" value="1"
                {{ checked(old('localizacion.bajaReporte',optional($RPD->condicionLocalizacion)->bajaReporte)) }}
            >
                <label class="custom-control-label" for="bajaReporte"></label>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="observacionesLocalizacion" class="col-sm-2 col-form-label col-form-label-sm">
            Observaciones localización:
        </label>
        <div class="col-sm-6">
            <textarea name="localizacion[observacion]" id="" cols="30" rows="10" class=" form-control  form-control-sm toUpperCase" >{{ old('localizacion.observacion',optional($RPD->condicionLocalizacion)->observacion) }}</textarea>
        </div>
    </div>