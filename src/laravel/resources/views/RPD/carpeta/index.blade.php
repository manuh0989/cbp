
	<script>
		$( document ).ready(function(){
          
			$('#accordionDatoGeneral').show();
			$('#collapseCarpeta').collapse('show');

		
            $('#carpeta').keypress(function(e){
                
                let url  = $(this).attr('url');
                let code = (e.keyCode ? e.keyCode : e.which);
                if(code==13 && e.shiftKey){
                    $.ajax({
                        url:url
                        ,type:'POST'
                        ,data:{
                            carpeta:$(this).val()
                        }
                        ,dataType:'JSON'
                        ,error:function(error){

                        }
                        ,beforeSend:function(){

                        }
                        ,success:function(data){
                            $('#listCarpeta').append('<li class="list-group-item">'+data.carpeta+ '</li>');
                        }
                    }); 
                    $(this).val('');
                }
            });

             ClassicEditor.create( document.querySelector( '#carpeta' ),{
                    language: 'es'
            }).then( editor => {
                /*editor.editing.view.document.on( 'keydown', ( evt, data ) => {
                    console.log(evt);
                    if(data.keyCode==13 && evt.shiftKey){
                        alert('siiii');
                        data.preventDefault();
                        evt.stop();
                    }
                });*/
                editor.keystrokes.set( 'Shift+ENTER', ( keyEvtData, cancel ) => {
                    let url  = $('#carpeta').attr('url');
                    $.ajax({
                        url:url
                        ,type:'POST'
                        ,data:{
                            carpeta:editor.getData()
                        }
                        ,dataType:'JSON'
                        ,error:function(error){

                        }
                        ,beforeSend:function(){

                        }
                        ,success:function(data){
                            editor.setData('');
                            $('#listCarpeta').append('<li class="list-group-item">'+data.carpeta+ '</li>');
                        }
                    }); 
                    cancel();
                } );
             })
            .catch( error => {
                console.error( error );
            });

    
			
			//$('#list-4Contacto-list').tab('show');

		});

        function guardarCarpeta(){
            alert('entro');
            let url  = $('#carpeta').attr('url');
            $.ajax({
                url:url
                ,type:'POST'
                ,data:{
                    carpeta:$('.ck-content').html()
                }
                ,dataType:'JSON'
                ,error:function(error){

                }
                ,beforeSend:function(){

                }
                ,success:function(data){
                    $('.ck-content').html('');
                    $('#listCarpeta').append('<li class="list-group-item">'+data.carpeta+ '</li>');
                }
            });
        }
	</script>

@include('RPD.carpeta._carpeta')
       