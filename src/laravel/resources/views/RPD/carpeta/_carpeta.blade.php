    <form 
        action="{{ route('RPD.storeCarpeta',$RPD) }}" 
        method="POST" 
        accept-charset="utf-8" 
        autocomplete="off" 
        enctype="multipart/form-data" 
    >
        <div class="form-group row ">
                @csrf @method('POST')
                <label for="archivos" class="col-sm-2 col-form-label col-form-label-sm">
                    Archivos:
                </label>
                <div class="col-sm-6">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="archivos" name="archivos[]" multiple="">
                        <label class="custom-file-label" for="archivos">Subir Archivos</label>
                    </div>

                </div>
                <div class="col-sm-4">
                    <button type="submit" class="btn btn-primary mb-2 btn-submit">
                        <i data-feather="upload"></i>
                    </button>
                </div>   
        </div>
    </form>

    <div class="form-group row">
        <label for="archivosCarpeta" class="col-sm-2 col-form-label col-form-label-sm">
            Archivos carpeta:
        </label>
        <div class="col-sm-10">
            <div class="list-group">
               
                @table(['encabezados'=>['Nombre archivo','Acciones'],'idTable'=>'archivosCarpetaRPD'])
                    @foreach($RPD->allFilesCarpeta as $key =>$archivo)
                        <tr>
                            <td>{{ $RPD->getNameFileCarpeta($archivo) }}</td>
                            <td>
                                <div class="list-inline">
                                    <a 
                                        href="{{ asset("storage/{$archivo}") }}"
                                        class="btn btn-datatable btn-icon btn-transparent-dark mr-2" 
                                        download="{{ $RPD->getNameFileCarpeta($archivo) }}"
                                    >
                                        <i data-feather="download"></i>
                                    </a>
                                    <button 
                                        class="btn btn-datatable btn-icon btn-transparent-dark mr-2 btn-submit" 
                                        archivo="{{ $archivo }}"
                                        onclick="trashFile(this);"
                                        frmTrash="frmBorrarFileCarpeta"
                                    >
                                        <i data-feather="x"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endtable

            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="carpeta" class="col-sm-2 col-form-label col-form-label-sm">
            Carpeta:
        </label>
        <div class="col-sm-10">
            <textarea name="carpeta"  url="{{ route('RPD.storeCarpeta',$RPD) }}"id="carpeta" cols="10" rows="2" class="form-control"></textarea>
            @if((new App\Models\SistemaOperativo)->isMovil())
                <button class="btn btn-primary" id="btnCarpeta" onclick="guardarCarpeta()">
                    Guardar
                </button>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-12">
            <ul class="list-group" id="listCarpeta">
                @foreach($RPD->carpetasRPD()->with('usuario')->get() as $key =>$carpeta)
                    <li class="list-group-item">
                        {!!$carpeta->carpetaText !!}
                    </li>
                @endforeach
            </ul>
        </div>
    </div>

    <form action="{{route('RPD.borrarFileCarpeta')  }}" method="POST" id="frmBorrarFileCarpeta">
        @csrf @method('POST')
        <input type="hidden" name="archivo" value="" id="RPDCarpetaArchivo">
    </form>