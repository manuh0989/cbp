@extends('layouts.app')

@section('content')

    <form action="{{ route('RPD.registro') }}" method="POST" enctype="multipart/form-data" autocomplete="off">
        @csrf @method('POST')
		
        @include('RPD._frmRPD')
		
        <button class="btn btn-submit btn-primary btn-block">Guardar</button>
    </form>

@endsection
