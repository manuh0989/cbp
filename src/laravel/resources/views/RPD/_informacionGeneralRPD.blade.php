<div class="nav accordion" id="accordionDatoGeneral" style="display: none;">
    <div id="toastNoAutohide">
        <div class="sidenav-menu-heading">
            <i data-feather="info"></i>
            <strong class="mr-auto">Información general</strong>
            <small class="text-muted ml-2"></small>
        </div>
        <div class="" id="bodyToastInfoGeneral">
        	<ul>
                <li>
                    Nombre de persona desaparecida:
                    <small id="nombreCompletoList">
                        {{ optional(optional($RPD ?? '')->personaDesaparecida)->nombreCompletoPD  }}
                    </small>
                </li>
                <li>
                    Estatus: 
                    <small id="statusList">
                        {{ optional($RPD ?? '')->statusLocalizacion }}
                    </small>
                </li>
                <li>
                    Fecha desaparición:

                    <small id="folioCBPList">
                        {{ optional($RPD ?? '' )->datoGeneralRPD->fechaDesaparicionFormat ?? '' }}
                    </small>
                </li>
                <li>Fecha registro: 
                    <small id="fechaAltaList">{{ optional($RPD ?? '')->fechaRegistroFormat }}</small>
                </li>
            </ul>
        </div>
    </div>
</div>
