<div class="form-group row">
	<label for="fechaDesaparicion" class="col-sm-2 col-form-label col-form-label-sm">
		Fecha desaparición:
	</label>
	
	<div class="col-sm-3">
		
	 	<input id="fechaDesaparicion" type="text" 
	 	class="toUpperCase form-control  form-control-sm @error('fechaDesaparicion') is-invalid @enderror text-center " 
	 	name="personaDesaparecida[fechaDesaparicion]"
	 	value="{{ old('personaDesaparecida.fechaDesaparicion',optional($RPD->datoGeneralRPD)->fechaDesaparicionFormat) }}"
	 	readonly
	 	>
	 	@inputErrors(['input'=>'personaDesaparecida.fechaDesaparicion'])
	 	
	 	<script>
	 		$('#fechaDesaparicion').datepicker({
	 			
	 			 uLibrary:'bootstrap4'
	 			,format: 'dd-mm-yyyy'
	 		});
	 	</script>
	</div>
	

	<label for="estado" class="col-sm-2 col-form-label col-form-label-sm">
		Hora desaparición:
	</label>	

	<div class="col-sm-3">
	 	<input id="horaDesaparicion" type="text" class="toUpperCase form-control  form-control-sm @error('horaDesaparicion') is-invalid @enderror" name="personaDesaparecida[horaDesaparicion]" 
	 	value="{{ old('personaDesaparecida.horaDesaparicion',optional($RPD->datoGeneralRPD)->horaDesaparicionFormat	) }}"
	 	readonly
	 	>
	 	@inputErrors(['input'=>'personaDesaparecida.horaDesaparicion'])
	 	
	 	<script>
	 		$('#horaDesaparicion').timepicker();
	 	</script>
	</div>
</div>

<div class="form-group row">
	<label for="estado" class="col-sm-2 col-form-label col-form-label-sm">
			Estado desaparición
	</label>
	<div class="col-sm-3">
	 	<select 
				name              ="personaDesaparecida[idEstado]" 
				id                ="idEstadoPersonaDesaparecida" 
				class             ="form-control form-control-sm"
				url               ="{{ route('ajax.municipios') }}"
				idMunicipio       ="{{ old('personaDesaparecida.idMunicipio',optional($RPD->datoGeneralRPD)->idMunicipio) }}"
				idLocalidad       ="{{old('personaDesaparecida.idLocalidad',optional($RPD->datoGeneralRPD)->idLocalidad)  }}"
				idAttrLocalidad   ="idLocalidadPersonaDesaparecida"
				idAttrMunicipio   ="idMunicipioPersonaDesaparecida"
				nameAttrLocalidad ="personaDesaparecida[idLocalidad]"
				nameAttrMunicipio ="personaDesaparecida[idMunicipio]"
				divMunicipio      ="divMunicipiospersonaDesaparecida"
				divLocalidad      ="divLocalidadespersonaDesaparecida"
	 	>
	 		<option value="">--sin opción--</option>
	 		@foreach($estados as $key =>$estado)
	 			<option value="{{$estado->idEstado }}"
	 				{{ selected(old('personaDesaparecida.idEstado',optional($RPD->datoGeneralRPD)->idEstado),$estado->idEstado) }}>
	 				{{ Str::upper($estado->nombreEstado) }}
	 			</option>
	 		@endforeach
	 	</select>
	 	@inputErrors(['input'=>'personaDesaparecida.idEstado'])
	</div>
	<label for="idMunicipiopersonaDesaparecida" class="col-sm-2 col-form-label col-form-label-sm">
		Municipio:
	</label>	
	<div class="col-sm-3" id="divMunicipiospersonaDesaparecida">
		@include('RPD.ajax._selectMunicipios',[
			'idLocalidad'        =>old('personaDesaparecida.idLocalidad',optional($RPD->datoGeneralRPD)->idLocalidad)
			,'idMunicipio'       =>optional($RPD->datoGeneralRPD)->idMunicipio
			,'idAttrLocalidad'   =>"idLocalidadpersonaDesaparecida"
			,'idAttrMunicipio'   =>'idMunicipiopersonaDesaparecida'
			,'nameAttrLocalidad' =>"personaDesaparecida[idLocalidad]"
			,'nameAttrMunicipio' =>"personaDesaparecida[idMunicipio]"
			,'divMunicipio'      =>'divMunicipiospersonaDesaparecida'
			,'divLocalidad'      =>'divLocalidadespersonaDesaparecida'
			])
	</div>
</div>

<div class="form-group row">
	<label for="idLocalidadpersonaDesaparecida" class="col-sm-2 col-form-label col-form-label-sm">
		Colonia:
	</label>	
	<div class="col-sm-3" id="divLocalidadespersonaDesaparecida">
		@include('RPD.ajax._selectLocalidades',[
			'idLocalidad'        =>old('personaDesaparecida.idLocalidad',optional($RPD->datoGeneralRPD)->idLocalidad)
			,'idAttrLocalidad'   =>'idLocalidadpersonaDesaparecida'
			,'nameAttrLocalidad' =>"personaDesaparecida[idLocalidad]"
		])
	 	@inputErrors(['input'=>'idLocalidad'])
	</div>
	<label for="calleNumeroDesaparicion" class="col-sm-2 col-form-label col-form-label-sm">
		Calle y número:
	</label>
	<div class="col-sm-3">
	 	<input 

	 	id="calleNumeroDesaparicion" 
	 	type="text" 
	 	class="toUpperCase form-control  form-control-sm @error('calleNumeroDesaparicion') is-invalid @enderror" 
	 	name="personaDesaparecida[calleNumeroDesaparicion]" 
	 	value="{{ old('personaDesaparecida.calleNumeroDesaparicion',optional($RPD->datoGeneralRPD)->calleNumeroDesaparicion) }}">
	 	@inputErrors(['input'=>'calleNumeroDesaparicion'])
	 	
	</div>
</div>