<select 
		name              ="{{$nameAttrMunicipio}}" 
		id                ="{{ $idAttrMunicipio }}" 
		class             ="form-control form-control-sm" 
		idLocalidad       ="{{ $idLocalidad ?? '' }}" 
		url               ="{{ route('ajax.localidades') }}"
		idAttrLocalidad   ="{{ $idAttrLocalidad }}"
		nameAttrLocalidad ="{{ $nameAttrLocalidad }}"
		divMunicipio      ="{{ $divMunicipio }}"
		divLocalidad      ="{{ $divLocalidad }}"
		onchange          ="ajaxLocalidades(this);"
>
	<option value="">--sin opción--</option>
	@foreach($municipios??[] as $key =>$municipio)
		<option value="{{ $municipio->idMunicipio }}"
			{{ selected($idMunicipio,$municipio->idMunicipio) }}
		>
			{{ Str::upper($municipio->nombreMunicipio) }}
		</option>
	 @endforeach
</select>
@inputErrors(['input'=>'municipioDesaparicion'])