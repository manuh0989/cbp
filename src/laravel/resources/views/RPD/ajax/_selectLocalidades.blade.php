<select name="{{ $nameAttrLocalidad }}" id="{{ $idAttrLocalidad }}" class="form-control form-control-sm">
	<option value="">--sin opción--</option>

	@foreach($localidades??[] as $key =>$localidad)

		<option value="{{ $localidad->idLocalidad }}"
			{{ selected($idLocalidad,$localidad->idLocalidad) }}
		>
			{{ Str::upper($localidad->nombreLocalidad) }}
		</option>
	 @endforeach
</select>
@inputErrors(['input'=>$nameAttrLocalidad])