@extends('layouts.app')
@section('scripts')
	<script>
		$( document ).ready(function(){
			$('#listaRPD').DataTable({
				searching: false,
				select: false,
				paginate:false,
				bInfo:false,
				order: [[ 0, "desc" ]],
				columnDefs: [
					{ "width": "15%", "targets": 1 }
					//{ "width": "20%" },
				]
	    	});


		});
		function disabledFechas(obj){
	    	if(obj.checked){
	    		$('.fechasDesaparicion').removeAttr('disabled');
	    	}else{
	    		$('.fechasDesaparicion').attr('disabled','true');
	    	}
	    }
	</script>
@endsection
@section('content')

	@card(['header'=>'RPD'])
		
		
		@include('RPD._camposBusqueda')
		@table(['encabezados'=>$encabezados,'idTable'=>'listaRPD'])
			@foreach($listaRPD as $key =>$rpd)
				<tr>
					<td>{{ $rpd->idRPD }}</td>
					<td style="width: 50px;">{{ $rpd->folioCBP }}</td>
					<td>{{ $rpd->folioFIPEDE }}</td>
					<td>{{ optional($rpd->personaDesaparecida)->nombreCompletoPD }}</td>
					<td class="text-right">
						{{ optional($rpd->datoGeneralRPD)->fechaDesaparicionFormat }}
					</td>
					<td class="text-right">{{ $rpd->fechaActualizacionFormat }}</td>
					<td>
						<span class="badge badge-{{ ($rpd->localizado)?'green':'red' }}">
							{{ $rpd->statusLocalizacion }}
						</span>
					</td>
					
					<td>
						<div class="list-inline">
							<a  href="{{route('pdf.reporteRPD',$rpd)  }}" 
								class="btn btn-datatable btn-icon btn-transparent-dark mr-2" 
								target="_new" 
								alt="Reporte Cedula"
								data-toggle="tooltip" data-placement="top" title="" data-original-title="Reporte Cédula"
							>
								<i data-feather="file-text"></i>
							</a>

							<a  href="{{route('pdf.volante',$rpd->idRPD)   }}" 
								class="btn btn-datatable btn-icon btn-transparent-dark mr-2 {{ $rpd->disabledVolante()  }}" 
								target="_new"
								data-toggle="tooltip" data-placement="top" title="" data-original-title="Reporte Volante"
							
							>
								<i data-feather="file-text"></i>
							</a>
							
							@can('update', auth()->user())
								<a  href="{{route('RPD.editar',$rpd)  }}" class="btn btn-datatable btn-icon btn-transparent-dark mr-2 btn-submit" >
									<i data-feather="edit"></i>
								</a>
							@endcan
							
							@can('delete',auth()->user())
								<button  class="btn btn-datatable btn-icon btn-transparent-dark mr-2 btn-submit" 
								id="{{ $rpd->idRPD }}"
								frmTrash="frmTrashRPD"
								onclick="trash(this)">
									<i data-feather="trash"></i>
								</button>
							@endcan
						</div>
					</td>

					<td>
						@can('seguimiento',auth()->user())
							
							<div class="list-inline">
								<a  href="{{ route('RPD.seguimiento',$rpd) }}" 
									class="btn btn-datatable btn-icon btn-transparent-dark mr-2 btn-submit" 
									alt="Reporte Cedula"
									data-toggle="tooltip" data-placement="top" title="" data-original-title="Dar seguimiento"
								>
									<i data-feather="arrow-right"></i>
								</a>
							</div>
						
						@endcan
					</td>
				</tr>
			@endforeach
		@endtable
		{{ $listaRPD->links() }}
	@endcard

	<form action="{{route('RPD.trash',':id')  }}" method="POST" id="frmTrashRPD">
		@csrf @method('PUT')
	</form>
@endsection