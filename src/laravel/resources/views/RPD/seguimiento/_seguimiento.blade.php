<div class="row">
    <div class="col-4">
        <div class="list-group" 
            id="list-contacto" 
            role="tablist" 
            url="{{ route('ajax.segimientoContacto',$RPD) }}"
        >
    		<a class="list-group-item list-group-item-action active" id="list-1Contacto-list" data-toggle="list" href="#list-1Contacto" contacto="1" role="tab" aria-controls="home">
    			1er Contacto
    		</a>
    		<a class="list-group-item list-group-item-action " id="list-2Contacto-list" data-toggle="list" href="#list-2Contacto" contacto="2" role="tab" aria-controls="profile">
    			2do Contacto
    		</a>
    		<a class="list-group-item list-group-item-action " id="list-3Contacto-list" data-toggle="list" href="#list-3Contacto" contacto="3" role="tab" aria-controls="messages">
    			3er Contacto
    		</a>
    		<a class="list-group-item list-group-item-action " id="list-4Contacto-list" data-toggle="list" href="#list-4Contacto"  contacto="4"role="tab" aria-controls="settings">
    			4to Contacto
    		</a>
        </div>
    </div>

    <div class="col-8">
        <div class="tab-content" id="nav-tabContent">
            @for($i=1;$i<5;$i++)
                    <div class="tab-pane fade {{ ($i==1)?"show active":'' }} contentSeguimiento" 
                        id="list-{{ $i }}Contacto" 
                        role="tabpanel" 
                        aria-labelledby="list-{{ $i }}Contacto-list"
                    >
                    </div>
            @endfor
        </div>
    </div>
</div>