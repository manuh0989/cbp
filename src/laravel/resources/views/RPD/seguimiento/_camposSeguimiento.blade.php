<script>
	
	function guardarSeguimiento(){
		let frm =$('#frmSeguimiento');
		let url =frm.attr('action');
		$.ajax({
	        url:url
	        ,type:'POST'
	        ,data:frm.serialize()
	        ,dataType:'JSON'
	        ,error:function(error){
				let errors     =error.responseJSON
				var errorsHtml = '';
				$.each(errors.errors,function (k,v) {
					errorsHtml += '<li>'+ v + '</li>';
				});
				$('#btnGuardarSeguimiento').removeAttr('disabled');
	        	$('#btnGuardarSeguimiento').html('Editar');
				$('#message').show();
				$('#message div').addClass('alert-danger');
				$('#message div ul').html(errorsHtml);

	        }
	        ,beforeSend:function(){

				$('#btnGuardarSeguimiento').html(spinner());
				$('#btnGuardarSeguimiento').attr('disabled','disabled');
	        }
	        ,success:function(data){
	        	$('#btnGuardarSeguimiento').removeAttr('disabled');
	        	$('#btnGuardarSeguimiento').html('Editar');
	        	$('#message').show();
	        	$('#message div').removeClass('alert-danger');
	        	$('#message div').addClass('alert-success');
	        	$('#message div ul').html(data.message)
	          
	        }
	    });
	}

</script>
<div id="message" style="display:none;">
	<div class="alert ">
		<ul></ul>
	</div>
</div>
<form action="{{ route('RPD.storeSeguimiento',$RPD) }}" method="POST"  autocomplete="off" id="frmSeguimiento">
	@csrf @method('POST')

	<div class="form-group row">
		<label for="fechaContactoSeguimiento" class="col-sm-2 col-form-label col-form-label-sm">
			Fecha contacto:
		</label>
		<div class="col-sm-3">
			<input id="fechaContactoSeguimiento" 
					name="fechaContactoSeguimiento"
					type="text" 
					class="toUpperCase form-control  form-control-sm @error('fechaContactoSeguimiento') is-invalid @enderror" 
					value="{{ old('fechaContactoSeguimiento',optional($seguimiento)->fechaContactoSeguimientoFormat) }}"
			>
			<script>
		 		$('#fechaContactoSeguimiento').datepicker({
		 			uLibrary:'bootstrap4'
		 			,format: 'dd-mm-yyyy'
		 		});
		 	</script>
			@inputErrors(['input'=>'fechaContactoSeguimiento'])
		</div>
		<label for="idEstatusSeguimientoRPD" class="col-sm-2 col-form-label col-form-label-sm">
			Estatus:
		</label>
		<div class="col-sm-3">
			<select name="idEstatusSeguimientoRPD" 
					id="idEstatusSeguimientoRPD" 
					class="form-control form-control-sm"
			>
				<option value="">--sin opción--</option>
				@foreach($estatusSeguimientosRPD as $key => $estatusSeguimientoRPD)
					<option value="{{ $estatusSeguimientoRPD->idEstatusSeguimientoRPD }}"
						{{ selected(old('idEstatusSeguimientoRPD',optional($seguimiento)->idEstatusSeguimientoRPD),$estatusSeguimientoRPD->idEstatusSeguimientoRPD) }}
					>
						{{ $estatusSeguimientoRPD->nombreEstatusSeguimiento }}
					</option>
				@endforeach
			</select>
			@inputErrors(['input'=>'estatusSeguimientoRPD'])
		</div>
	</div>

	<div class="form-group row">
		<label for="descripcionSeguimiento" class="col-sm-2 col-form-label col-form-label-sm">
			Descripcion:
		</label>
		<div class="col-sm-12">
			<textarea  onkeypress="$(this).val($(this).val().toUpperCase());"
			name="descripcionSeguimiento" id="descripcionSeguimiento" cols="30" rows="10" class="form-control form-control-sm toUpperCase @error('descripcionSeguimiento') is-invalid @enderror" >{{ optional($seguimiento)->descripcionSeguimiento }}</textarea>
			@inputErrors(['input'=>'descripcionSeguimiento'])
		</div>
	</div>

	<input type="hidden" value="{{ $contacto }}" name="numeroContacto">
	<input type="hidden" value="{{ $RPD->idRPD }}" name="idRPD">
	<button class="btn btn-submit btn-primary btn-block" 
			type="button" id="btnGuardarSeguimiento" 
			onclick="guardarSeguimiento(this)">
		Editar
	</button>
</form>
