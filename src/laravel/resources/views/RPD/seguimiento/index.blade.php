@extends('layouts.app')


@section('content')
	<script>
		$( document ).ready(function(){
            //ajaxMunicipios();
			$('#accordionDatoGeneral').show();
			$('#collapseSeguimiento').collapse('show');

			//$('#list-4Contacto-list').tab('show');

			$('#list-contacto a').on('click', function (e) {
				e.preventDefault();
				let contacto=$(this).attr('contacto');
				ajaxContacto(contacto);
				$(this).tab('show')
			});
            
            $('#idEstado').change(function(event){
                ajaxMunicipios();
            });	
		});

		function ajaxContacto(contacto){
			let url = $('#list-contacto ').attr('url');
			$.ajax({
                url:url
                ,type:'POST'
                ,data:{
                	contacto:contacto
                }
                ,dataType:'JSON'
                ,beforeSend:function(){
                	$('.contentSeguimiento').html('');
                }
                ,success:function(data){
                  $('#list-'+data.contacto+'Contacto').html(data.view);
                }
                ,error:function(data){
                    $('#list-'+data.responseJSON.contacto+'Contacto').html(data.responseJSON.view);
                }
            });
		}
	</script>

   <!-- <div class="card card-collapsable">
        <a class="card-header" href="#collapseCardSeguimiento" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardSeguimiento">
            Seguimiento
            <div class="card-collapsable-arrow">
                <i class="fas fa-chevron-down"></i>
            </div>
        </a>
        <div class="collapse show" id="collapseCardSeguimiento">
            <div class="card-body">
                @include('RPD.seguimiento._seguimiento')
            </div>
        </div>
    </div>
-->


    <div class="card card-collapsable">
        <a class="card-header" href="#collapseCardCarpeta" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardCarpeta">
            Bitácora de acciones
            <div class="card-collapsable-arrow">
                <i class="fas fa-chevron-down"></i>
            </div>
        </a>
        <div class="collapse show" id="collapseCardCarpeta">
            <div class="card-body">
                @include('RPD.carpeta.index')
            </div>
        </div>
    </div>

    <div class="card card-collapsable">
        <a class="card-header" href="#collapseCardCanalizar" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardCanalizar">
            Diligencias
            <div class="card-collapsable-arrow">
                <i class="fas fa-chevron-down"></i>
            </div>
        </a>
        <div class="collapse show" id="collapseCardCanalizar">
            <div class="card-body">
                @include('RPD.canalizar.index')
            </div>
        </div>
    </div>
@endsection