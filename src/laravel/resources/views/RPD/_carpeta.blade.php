<form action="{{ route('RPD.storeCarpeta',$RPD) }}" method="POST" accept-charset="utf-8" autocomplete="off" enctype="multipart/form-data"
>
    @csrf @method('POST')
    
    
    <div class="form-group row">
        <label for="archivos" class="col-sm-2 col-form-label col-form-label-sm">
            Archivos:
        </label>
        <div class="col-sm-5">
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="archivos" name="archivos[]" multiple="">
                <label class="custom-file-label" for="archivos">Subir Archivos</label>
            </div>
        </div>
        <div class="col-sm-5">
            <div class="list-group">
                @foreach (Storage::disk('public')->allFiles($RPD->rutaCarpeta); as $key =>$archivo)
                    <a 
                        href="{{ Storage::disk('public')->url($archivo) }}" 
                        class="list-group-item list-group-item-action"
                        download="archivo.{{ pathinfo($archivo, PATHINFO_EXTENSION) }}"
                    >
                        Archivo: {{ $key+1 }}
                    </a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="carpeta" class="col-sm-2 col-form-label col-form-label-sm">
            Carpeta:
        </label>
        <div class="col-sm-10">
            <textarea name="carpeta"  url="{{ route('RPD.storeCarpeta',$RPD) }}"id="carpeta" cols="10" rows="2" class="form-control"></textarea>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-12">
            <ul class="list-group" id="listCarpeta">
                @foreach($RPD->carpetasRPD()->with('usuario')->get() as $key =>$carpeta)
                    <li class="list-group-item">
                        {!!$carpeta->carpetaText !!}
                    </li>
                @endforeach
            </ul>
        </div>
    </div>

    <button class="btn  btn-primary btn-block btn-submit"  
            type="button"  
    >
        Guardar
    </button>
</form>