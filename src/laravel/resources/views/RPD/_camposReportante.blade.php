<div class="form-group row">
	<label for="nombreCompletoReportante" class="col-sm-2 col-form-label col-form-label-sm">
		Nombre completo:
	</label>
	<div class="col-sm-3">
		<input id="nombreCompletoReportante" type="text" class="toUpperCase form-control  form-control-sm @error('reportante.nombreCompletoReportante') is-invalid @enderror" name="reportante[nombreCompletoReportante]" 
		value="{{ old('reportante.nombreCompletoReportante',optional($RPD->reportanteRPD)->nombreCompletoReportante) }}">
			@inputErrors(['input'=>'reportante.nombreCompletoReportante'])
	</div>
	<!--<label for="edadReportante" class="col-xs-3 col-form-label col-form-label-sm">
		Edad:
	</label>
	<div class="col-xs-3">
		<input id="edadReportante" type="text" class="toUpperCase form-control  form-control-sm @error('edadReportante') is-invalid @enderror" name="reportante[edadReportante]" 
		value="{{ old('reportante.edadReportante',optional($RPD->reportanteRPD)->edadReportante) }}"
		size="3"
		>
		@inputErrors(['input'=>'reportante.edadReportante'])
	</div>-->
	<label for="idParentesco" class="col-sm-2 col-form-label col-form-label-sm">
		Parentesco:
	</label>
	<div class="col-sm-3">
		<select name="reportante[idParentesco]" id="idParentesco" class="form-control form-control-sm">
			<option value="">--sin opción--</option>
			@foreach($parentescos as $key => $parentesco)
				<option value="{{ $parentesco->idParentesco }}"
					{{ selected( old('reportante.idParentesco',optional($RPD->reportanteRPD)->idParentesco),$parentesco->idParentesco) }}>
					{{ $parentesco->nombreParentesco }}
				</option>
			@endforeach
		</select>
		@inputErrors(['input'=>'reportante.idParentesco'])
	</div>

</div>

<div class="form-group row">
	<label for="telefonoReportante"class="col-sm-2 col-form-label col-form-label-sm">
		Teléfono:
	</label>
	<div class="col-sm-3">
		<input id="telefonoReportante" type="text" class="phone-mask toUpperCase form-control  form-control-sm @error('telefonoReportante') is-invalid @enderror" name="reportante[telefonoReportante]" placeholder="(00) 0000-0000" 
		value="{{ old('reportante.telefonoReportante',optional($RPD->reportanteRPD)->telefonoReportante) }}">
		@inputErrors(['input'=>'reportante.telefonoReportante'])
	</div>
	<label for="telefono2Reportante"class="col-sm-2 col-form-label col-form-label-sm ">
		Teléfono2:
	</label>
	<div class="col-sm-3">
		<input id="telefono2Reportante" type="text" class=" phone-mask toUpperCase form-control  form-control-sm @error('telefono2Reportante') is-invalid @enderror" name="reportante[telefono2Reportante]" placeholder="(00) 0000-0000" 
		value="{{ old('reportante.telefono2Reportante',optional($RPD->reportanteRPD)->telefono2Reportante) }}">
		@inputErrors(['input'=>'reportante.telefono2Reportante'])
	</div>
</div>

<div class="form-group row">
	

	<!--<label for="telefono3Reportante"class="col-sm-2 col-form-label col-form-label-sm">
		Teléfono3:
	</label>
	<div class="col-sm-3">
		<input id="telefono3Reportante" type="text" class="phone-mask toUpperCase form-control  form-control-sm @error('telefono3Reportante') is-invalid @enderror" name="reportante[telefono3Reportante]" placeholder="(00) 0000-0000"
		value="{{ old('reportante.telefono3Reportante',optional($RPD->reportanteRPD)->telefono3Reportante) }}">
		@inputErrors(['input'=>'reportante.telefono3Reportante'])
	</div>-->
</div>

<div class="form-group row">
	<label for="correoReportante"class="col-sm-2 col-form-label col-form-label-sm">
		Correo:
	</label>
	<div class="col-sm-3">
		<input id="correoReportante" type="text" class="toUpperCase form-control  form-control-sm @error('correoReportante') is-invalid @enderror" name="reportante[correoReportante]" 
		value="{{ old('reportante.correoReportante',optional($RPD->reportanteRPD)->correoReportante) }}">
		@inputErrors(['input'=>'reportante.correoReportante'])
	</div>

	<label for="redSocialReportante"class="col-sm-2 col-form-label col-form-label-sm">
		Red social:
	</label>
	<div class="col-sm-3">
		<input id="redSocialReportante" type="text" class="toUpperCase form-control  form-control-sm @error('redSocialReportante') is-invalid @enderror" name="reportante[redSocialReportante]" 
		value="{{ old('reportante.redSocialReportante',optional($RPD->reportanteRPD)->redSocialReportante) }}">
		@inputErrors(['input'=>'reportante.redSocialReportante'])
	</div>
</div>

<div class="form-group row">
	<!--<label for="redSocial2Reportante"class="col-sm-2 col-form-label col-form-label-sm">
		Red social2:
	</label>
	<div class="col-sm-3">
		<input id="redSocial2Reportante" type="text" class="toUpperCase form-control  form-control-sm @error('redSocialReportante') is-invalid @enderror" name="reportante[redSocial2Reportante]" 
		value="{{ old('reportante.redSocial2Reportante',optional($RPD->reportanteRPD)->redSocial2Reportante) }}">
		@inputErrors(['input'=>'reportante.redSocial2Reportante'])
	</div>
	-->

	<label for="otroContactoMedio"class="col-sm-2 col-form-label col-form-label-sm">
		Otro medio de contacto:
	</label>
	<div class="col-sm-10">
		<textarea name="reportante[otroContactoMedio]" id="otroContactoMedio" cols="20" rows="5"  
			class="form-control @error('otroContactoMedio') is-invalid @enderror toUpperCase"
		>{{ old('reportante.otroContactoMedio',optional($RPD->reportanteRPD)->otroContactoMedio) }}</textarea>
		@inputErrors(['input'=>'reportante.otroContactoMedio'])
	</div>
</div>

<div class="form-group row">
	<label for="RNPDNO"class="col-sm-6 col-form-label col-form-label-sm">
		Canalización sin datos de contacto:
	</label>
	 <div class="col-sm-6">
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="RNPDNO" name="reportante[RNPDNO]" value="1"
                    {{ checked(old('reportante.RNPDNO',optional($RPD->reportanteRPD)->RNPDNO)) }}
                >
                <label class="custom-control-label" for="RNPDNO"></label>
            </div>
      </div>

      <label for="contactoInicial"class="col-sm-6 col-form-label col-form-label-sm">
		No fue posible el contacto inicial con reportante:
	</label>
	 <div class="col-sm-6">
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="contactoInicial" name="reportante[contactoInicial]" value="1"
                    {{ checked(old('reportante.contactoInicial',optional($RPD->reportanteRPD)->contactoInicial)) }}
                >
                <label class="custom-control-label" for="contactoInicial"></label>
            </div>
      </div>
</div>