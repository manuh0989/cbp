
<div class="form-group row">
	<label for="idCircunstanciaDesaparicion" class="col-sm-2 col-form-label col-form-label-sm">
		Circunstancia de la desaparición:
	</label>
	<div class="col-sm-3">
	 	<select name="hechosDesaparicion[idCircunstanciaDesaparicion]" id="idCircunstanciaDesaparicion" class="form-control form-control-sm">
	 		<option value="">--sin opción--</option>
	 		@foreach($circunstanciaDesaparicion as $key =>$circunstancia)
	 			<option value="{{ $circunstancia->idCircunstanciaDesaparicion }}" 
	 				{{ selected(old('hechosDesaparicion.idCircunstanciaDesaparicion',optional($RPD->hechoDesaparicionRPD)->idCircunstanciaDesaparicion),$circunstancia->idCircunstanciaDesaparicion) }}>
	 				{{ $circunstancia->nombreCircunstanciaDesaparicion }}
	 			</option>
	 		@endforeach
	 	</select>
	 	@inputErrors(['input'=>'hechosDesaparicion.idCircunstanciaDesaparicion'])
	</div>

	<label for="idHipotesisDesaparicion" class="col-sm-2 col-form-label col-form-label-sm">
		Hipotesis
	</label>
	<div class="col-sm-3">
	 	<select name="hechosDesaparicion[idHipotesisDesaparicion]" id="idHipotesisDesaparicion" 
	 	class="form-control form-control-sm">
	 		<option value="">--sin opción--</option>
	 		@foreach($hipotesisDesaparicion as $key =>$hipotesis)
	 			<option value="{{ $hipotesis->idHipotesisDesaparicion }}" 
	 				{{ selected(old('hechosDesaparicion.idHipotesisDesaparicion',optional($RPD->hechoDesaparicionRPD)->idHipotesisDesaparicion),$hipotesis->idHipotesisDesaparicion) }}>
	 				{{ $hipotesis->nombreHipotesisDesaparicion }}
	 			</option>
	 		@endforeach
	 	</select>
	 	@inputErrors(['input'=>'hechosDesaparicion.idHipotesisDesaparicion'])
	</div>
</div>

<div class="form-group row">
	<label for="idLugarDesaparicion" class="col-sm-2 col-form-label col-form-label-sm">
		Lugar desaparición:
	</label>
	<div class="col-sm-3">
		<select name="hechosDesaparicion[idLugarDesaparicion]" id="idLugarDesaparicion" 
		class="form-control form-control-sm">
			<option value="">--sin opción--</option>
			@foreach($lugaresDesaparicion as $key => $lugarDesaparicion)
				<option value="{{ $lugarDesaparicion->idLugarDesaparicion }}"
					{{ selected(old('hechosDesaparicion.idLugarDesaparicion',optional($RPD->hechoDesaparicionRPD)->idLugarDesaparicion ),$lugarDesaparicion->idLugarDesaparicion) }}
				>
					{{ $lugarDesaparicion->nombreLugarDesaparicion }}
				</option>
			@endforeach
		</select>
	</div>
</div>

<div class="form-group row">
	<label for="descripcionHechosDesaparicion" class="col-sm-2 col-form-label col-form-label-sm">
		Descripción de los hechos:
	</label>
	<div class="col-sm-8">
		<textarea name="hechosDesaparicion[descripcionHechosDesaparicion]" id="descripcionHechosDesaparicion" cols="30" rows="10" class="form-control toUpperCase ">{{ old('hechosDesaparicion.descripcionHechosDesaparicion',optional($RPD->hechoDesaparicionRPD)->descripcionHechosDesaparicion) }}</textarea>
	</div>
</div>

<div class="form-group row">
	<label for="fotovolanteHechosDesaparicion" class="col-sm-2 col-form-label col-form-label-sm">
		Descripción fotovolante:
	</label>
	<div class="col-sm-8">
		<textarea name="hechosDesaparicion[fotovolanteHechosDesaparicion]" id="fotovolanteHechosDesaparicion" cols="30" rows="5" class="form-control toUpperCase ">{{ old('hechosDesaparicion.fotovolanteHechosDesaparicion',optional($RPD->hechoDesaparicionRPD)->fotovolanteHechosDesaparicion) }}</textarea>
	</div>
</div>