@if($idEstatusLocalizacion=='1')
    <div class="form-group row" >
        <label for="idMotivoReferido" class="col-sm-2 col-form-label col-form-label-sm">
            Motivo referido:
        </label>
        <div class="col-sm-3">
            <select name="localizacion[idMotivoReferido]" id="idMotivoReferido" class="form-control form-control-sm">
                <option value="">--sin opción--</option>
                @foreach(\App\Models\MotivoReferido::all() as $key =>$motivo)
                    <option value="{{ $motivo->idMotivoReferido }}"
                        {{ selected($idMotivoReferido,$motivo->idMotivoReferido) }}
                    >
                        {{ Str::upper($motivo->nombreMotivoReferido) }}
                    </option>
                @endforeach
            </select>
            @inputErrors(['input'=>'localizacion.idMotivoReferido'])
        </div>

        <label for="idEstadoPersona" class="col-sm-2 col-form-label col-form-label-sm">
            Estado de la persona:
        </label>
        <div class="col-sm-3">
            <select name="localizacion[idEstadoPersona]" id="idEstadoPersona" class="form-control form-control-sm">
                <option value="">--sin opción--</option>
                @foreach(\App\Models\EstadoPersona::all() as $key =>$estadoPersona)
                    <option value="{{ $estadoPersona->idEstadoPersona }}"
                        {{ selected($idEstadoPersona,$estadoPersona->idEstadoPersona) }}
                    >
                        {{ Str::upper($estadoPersona->nombreEstadoPersona) }}
                    </option>
                @endforeach
            </select>
            @inputErrors(['input'=>'localizacion.idEstadoPersona'])
        </div>
    </div>

    <div class="form-group row" >
        <label for="idTipoLugar" class="col-sm-2 col-form-label col-form-label-sm">
            Tipo lugar:
        </label>
        <div class="col-sm-3">
            <select name="localizacion[idTipoLugar]" id="idTipoLugar" class="form-control form-control-sm">
                <option value="">--sin opción--</option>
                @foreach(\App\Models\TipoLugar::all() as $key =>$lugar)
                    <option value="{{ $lugar->idTipoLugar }}"
                        {{ selected($idTipoLugar,$lugar->idTipoLugar) }}
                    >
                        {{ Str::upper($lugar->nombreTipoLugar) }}
                    </option>
                @endforeach
            </select>
            @inputErrors(['input'=>'localizacion.idTipoLugar'])
        </div>

        <label for="idConstancia" class="col-sm-2 col-form-label col-form-label-sm">
            Constancia:
        </label>
        <div class="col-sm-3">
            <select name="localizacion[idConstancia]" id="idConstancia" class="form-control form-control-sm">
                <option value="">--sin opción--</option>
                @foreach(\App\Models\Constancia::all() as $key =>$constancia)
                    <option value="{{ $constancia->idConstancia }}"
                        {{ selected($idConstancia,$constancia->idConstancia) }}
                    >
                        {{ Str::upper($constancia->nombreConstancia) }}
                    </option>
                @endforeach
            </select>
            @inputErrors(['input'=>'localizacion.idConstancia'])
        </div>
    </div>

    <div class="form-group row" >
        <label for="idDelito" class="col-sm-2 col-form-label col-form-label-sm">
            Delito:
        </label>
        <div class="col-sm-3">
            <select name="localizacion[idDelito]" id="idDelito" class="form-control form-control-sm">
                <option value="">--sin opción--</option>
                @foreach(\App\Models\Delito::all() as $key =>$delito)
                    <option value="{{ $delito->idDelito }}"
                        {{ selected($idDelito,$delito->idDelito) }}
                    >
                        {{ Str::upper($delito->nombreDelito) }}
                    </option>
                @endforeach
            </select>
            @inputErrors(['input'=>'localizacion.idDelito'])
        </div>
    </div>
@endif

@if($idEstatusLocalizacion=='2')
    <div class="form-group row" >
        <label for="idTipoLugar" class="col-sm-2 col-form-label col-form-label-sm">
            Tipo lugar:
        </label>
        <div class="col-sm-3">
            <select name="localizacion[idTipoLugar]" id="idTipoLugar" class="form-control form-control-sm">
                <option value="">--sin opción--</option>
                @foreach(\App\Models\TipoLugar::all() as $key =>$lugar)
                    <option value="{{ $lugar->idTipoLugar }}"
                        {{ selected($idTipoLugar,$lugar->idTipoLugar) }}
                    >
                        {{ Str::upper($lugar->nombreTipoLugar) }}
                    </option>
                @endforeach
            </select>
            @inputErrors(['input'=>'localizacion.idTipoLugar'])
        </div>

        <label for="idCausaDefuncion" class="col-sm-2 col-form-label col-form-label-sm">
            Causa de defunción:
        </label>
        <div class="col-sm-3">
            <select name="localizacion[idCausaDefuncion]" id="idTipoLugar" class="form-control form-control-sm">
                <option value="">--sin opción--</option>
                @foreach(\App\Models\CausaDefuncion::all() as $key =>$causadefuncion)
                    <option value="{{ $causadefuncion->idCausaDefuncion }}"
                        {{ selected($idCausaDefuncion,$causadefuncion->idCausaDefuncion) }}
                    >
                        {{ Str::upper($causadefuncion->nombreCausaDefuncion) }}
                    </option>
                @endforeach
            </select>
            @inputErrors(['input'=>'localizacion.idCausaDefuncion'])
        </div>
    </div>
    <div class="form-group row" >
        <label for="delitoCometido" class="col-sm-2 col-form-label col-form-label-sm">
            Delito cometido:
        </label>
        <div class="col-sm-3">
             <select name="localizacion[idDelito]" id="idConstancia" class="form-control form-control-sm">
                <option value="">--sin opción--</option>
                @foreach(\App\Models\Delito::all() as $key =>$delito)
                    <option value="{{ $delito->idDelito }}"
                        {{ selected($idDelito,$delito->idDelito) }}
                    >
                        {{ Str::upper($delito->nombreDelito) }}
                    </option>
                @endforeach
            </select>
            @inputErrors(['input'=>'idDelito'])
        </div>
    </div>
@endif
