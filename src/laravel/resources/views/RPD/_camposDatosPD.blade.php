<div class="form-group row">
	<label for="fechaNacimientoPD" class="col-sm-2 col-form-label col-form-label-sm">
		Fecha nacimiento:
	</label>

	<div class="col-sm-3">
	 	<input id="fechaNacimientoPD" type="text" 
	 	class="date-mask form-control  form-control-sm @error('fechaNacimientoPD') is-invalid @enderror text-right" 
	 	name="personaDesaparecida[fechaNacimientoPD]" placeholder="dd-mm-aaaa" 
	 	url="{{ route('ajax.calcularEdad') }}"
	 	value="{{ old('personaDesaparecida.fechaNacimientoPD',optional($RPD->personaDesaparecida)->fechaNacimientoPDFormat) }}">
	 	@inputErrors(['input'=>'personaDesaparecida.fechaNacimientoPD'])
	</div>

	<label for="fechaNacimientoPD" class="col-xs-2 col-form-label col-form-label-sm">
		Edad:
	</label>
	<div class="col-xs-3">
	 	<input 
	 	id="edadPD" 
	 	type="text" 
	 	class="toUpperCase form-control  form-control-sm @error('edadPD') is-invalid @enderror" 
	 	name="personaDesaparecida[edadPD]"  size="3"
	 		value="{{ old('personaDesaparecida.edadPD',optional($RPD->personaDesaparecida)->edadPD) }}">
	 	@inputErrors(['input'=>'personaDesaparecida.edadPD'])
	</div>

	<label for="idNacionalidad" class="col-sm-2 col-form-label col-form-label-sm">
		Nacionalidad:
	</label>

	<div class="col-sm-3">
		<select name="personaDesaparecida[idNacionalidad]" id="idNacionalidad" class="form-control form-control-sm">
			<option value="">--sin opción--</option>

			@foreach($nacionalidades as $key =>$nacionalidad)
				<option value="{{ $nacionalidad->idNacionalidad }}" 
					{{ selected(old('personaDesaparecida.idNacionalidad',optional($RPD->personaDesaparecida)->idNacionalidad),$nacionalidad->idNacionalidad) }}
				>
					{{ Str::upper($nacionalidad->nombreNacionalidad) }}
				</option>
			@endforeach
		</select>
	</div>
</div>

<div class="form-group row">
	<label for="lugarNacimientoPD" class="col-sm-2 col-form-label col-form-label-sm">
		Lugar nacimiento:
	</label>
	<div class="col-sm-8">
		
		<select name="personaDesaparecida[lugarNacimientoPD]" id="lugarNacimientoPD" class="form-control form-control-sm">
	 		<option value="">--sin opción--</option>

	 		@foreach($estados as $key =>$estado)
	 			<option value="{{ Str::upper($estado->nombreEstado) }}" 
	 				{{ selected(old('personaDesaparecida.lugarNacimientoPD',optional($RPD->personaDesaparecida)->lugarNacimientoPD),str::upper($estado->nombreEstado)) }}
	 			>
	 				{{ Str::upper($estado->nombreEstado) }}
	 			</option>
	 		@endforeach
	 	</select>
	 	@inputErrors(['input'=>'personaDesaparecida.lugarNacimientoPD'])
	</div>
</div>

<div class="form-group row">
	<label for="telefonoPD" class="col-sm-2 col-form-label col-form-label-sm">
		Teléfono:
	</label>
	<div class="col-sm-3">
	 	<input 
	 	id="telefonoPD" 
	 	type="text" 
	 	class="toUpperCase form-control  phone-mask form-control-sm @error('personaDesaparecida.telefonoPD') is-invalid @enderror" 
	 	name="personaDesaparecida[telefonoPD]"
	 		value="{{ old('personaDesaparecida.telefonoPD',optional($RPD->personaDesaparecida)->telefonoPD) }}">
	 	@inputErrors(['input'=>'personaDesaparecida.telefonoPD'])
	</div>
	<label for="redSocialPD" class="col-sm-2 col-form-label col-form-label-sm">
		Red social:
	</label>
	<div class="col-sm-3">
	 	<input 
	 	id="redSocialPD" 
	 	type="text" 
	 	class="toUpperCase form-control form-control-sm @error('personaDesaparecida.redSocialPD') is-invalid @enderror" 
	 	name="personaDesaparecida[redSocialPD]"
	 		value="{{ old('personaDesaparecida.redSocialPD',optional($RPD->personaDesaparecida)->redSocialPD) }}">
	 	@inputErrors(['input'=>'personaDesaparecida.redSocialPD'])
	</div>
</div>


<div class="form-group row">
	<label for="CURPPD" class="col-sm-2 col-form-label col-form-label-sm">
		CURP:
	</label>
	<div class="col-sm-3">
	 	<input id="CURPPD" type="text" class="toUpperCase form-control  form-control-sm @error('CURPPD') is-invalid @enderror" 
	 		name="personaDesaparecida[CURPPD]" 
	 		value="{{ old('personaDesaparecida.CURPPD',optional($RPD->personaDesaparecida)->CURPPD) }}">
	 	@inputErrors(['input'=>'personaDesaparecida.CURPPD'])
	</div>
	 
	<label for="RFCPD" class="col-sm-2 col-form-label col-form-label-sm">
		RFC:
	</label>
	<div class="col-sm-3">
	 	<input id="RFCPD" type="text" class=" toUpperCase form-control  form-control-sm @error('RFCPD') is-invalid @enderror" 
	 	name="personaDesaparecida[RFCPD]" 
	 		value="{{ old('personaDesaparecida.RFCPD',optional($RPD->personaDesaparecida)->RFCPD) }}">
	 	@inputErrors(['input'=>'personaDesaparecida.RFCPD'])
	</div>
</div>

<div class="form-group row">
	<label for="idSexo" class="col-sm-2 col-form-label col-form-label-sm">
		Sexo:
	</label>
	<div class="col-sm-3">
	 	<select name="personaDesaparecida[idSexo]" id="idSexo" class="form-control form-control-sm">
	 		<option value="">--sin opción--</option>
			@foreach($sexos as $key=>$sexo)
				<option value="{{ $sexo->idSexo }}" 
					{{ selected(old('personaDesaparecida.idSexo',optional($RPD->personaDesaparecida)->idSexo),$sexo->idSexo) }}>
					{{ $sexo->nombreSexo }}
				</option>
			@endforeach
		</select>
	 	@inputErrors(['input'=>'personaDesaparecida.idSexo'])
	</div>
	
	<label for="idEscolaridad" class="col-sm-2 col-form-label col-form-label-sm">
		Escolaridad:
	</label>
	<div class="col-sm-3">
	 	<select name="personaDesaparecida[idEscolaridad]" id="idEscolaridad" class="form-control form-control-sm">
	 		<option value="">--sin opción--</option>
			@foreach($escolaridades as $key=>$escolaridad)
				<option value="{{ $escolaridad->idEscolaridad }}" 
					{{ selected(old('personaDesaparecida.idEscolaridad',optional($RPD->personaDesaparecida)->idEscolaridad),$escolaridad->idEscolaridad) }}>
					{{ $escolaridad->nombreEscolaridad }}
				</option>
			@endforeach
		</select>
	 	@inputErrors(['input'=>'personaDesaparecida.idEscolaridad'])
	</div>
</div>

<div class="form-group row">
	<label for="idEstadocivil" class="col-sm-2 col-form-label col-form-label-sm">
		Estado civil:
	</label>
	<div class="col-sm-3">
	 	
		<select name="personaDesaparecida[idEstadocivil]" id="idEstadocivil" class="form-control form-control-sm">
			<option value="">--sin opción--</option>
			@foreach($estadosCivil as $key=>$estadoCivil)
				<option value="{{ $estadoCivil->idEstadoCivil }}" 
					{{ selected(old('personaDesaparecida.idEstadocivil',optional($RPD->personaDesaparecida)->idEstadocivil),$estadoCivil->idEstadoCivil) }}>
					{{ $estadoCivil->nombreEstadoCivil }}
				</option>
			@endforeach
		</select>

	 	@inputErrors(['input'=>'personaDesaparecida.idEstadocivil'])
	</div>
	
	<label for="idVulnerabilidad" class="col-sm-2 col-form-label col-form-label-sm">
		Vulnerabilidad:
	</label>
	<div class="col-sm-3">
		<select name="personaDesaparecida[idVulnerabilidad]" id="idVulnerabilidad" class="form-control form-control-sm">
			<option value="">--sin opción--</option>
			@foreach($vulnerabilidades as $key=>$vulnerabilidad)
				<option value="{{ $vulnerabilidad->idVulnerabilidad }}" 
					{{ selected(old('personaDesaparecida.idVulnerabilidad',optional($RPD->personaDesaparecida)->idVulnerabilidad),$vulnerabilidad->idVulnerabilidad) }}>
					{{ $vulnerabilidad->nombreVulnerabilidad }}
				</option>
			@endforeach
		</select>
	 	@inputErrors(['input'=>'personaDesaparecida.idVulnerabilidad'])
	</div>
</div>