<div class="form-group">
	
	<div class="col-12">
		<div class="form-group row">
			<div class="col-sm-12">
				
				<div class="input-group mb-3">
					<img src="{{ ($RPD->fotografiaRPD)?$RPD->rutaFotografiaRPD:''}}" 
						class="rounded mx-auto d-block" 
						alt=""
						with="200" height="200" 
						id="imgPrevia"
					>
				</div>
				
				<div class="input-group mb-3">
					<div class="input-group-prepend">

						<span class="input-group-text">Subir</span>
					</div>
					<div class="custom-file">
						<input 
							type="file" 
							class="custom-file-input" 
							id="fotos" 
							name="mediaFiliacion[fotos][]" 
							accept="image/*"
							onchange="$('#imgPrevia').attr('src',window.URL.createObjectURL(this.files[0]));"
						>
						<label  id="labelFoto" class="custom-file-label" for="foto1">Elegir fotos</label>
					</div>
					@inputErrors(['input'=>'fotos'])
				</div>
			</div>
		</div>
	</div>
</div>

<div class="form-group row">
	<label for="idGeneroReportado" class="col-sm-2 col-form-label col-form-label-sm">
		Genero reportado:
	</label>
	<div class="col-sm-3">
	 	<select name="mediaFiliacion[idGeneroReportado]" id="idGeneroReportado" class="form-control form-control-sm">
	 		<option value="">--sin opción--</option>
			@foreach($sexos as $key=>$sexo)
				<option value="{{ $sexo->idSexo }}" 
					{{ selected(old('mediaFiliacion.idGeneroReportado',optional($RPD->mediaFiliacionRPD)->idGeneroReportado),$sexo->idSexo) }}>
					{{ $sexo->nombreSexo }}
				</option>
			@endforeach
		</select>
	 	@inputErrors(['input'=>'mediaFiliacion.idGeneroReportado'])
	</div>

	<label for="idComplexion" class="col-sm-2 col-form-label col-form-label-sm">
		Complexion:
	</label>
	<div class="col-sm-3">
	 	<select name="mediaFiliacion[idComplexion]" id="idComplexion" class="form-control form-control-sm">
	 		<option value="">--sin opción--</option>
			@foreach($complexiones as $key=>$complexion)
				<option value="{{ $complexion->idComplexion }}" 
					{{ selected(old('mediaFiliacion.idComplexion',optional($RPD->mediaFiliacionRPD)->idComplexion),$complexion->idComplexion) }}>
					{{ $complexion->nombreComplexion }}
				</option>
			@endforeach
		</select>
	 	@inputErrors(['input'=>'mediaFiliacion.idComplexion'])
	</div>
</div>

<div class="form-group row">
	<label for="tez" class="col-sm-2 col-form-label col-form-label-sm">
		Tez:
	</label>
	<div class="col-sm-3">
		<select name="mediaFiliacion[tez]" id="tez" class="form-control form-control-sm">
			<option value="">--sin opción--</option>
			@foreach($mediaFiliacion['tez'] as $key =>$tez)
				<option value="{{ $tez->nombreTipoTez }}"
					{{ selected(old('mediaFiliacion.tez',optional($RPD->mediaFiliacionRPD)->tez),$tez->nombreTipoTez) }}
				>
					{{ $tez->nombreTipoTez }}
				</option>
			@endforeach
		</select>
	 	
	 	@inputErrors(['input'=>'mediaFiliacion.tez'])
	</div>
	<label for="frente" class="col-sm-2 col-form-label col-form-label-sm">
		Frente:
	</label>
	<div class="col-sm-3">
		<select name="mediaFiliacion[frente]" id="frente" class="form-control form-control-sm">
			<option value="">--sin opción--</option>
			@foreach($mediaFiliacion['frente'] as $key =>$frente)
				<option value="{{ $frente->nombreTipoFrente}}"
					{{ selected(old('mediaFiliacion.frente',optional($RPD->mediaFiliacionRPD)->frente),$frente->nombreTipoFrente) }}
				>
					{{ $frente->nombreTipoFrente }}
				</option>
			@endforeach
		</select>
	 	@inputErrors(['input'=>'mediaFiliacion.frente'])
	</div>
</div>

<div class="form-group row">
	<label for="boca" class="col-sm-2 col-form-label col-form-label-sm">
		Boca:
	</label>
	<div class="col-sm-3">
		<select name="mediaFiliacion[boca]" id="boca" class="form-control form-control-sm">
			<option value="">--sin opción--</option>
			@foreach($mediaFiliacion['boca'] as $key =>$boca)
				<option value="{{ $boca->nombreTipoBoca}}"
					{{ selected(old('mediaFiliacion.boca',optional($RPD->mediaFiliacionRPD)->boca),$boca->nombreTipoBoca) }}
				>
					{{ $boca->nombreTipoBoca }}
				</option>
			@endforeach
		</select>
	 	@inputErrors(['input'=>'mediaFiliacion.boca'])
	</div>
	<label for="cejas" class="col-sm-2 col-form-label col-form-label-sm">
		Cejas:
	</label>
	<div class="col-sm-3">
		<select name="mediaFiliacion[cejas]" id="cejas" class="form-control form-control-sm">
			<option value="">--sin opción--</option>
			@foreach($mediaFiliacion['cejas'] as $key =>$cejas)
				<option value="{{ $cejas->nombreTipoCejas }}"
					{{ selected(old('mediaFiliacion.cejas',optional($RPD->mediaFiliacionRPD)->cejas),$cejas->nombreTipoCejas) }}
				>
					{{ $cejas->nombreTipoCejas }}
				</option>
			@endforeach
		</select>
	 	@inputErrors(['input'=>'mediaFiliacion.cejas'])
	</div>
</div>

<div class="form-group row">
	<label for="menton" class="col-sm-2 col-form-label col-form-label-sm">
		Menton:
	</label>
	<div class="col-sm-3">
		<select name="mediaFiliacion[menton]" id="menton" class="form-control form-control-sm">
			<option value="">--sin opción--</option>
			@foreach($mediaFiliacion['menton'] as $key =>$menton)
				<option value="{{ $menton->nombreTipoMenton }}"
					{{ selected(old('mediaFiliacion.menton',optional($RPD->mediaFiliacionRPD)->menton),$menton->nombreTipoMenton) }}
				>
					{{ $menton->nombreTipoMenton }}
				</option>
			@endforeach
		</select>
	 	@inputErrors(['input'=>'mediaFiliacion.menton'])
	</div>
	<label for="colorOjos" class="col-sm-2 col-form-label col-form-label-sm">
		Color ojos:
	</label>
	<div class="col-sm-3">
		<select name="mediaFiliacion[colorOjos]" id="colorOjos" class="form-control form-control-sm">
			<option value="">--sin opción--</option>
			@foreach($mediaFiliacion['colorOjos'] as $key =>$colorOjos)
				<option value="{{ $colorOjos->nombreTipoColorOjos }}"
					{{ selected(old('mediaFiliacion.colorOjos',optional($RPD->mediaFiliacionRPD)->colorOjos),$colorOjos->nombreTipoColorOjos) }}
				>
					{{ $colorOjos->nombreTipoColorOjos }}
				</option>
			@endforeach
		</select>
	 	@inputErrors(['input'=>'mediaFiliacion.colorOjos'])
	</div>
</div>

<div class="form-group row">
	<label for="tipoCabello" class="col-sm-2 col-form-label col-form-label-sm">
		Tipo cabello:
	</label>
	<div class="col-sm-3">
		<select name="mediaFiliacion[tipoCabello]" id="tipoCabello" class="form-control form-control-sm">
			<option value="">--sin opción--</option>
			@foreach($mediaFiliacion['tipoCabello'] as $key =>$tipoCabello)
				<option value="{{ $tipoCabello->nombreTipoCabello }}"
					{{ selected(old('mediaFiliacion.tipoCabello',optional($RPD->mediaFiliacionRPD)->tipoCabello),$tipoCabello->nombreTipoCabello) }}
				>
					{{ $tipoCabello->nombreTipoCabello }}
				</option>
			@endforeach
		</select>
	 	@inputErrors(['input'=>'mediaFiliacion.tipoCabello'])
	</div>
	<label for="longitudCabello" class="col-sm-2 col-form-label col-form-label-sm">
		Longitud cabello:
	</label>
	<div class="col-sm-3">
		<select name="mediaFiliacion[longitudCabello]" id="longitudCabello" class="form-control form-control-sm">
			<option value="">--sin opción--</option>
			@foreach($mediaFiliacion['longitudCabello'] as $key =>$longitudCabello)
				<option value="{{ $longitudCabello->nombreTipoLogitudCabello }}"
					{{ selected(old('mediaFiliacion.longitudCabello',optional($RPD->mediaFiliacionRPD)->longitudCabello),$longitudCabello->nombreTipoLogitudCabello) }}
				>
					{{ $longitudCabello->nombreTipoLogitudCabello }}
				</option>
			@endforeach
		</select>
	 	
	 	@inputErrors(['input'=>'mediaFiliacion.longitudCabello'])
	</div>
</div>

<div class="form-group row">
	<label for="estatura" class="col-sm-2 col-form-label col-form-label-sm">
		Estatura:
	</label>
	<div class="col-sm-3">
	 	<input id="estatura" type="text" class=" toUpperCase form-control  form-control-sm @error('estatura') is-invalid @enderror" name="mediaFiliacion[estatura]" 
	 		value="{{ old('mediaFiliacion.estatura',optional($RPD->mediaFiliacionRPD)->estatura) }}">
	 	@inputErrors(['input'=>'mediaFiliacion.estatura'])
	</div>
	<label for="cara" class="col-sm-2 col-form-label col-form-label-sm">
		Cara:
	</label>
	<div class="col-sm-3">
		<select name="mediaFiliacion[cara]" id="cara" class="form-control form-control-sm">
			<option value="">--sin opción--</option>
			@foreach($mediaFiliacion['cara'] as $key =>$cara)
				<option value="{{ $cara->nombreTipoCara }}"
					{{ selected(old('mediaFiliacion.cara',optional($RPD->mediaFiliacionRPD)->cara),$cara->nombreTipoCara) }}
				>
					{{ $cara->nombreTipoCara }}
				</option>
			@endforeach
		</select>
	 	@inputErrors(['input'=>'mediaFiliacion.cara'])
	</div>
</div>

<div class="form-group row">
	<label for="nariz" class="col-sm-2 col-form-label col-form-label-sm">
		Nariz:
	</label>
	<div class="col-sm-3">
		<select name="mediaFiliacion[nariz]" id="nariz" class="form-control form-control-sm">
			<option value="">--sin opción--</option>
			@foreach($mediaFiliacion['nariz'] as $key =>$nariz)
				<option value="{{ $nariz->nombreTipoNariz }}"
					{{ selected(old('mediaFiliacion.nariz',optional($RPD->mediaFiliacionRPD)->nariz),$nariz->nombreTipoNariz) }}
				>
					{{ $nariz->nombreTipoNariz }}
				</option>
			@endforeach
		</select>
	 	@inputErrors(['input'=>'mediaFiliacion.nariz'])
	</div>
	<label for="labios" class="col-sm-2 col-form-label col-form-label-sm">
		Labios:
	</label>
	<div class="col-sm-3">
		<select name="mediaFiliacion[labios]" id="labios" class="form-control form-control-sm">
			<option value="">--sin opción--</option>
			@foreach($mediaFiliacion['labios'] as $key =>$labios)
				<option value="{{ $labios->nombreTipoLabio }}"
					{{ selected(old('mediaFiliacion.labios',optional($RPD->mediaFiliacionRPD)->labios),$labios->nombreTipoLabio) }}
				>
					{{ $labios->nombreTipoLabio }}
				</option>
			@endforeach
		</select>
	 	@inputErrors(['input'=>'mediaFiliacion.labios'])
	</div>
</div>

<div class="form-group row">
	<label for="descripcionMediaFiliacion" class="col-sm-2 col-form-label col-form-label-sm">
		Descripción:
	</label>
	<div class="col-sm-8">
		<textarea name="mediaFiliacion[descripcion]"  class="form-control form-control-sm toUpperCase"id="descripcionMediaFiliacion" cols="30" rows="5" maxlength="300">{{ old('mediaFiliacion.descripcion',optional($RPD->mediaFiliacionRPD)->descripcion) }}</textarea>
	</div>
</div>

<div class="form-group row">
	<label for="otrosDatosMediaFiliacion" class="col-sm-2 col-form-label col-form-label-sm">
		Otros datos:
	</label>
	<div class="col-sm-8">
		<textarea name="mediaFiliacion[otrosDatos]"  class="form-control form-control-sm toUpperCase"id="otrosDatosMediaFiliacion" cols="30" rows="5" maxlength="300">{{ old('mediaFiliacion.otrosDatos',optional($RPD->mediaFiliacionRPD)->otrosDatos) }}</textarea>
	</div>
</div>