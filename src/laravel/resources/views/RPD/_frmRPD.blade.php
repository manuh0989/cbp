@section('scripts')
    <script>
        $( document ).ready(function() {

            ajaxMunicipios($('#idEstadoPersonaDesaparecida'));
            ajaxMunicipios($('#idEstadoLocalizacion'));
            mostarLocalizacion($('#statusLocalizacion'));
            camposEstatusLocalizacion($('#idEstatusLocalizacion'));

            $('.btn-link').click(function(event) {
                
                var collapse   =$(this).attr('data-target');
                var inputFocus =$(this).attr('input-focus');
                
                $(collapse).collapse('toggle');
                $('#'+inputFocus).focus();
            });

            $('#fechaNacimientoPD').change(function(event){
                var url=$(this).attr('url');
                var fecha=this.value;
                $.ajax({
                    url:url
                    ,type:'POST'
                    ,data:{
                        fecha:fecha
                    }
                    ,dataType:'JSON'
                    ,beforeSend:function(){
                      //$('#divMunicipios').html(spinner());
                    }
                    ,success:function(data){
                      $('#edadPD').val(data.edad);
                    }
                });
            });

            $('#nombrePD,#primerApellidoPD,#segundoApellidoPD').change(function(event){
                llenarToastInformacion();
            });

            $('#idEstadoPersonaDesaparecida,#idEstadoLocalizacion').change(function(event){
                ajaxMunicipios(this);
            });

            $('#statusLocalizacion').change(function(event){
                mostarLocalizacion(this);
            });

            $('#idFuente').change(function(event) {
                let url      =$(this).attr('url');
                let idFuente =this.value;
                $.ajax({
                    url:url
                    ,type:'POST'
                    ,dataType:'JSON'
                    ,data:{
                        idFuente:idFuente
                    }
                    ,beforeSend:function(){
                      //$('#divMunicipios').html(spinner());
                    }
                    ,success:function(data){
                      $('#folioCBP').val(data.folioCBP);
                    }
                });
            });
        });
    </script>
@endsection


<div id="accordion" class="accordion">

    <div class="card">
        <div class="card-header" id="headingDatosBasicos">
            <h5 class="mb-0">
                <button class="btn btn-link" type="button" data-target="#collapseDatosBasicos" input-focus="folioFIPEDE">
                    Datos del Reporte
                </button>
            </h5>
        </div>

        <div id="collapseDatosBasicos" class="collapse show" aria-labelledby="headingDatosBasicos" data-parent="#accordion">
            <div class="card-body">
                @include('RPD._camposRPD')
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header" id="headingDPD">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed"  data-target="#collapseDPD"  type="button" input-focus="nombrePD">
                    Datos de la Persona Desaparecida
                </button>
            </h5>
        </div>
        <div id="collapseDPD" class="collapse" aria-labelledby="headingDPD" data-parent="#accordion">
            <div class="card-body">
                @include('RPD._camposPersonaDesaparecida')
                @include('RPD._camposDatosPD')
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header" id="headingDGD">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed"  data-target="#collapseDGD" type="button" input-focus="estadoDesaparicion">
                    Datos Generales de la Desaparición
                </button>
            </h5>
        </div>
        <div id="collapseDGD" class="collapse" aria-labelledby="headingDGD" data-parent="#accordion">
            <div class="card-body">
                @include('RPD._camposDatosGeneralesDesaparicion')
                @include('RPD._camposHechosDesaparicion')
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header" id="headingReportante">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed"  data-target="#collapseReportante"type="button"
                input-focus="nombreCompletoReportante">
                    Datos del Reportante
                </button>
            </h5>
        </div>
        <div id="collapseReportante" class="collapse" aria-labelledby="headingReportante" data-parent="#accordion">
            <div class="card-body">
                @include('RPD._camposReportante')
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header" id="headingMediaFiliacion">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-target="#collapseMediaFiliacion"type="button"
                input-focus="idGeneroReportado">
                    Media Filiación
                </button>
            </h5>
        </div>
        <div id="collapseMediaFiliacion" class="collapse" aria-labelledby="headingMediaFiliacion" data-parent="#accordion">
            <div class="card-body">
                @include('RPD._camposMediaFiliacion')
            </div>
        </div>
    </div>

    <div class="card">
         <div class="card-header" id="headingLocalizacion">
            <h5 class="mb-0">
                <button 
                    id="btnAcordionLocalizacion"
                    class="btn btn-link collapsed disabled " 
                    data-target="#collapseLocalizacion"
                    type="button"
                    input-focus="idEstatusLocalizacion"
                >
                    Datos de la Localización
                </button>
            </h5>
        </div>
        <div id="collapseLocalizacion" class="collapse" aria-labelledby="headingLocalizacion" data-parent="#accordion">
            <div class="card-body">
                @include('RPD.condicionLocalizacion._camposCondicionLocalizacion')
            </div>
        </div>
    </div>
</div>