	<script>
		$( document ).ready(function(){
			$('#tblDesplegable').DataTable({
		       searching: false,
		       select: false,
		       paginate:true,
		       bInfo:false
	    	});

			$('.toUpperCase').keyup(function(event){
				$(this).val($(this).val().toUpperCase());
			});
			feather.replace();

		});
	</script>
@table(['encabezados'=>$encabezados,'idTable'=>'tblDesplegable'])
	@foreach($datos as $key =>$dato)
		<tr id="tr{{ $dato->id }}">
			<td>{{ $key+1 }}</td>
			<td>
				<span id="span{{ $dato->id }}" style="display: block;">
					{{ $dato->nombre }}
				</span>
				<form 
					class="form-inline" 
					id="frm{{ $dato->id }}" 
					style="display: none;" 
					method="POST" 
					action={{ route('catalogo.update',$dato) }} 
				>
					@method('PUT')
					@csrf
					<div class="form-group mb-2">
						<input type="text" type="text" class="form-control-plaintext form-control-sm toUpperCase" value="{{ $dato->nombre }}" id="txt{{ $dato->id }}"  name="txt{{ $dato->id }}"/>

						<input type="hidden" value="{{ $model }}" name="model{{ $dato->id }}" id="model{{ $dato->id }}">
					</div>
					<div class="form-group mx-sm-3 mb-2">
						<button class="btn btn-green btn-icon btn-xs" id="{{ $dato->id }}"type="button" onclick="editarCatalogo(this);">
							S
						</button>
					</div>
				</form>
			</td>
			<td>
				@if(!$dato->trashed())
					<span class="badge badge-green">Habilitado</span>
				@else
					<span class="badge badge-danger">DesHabilitado</span>
				@endif
			</td>
			<td>
				@if(!$dato->trashed())
					<img  id="{{ $dato->id }}"
						  src="{{ asset('img/data-feather/edit.svg') }}" alt="" 
						  class="mr-2 pointer" 
						  onclick="mostrarEditarCatalogo(this);"
					>
					
					<img id="trash{{ $dato->id }}" 
						idCatalogo="{{ $dato->id  }}"
						src="{{ asset('img/data-feather/trash-2.svg') }}" alt="" 
						class="pointer"
						model="{{ get_class($dato) }}"
						url="{{ route('ajax.trashedCatalogo') }}"
						onclick="trashedCatalogo(this)" 
					>
				@endif
				@if($dato->trashed())
					<button  class="btn btn-datatable btn-icon btn-transparent-dark mr-2 btn-submit" 
					id="trash{{ $dato->id }}" 
					idCatalogo="{{ $dato->id  }}"
					model="{{ get_class($dato) }}"
					url="{{ route('ajax.restoreCatalogo') }}"
					onclick="restoreCatalogo(this)"
					>
						<i data-feather="play"></i>
					</button>
				@endif
			</td>
		</tr>
	@endforeach
@endtable


