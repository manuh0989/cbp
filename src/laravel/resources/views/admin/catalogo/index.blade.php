@extends('layouts.app')
@section('scripts')
	<script>
		$( document ).ready(function() {
			mostrarCatalogos($('#catalogos'));
			$('#btnGuardarCatalogo').click(function(event){
				var frm = $('#frmCatalogo');
				$.ajax({
					url:frm.attr('action')

					,type:'POST'
					,data:frm.serialize()+'&model='+this.value
					,dataType:'JSON'
					,beforeSend:function(){
						$('#btnGuardarCatalogo').html(spinner());
					}
					,error:function(data){
						$('#btnGuardarCatalogo').html('Guardar');
						$.each(data.responseJSON.errors,function(item, mensaje){
							$('#'+item).addClass('is-invalid');
							$('#error'+item).html(mensaje)
						});
						
					}
					,success:function(data){
						$('#btnGuardarCatalogo').html('Guardar');
						$('#messageAjax').show();
						$('#contentMessageAjax').html(data.message)
						/*$('html, body').animate({
							scrollTop: $("#divCatalogo").offset().top
						}, 2000);*/
						mostrarCatalogos($('#catalogos'));
					}
				});
			});

			
		});
	</script>
@endsection

@section('content')

	<div class="p-4 border-bottom" style="display: none" id="messageAjax">
		<div class="alert alert-primary alert-icon border-left-lg" role="alert">
			<button class="close" type="button" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<div class="alert-icon-aside">
				<i class="fas fa-check"></i>
			</div>
			<div class="alert-icon-content" id="contentMessageAjax"></div>
		</div>
	</div>

	@card(['header'=>'Administrar catálogos'])
		<form action="{{ route('catalogo.store') }}" method="POST" id="frmCatalogo">
			@csrf
			<div class="form-group row">
				<div class="col-md-6 ">
					<label for="catalogos">
						Catálogo:
					</label>
					<select name="catalogos" id="catalogos" class="form-control"
					onchange="mostrarCatalogos(this);" 
						url={{ route('ajax.catalogo') }}
					>
						@foreach($catalogos as $model => $catalogos)
							<option value="{{ $model }}">
								 {{ $catalogos }}
							</option>
						@endforeach
					</select>

					<span role="alert">
						<strong id="errorcatalogos"></strong>
					</span>
				</div>

				<div class="col-md-5">
					<label for="nombre">
						Nombre:
					</label>
					<input type="text" class="form-control toUpperCase" name="nombre" id="nombre_">
					<span role="alert">
						<strong id="errornombre"></strong>
					</span>
	
				</div>
				<div class="col-md-1">
					<label for="guardar">Guardar</label>
					<button class="btn btn-primary btn-sm" type="button" id="btnGuardarCatalogo">
						Guardar
					</button>
				</div>
			</div>
		</form>
	@endcard
	
	@card(['header'=>'Catalogo'])
		<div id="divCatalogo"></div>
	@endcard
@endsection