@extends('layouts.app')
@section('scripts')
	<script>
		$( document ).ready(function(){
			$('#tblUsuarios').DataTable({
		       searching: false,
		       select: false,
		       paginate:false,
		       bInfo:false

	    	});
		});
	</script>
@endsection

@section('content')
	@card(['header'=>'Usuarios'])
		@table(['encabezados'=>$encabezados,'idTable'=>'tblUsuarios'])
			@foreach($usuarios as $key => $usuario)
				<tr>
					<td>{{ $key+1 }}</td>
					<td>{{ $usuario->nombreCompleto }}</td>
					<td>{{ $usuario->email }}</td>
					<td>{{ $usuario->created_at }}</td>
					<td>{{ $usuario->updated_at }}</td>
					<td>
						@if(!$usuario->trashed())
							<span class="badge badge-green">Habilitado</span>
						@else
							<span class="badge badge-danger">DesHabilitado</span>
						@endif
					</td>
					<td>
						@if($usuario->isAdmin())
							<span class="badge badge-green">{{ $usuario->nombreRole }}</span>
						@else
							<span class="badge badge-orange">{{ $usuario->nombreRole }}</span>
						@endif

					</td>
					<td>
						@if($usuario->trashed())

							<button  class="btn btn-datatable btn-icon btn-transparent-dark mr-2 btn-submit" 
							id="{{ $usuario->idUsuario }}"
							frmRestore="frmRestoreUsuario"
							onclick="restore(this)">
								<i data-feather="play"></i>
							</button>
						@endif
						@if(!$usuario->trashed())
							<a  href="{{route('usuario.editar',$usuario)  }}" class="btn btn-datatable btn-icon btn-transparent-dark mr-2 btn-submit" >
								<i data-feather="edit"></i>
							</a>
							<button  class="btn btn-datatable btn-icon btn-transparent-dark mr-2 btn-submit" 
							id="{{ $usuario->idUsuario }}"
							frmTrash="frmTrashUsuario"
							onclick="trash(this)">
								<i data-feather="trash"></i>
							</button>
						@endif
					</td>
				</tr>
			@endforeach
		@endtable
	@endcard

	<form action="{{route('usuario.trash',':id')  }}" method="POST" id="frmTrashUsuario">
		@csrf @method('PUT')
	</form>

	<form action="{{route('usuario.restore',':id')  }}" method="POST" id="frmRestoreUsuario">
		@csrf @method('PUT')
	</form>
@endsection