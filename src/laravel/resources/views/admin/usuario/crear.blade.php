@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @card(['header'=>__('Register')])
                <form method="POST" action="{{ route('register') }}" autocomplete="off">
                    @csrf @method('POST')
                    
                    @include('admin.usuario._camposUsuario')
            
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Register') }}
                            </button>
                        </div>
                    </div>
                </form>
            @endcard
        </div>
    </div>
</div>
@endsection
