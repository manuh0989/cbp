       <div class="form-group row">
            <label for="username" class="col-md-4 col-form-label text-md-right">
                Usuario
            </label>

            <div class="col-md-6">
                <input id="usernameh" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username',$usuario->username) }}"  autocomplete="username" autofocus>

                @error('username')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

            <div class="col-md-6">
                <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre',$usuario->nombre) }}"  autocomplete="name" autofocus>

                @error('nombre')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="apellido_paterno" class="col-md-4 col-form-label text-md-right">
                Apellido paterno
            </label>

            <div class="col-md-6">
                <input id="apellido_paterno" type="text" class="form-control @error('apellido_paterno') is-invalid @enderror" name="apellido_paterno" value="{{ old('apellido_paterno',$usuario->apellido_paterno) }}"  autocomplete="apellido_paterno" autofocus>

                @error('apellido_paterno')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="apellido_materno" class="col-md-4 col-form-label text-md-right">
                Apellido materno
            </label>

            <div class="col-md-6">
                <input id="apellido_materno" type="text" class="form-control @error('apellido_materno') is-invalid @enderror" name="apellido_materno" value="{{ old('apellido_materno',$usuario->apellido_materno) }}"  autocomplete="apellido_materno" autofocus>

                @error('apellido_materno')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email',$usuario->email) }}"  autocomplete="email">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  autocomplete="new-password">
            </div>
        </div>

        <div class="form-group row">
            <label for="Role" class="col-md-4 col-form-label text-md-right">
                Role
            </label>

            <div class="col-md-6">
                <select name="idRole" id="idRole" class="form-control">
                    @foreach(App\Models\Role::all() as $key => $role)
                        <option value="{{ $role->idRole }}" {{ selected(old('idRole',$usuario->idRole),$role->idRole) }}>
                            {{ $role->role }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>