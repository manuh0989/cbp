@extends('layouts.app')
@section('content')
	@card(['header'=>'Editar usuario'])
        <form method="POST" action="{{ route('usuario.update',$usuario) }}" 
        	autocomplete="off"
        >
            @csrf @method('PUT')
            @include('admin.usuario._camposUsuario')
    
            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary btn-submit">
                        Editar
                    </button>
                </div>
            </div>
        </form>
    @endcard
@endsection