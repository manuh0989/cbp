<div class="nav accordion" id="accordionCBP">
    <div class="sidenav-menu-heading">
        Registros CBP
    </div>
    
    <a class="nav-link collapsed" href="javascript:void(0);" 
        data-toggle="collapse" 
        data-target="#cbp" 
        aria-expanded="false" 
        aria-controls="dashboard">

            <div class="nav-link-icon">
                <i data-feather="activity"></i>
            </div>
            CBP
            <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>            
    </a>
        
    <div class="{{ (in_array(Request::route()->getName(),['RPD.registro','RPD.index','RPD.editar','RPD.seguimiento']))?'collapsed':'collapse' }}" id="cbp" data-parent="#accordionCBP">
        <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPages">
            <div class="nav accordion" id="accordionSidenav">                    
                <a class="nav-link collapsed" href="{{ route('RPD.registro') }}">
                    <div class="nav-link-icon">
                        <i data-feather="user-plus"></i>
                    </div>
                    Nuevo registro
                </a>
                <a class="nav-link collapsed" href="{{ route('RPD.index') }}">
                    <div class="nav-link-icon">
                        <i data-feather="list"></i>
                    </div>
                    Ver registros
                </a>
            </div>
        </nav>
    </div>
</div>



@can('view',auth()->user())
    <div class="nav accordion" id="accordionAdmin">
        <div class="sidenav-menu-heading">
            Administración
        </div>
        
        <a class="nav-link collapsed" href="javascript:void(0);" 
            data-toggle="collapse" 
            data-target="#usuarios" 
            aria-expanded="false" 
            aria-controls="dashboard">

                <div class="nav-link-icon">
                    <i data-feather="activity"></i>
                </div>
                CBP usuarios
                <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>            
        </a>
        
        <div class="{{ (in_array(Request::route()->getName(),['usuario.index','usuario.crear']))?'collapsed':'collapse' }}" id="usuarios" data-parent="#accordionAdmin">
            <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPages">
                    <div class="nav accordion" id="accordionSidenav">                    
                        <a class="nav-link collapsed" href="{{ route('usuario.index') }}">
                            <div class="nav-link-icon">
                                <i data-feather="square"></i>
                            </div>
                            Ver usuarios
                        </a>
                         <a class="nav-link collapsed" href="{{ route('usuario.crear') }}">
                            <div class="nav-link-icon">
                                <i data-feather="square"></i>
                            </div>
                            Registrar usuario
                        </a>
                    </div>
            </nav>
        </div>
        

        <a class="nav-link collapsed" href="javascript:void(0);" 
            data-toggle="collapse" 
            data-target="#desplegables" 
            aria-expanded="false" 
            aria-controls="dashboard">

                <div class="nav-link-icon">
                    <i data-feather="activity"></i>
                </div>
                CBP Catálogos
                <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>            
        </a>
        <div class="{{ (in_array(Request::route()->getName(),['catalogo.index']))?'collapsed':'collapse' }}" id="desplegables" data-parent="#accordionAdmin">
            <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPages">
                    <div class="nav accordion" id="accordionSidenav">                    
                        <a class="nav-link collapsed" href="{{ route('catalogo.index') }}">
                            <div class="nav-link-icon">
                                <i data-feather="square"></i>
                            </div>
                            Ver catálogos
                        </a>
                    </div>
            </nav>
        </div>
            
        
    </div>
@endcan

@include('RPD._informacionGeneralRPD')