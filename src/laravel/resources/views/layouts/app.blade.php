<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <meta name="description" content="" />
        <meta name="author" content="" />

        <title>{{ config('app.name', 'Laravel') }}</title>

        
        @include('compartidos._links')
        
        @include('compartidos._scripts')
        @yield('scripts')
    </head>
    <body class="nav-fixed">
        
       @include('layouts._header');
        

        <div id="layoutSidenav">

            <div id="layoutSidenav_nav">                
                <nav class="sidenav shadow-right sidenav-dark">
                    
                    <div class="sidenav-menu">
                        @include('layouts._menu')
                    </div>

                    <div class="sidenav-footer">
                        <div class="sidenav-footer-content">
                            @auth
                                <div class="sidenav-footer-subtitle">
                                    Bienvenid@:
                                </div>
                                <div class="sidenav-footer-title">
                                    {{ auth()->user()->nombre }}
                                </div>
                            @endauth
                        </div>
                    </div>
                </nav>


            </div>
            
            <div id="layoutSidenav_content">
                <main>
                    <div class="page-header pb-10 ">
                        <div class="row">
                            <div class="col-lg-12">
                                @include('compartidos._errors')
                                @include('compartidos._message')
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid mt-n10">
                        <div class="row">
                            <div class="col-lg-12">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </main>
                @include('compartidos._footer')
            </div>

        </div>
        
    </body>
</html>
