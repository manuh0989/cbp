<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="" />
        <meta name="author" content="" />
        
        <title>{{ config('app.name', 'Laravel') }}</title>

        <link rel="stylesheet" href="{{ asset('css/styles.css') }}"  />
    
        <link rel="icon" type="image/x-icon" href="{{ asset('assets/img/favicon.png') }}" />

        <script data-search-pseudo-elements defer src="{{ asset('js/font-awesome/all.min.js') }}" crossorigin="anonymous"></script>

        <script src="{{ asset('js/feather-icons/feather.min.js') }}" crossorigin="anonymous"></script>

    </head>
    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header justify-content-center"><h3 class="font-weight-light my-4">Login</h3></div>
                                    <div class="card-body">
                                        <form method="POST" action="{{ route('login') }}" autocomplete="off">
                                            @csrf
                                            <div class="form-group">
                                                <label class="small mb-1" for="email">
                                                    Correo:
                                                </label>

                                                <input id="email" type="email" class="form-control py-4 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autofocus>
                                                @inputErrors(['input'=>'email'])
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for="password">
                                                    Contraseña
                                                </label>
                                                
                                                <input id="password" type="password" class="form-control  py-4 @error('password') is-invalid @enderror" name="password">
                                                @inputErrors(['input'=>'password'])
                                            </div>
                                         
                                            <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">

                                                @if (Route::has('password.request'))
                                                    <a class="small" href="{{ route('password.request') }}">
                                                    ¿Olvidó su contraseña?
                                                </a>
                                                @endif

                                                <button type="submit" class="btn btn-primary btn-submit">
                                                    Login
                                                </button>

                                            </div>
                                        </form>
                                    </div>
                                    <!--<div class="card-footer text-center">
                                        <div class="small"><a href="register-basic.html">Need an account? Sign up!</a></div>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="footer mt-auto footer-dark">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 small">Copyright &copy; Your Website 2020</div>
                            <div class="col-md-6 text-md-right small">
                                <a href="#!">Privacy Policy</a>
                                &middot;
                                <a href="#!">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        @include('compartidos._scripts')
    </body>
</html>