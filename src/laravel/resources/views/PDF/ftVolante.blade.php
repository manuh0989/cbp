<html>
    <head>
        <style>
            @page {
                margin: 0cm 0cm;
                padding: 0cm 0cm;
            }

            body {
                margin-top: 2cm;
                margin-left: 2cm;
                margin-right: 2cm;
                margin-bottom: 2cm;
                font-family: Arial, sans-serif !important;
            }

            header {
                position: fixed;
                top: 0cm;
                left: 0cm;
                right: 0cm;
                height: 2cm;
                color: white;
                line-height: 1.5cm;
            }

            footer {
                position: fixed; 
                bottom: 0cm; 
                left: 0cm; 
                right: 0cm;
                height: 2.15cm;
                background-color: #00ab4f;
                color: white;
                text-align: center;
            }
            
            main{
                margin-top: 80px;
            }

            body{
                margin: 0;
                padding: 0;
            }

            .tittleLocTo{
                width: 100%;
                text-align: right;
            }

            footer table tr td{
                color: white;
                text-align: center;
            }
            table{
                width: 100%;
            }
            .tittleLocTo{
                width: 100%;
                text-align: right;
            }
            .tittleLocTo h1{
                font-size: 15px;
            }
            .nombrePersona{
                width: 100%;
                background-color: #00ab4f;
                text-align: right;
                color: #fff;
            }
            .nombrePersona h1{
                margin-right: 30px;
            }
            .nombrePersona table{
                color: #fff !important;
                text-align: center !important;
                width: 100%;
            }
            h5, .nombrePersona h1, h2, .tittleLocTo h1{
                margin-top: 0px !important;
                margin-bottom: 0px !important;
            }
        </style>
    </head>
    <body>
        <header>
            <img src="{{ asset('assets/img/logo_volante.jpg') }}" width="50%" style="margin-left: 3px">
        </header>

        <footer>
            <table style="padding-top: 15px">
                <tr>
                    <td>
                        <h5 style="margin-top: 0; margin-bottom: 5px;">Si tienes alguna información, por favor comunícate a: 55 9131 4605 ó 81 3238 5672</h5>
                        <h5 style="margin-top: 0; margin-bottom: 5px;">comisionbusquedadepersonas@cdmx.gob.mx</h5>
                        <h5 style="margin-top: 0; margin-bottom: 5px;">Twitter e Instagram <b>@busqueda_cdmx</b> y Facebook <b>@cbusquedacdmx.</b></h5>
                    </td>
                    <td>
                        <h4 style="margin-top: 0; margin-bottom: 0;">CIUDAD <b>INNOVADORA</b> <br> Y DE <b>DERECHOS</b></h4>
                    </td>
                    <td>
                        <h4 style="margin-top: 0; margin-bottom: 0;">NUESTRA <br><b>CASA</b></h4>
                    </td>
                </tr>
            </table>
        </footer>

        <main>
            <div class="tittleLocTo">
                <h1 style="font-size: 38px; margin-top: 0; margin-bottom: 0; margin-right: 25px">AYÚDANOS A LOCALIZAR A:</h1>
            </div>
            <div class="nombrePersona">
                <table style="width: 100% !important">
                    <tr>
                        <td style="width: 50%"></td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <h1 style="margin-top: 0; margin-bottom: 0; color: #f5f6f6 !important; font-size: 30px !important;">{{ $data[0]->nombrePD }}</h1>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h1 style="margin-top: 0; margin-bottom: 0; font-size: 20px !important">{{ $data[0]->primerApellidoPD . ' ' . $data[0]->segundoApellidoPD }}</h1>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="volContent">
                <table style="z-index: 1;">
                    <tr>
                        <td style="width: 40%">
                            <img src="{{ asset('storage/RPD/' . $id . '/' . $data[0]->foto) }}" width="350px;" height="450px" style="margin-left: 15px; margin-top: -80px;"">
                            <!--<img src="{{ asset('../../laravel/storage/app/public/RPD/' . $id . '/' . $data[0]->foto) }}" width="350px;" height="450px" style="margin-left: 15px; margin-top: -80px;">-->
                        </td>
                        <td style="width: 60%">
                            <table>
                                <tr>
                                    <td style="text-align: center; ">
                                        @php
                                            $fecha = new DateTime($data[0]->fechaDesaparicion);
                                        @endphp
                                        <h2 style="font-size: 20px !important">DESAPARECIÓ EL {{ $fecha->format('d-m-Y') }}</h2> <br>

                                        <h2>EN  {{ Str::upper($data[0]->nombreMunicipio)   }}  {{  Str::upper($data[0]->nombreLocalidad)  }}</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center">
                                        <p style="padding: 15px;">
                                            @if (strlen($data[0]->edadPD) > 0) EDAD {{ $data[0]->edadPD }} AÑOS,  @endif
                                            @if (strlen($data[0]->nombreSexo) > 0) SEXO {{ $data[0]->nombreSexo }}, @endif
                                            @if (strlen($data[0]->tez) > 0) TEZ {{ $data[0]->tez }}, @endif
                                            @if (strlen($data[0]->estatura) > 0) ESTATURA {{ $data[0]->estatura }} m, @endif
                                            @if (strlen($data[0]->cara)) CARA {{ $data[0]->cara }}, @endif
                                            @if (strlen($data[0]->frente) > 0) FRENTE {{ $data[0]->frente }}, @endif
                                            @if (strlen($data[0]->labios) > 0) LABIOS {{ $data[0]->labios }}, @endif
                                            @if (strlen($data[0]->nariz) > 0) NARIZ {{ $data[0]->nariz }}, @endif
                                            @if (strlen($data[0]->cejas) > 0) CEJAS {{ $data[0]->cejas }}, @endif
                                            @if (strlen($data[0]->tipoCabello) > 0) TIPO DE CABELLO {{ $data[0]->tipoCabello }}, @endif
                                            @if (strlen($data[0]->colorOjos) > 0) @endif
                                            @if (strlen($data[0]->colorOjos) > 0) OJOS COLOR {{ $data[0]->colorOjos }}, @endif
                                            @if (strlen($data[0]->nombreComplexion) > 0) COMPLEXION {{ $data[0]->nombreComplexion }} @endif
                                        </p>
                                        <h3 style="">
                                            <span style="vertical-align: 50px;">FOLIO DE BÚSQUEDA {{ $data[0]->folioCBP }}</span>
                                            <img src="{{ asset('assets/img/qrCode.jpeg') }}" width="120px">
                                        </h3>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="hechos" style="text-align: center">
                <h3>{{ $data[0]->fotovolanteHechosDesaparicion}}</h3>
            </div>
        </main>
    </body>
</html>