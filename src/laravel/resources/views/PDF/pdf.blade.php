<html>
    <head>
        <style>
            @page {
                margin: 0.2cm 0.2cm;
            }
            body {
                font-family: 'Comfortaa', Arial, sans-serif !important;
                font-size: 12px
            }
        </style>
    </head>
    <body>
        <main>
            <table style="border: 3px solid #000; width: 100%;">
                <tbody>
                    <tr>
                        <td>
                            <center>
                                    
                                <img src="{{ asset('assets/img/logo2.jpg') }}" width="387.717px" height="79.0167px">
                                <p style="font-size: 15px">
                                    COMISIÓN DE BUSQUEDA DE PERSONAS DE LA CIUDAD DE MEXICO
                                    <br><br>
                                    <b>CÉDULA DE REGISTRO DE PERSONAS DESAPARECIDAS</b>
                                </p>
                            </center>
                            <table style="width: 100%">
                                <tbody>
                                    <tr>
                                        <td colspan="1" style="text-align: left;">TIPO DE CANALIZACION</td>
                                        <td colspan="2">{{ $data[0]->nombreOrigenNoticia  }}</td>
                                    </tr>
                                    <tr>
                                        <td>FOLIO CBP</td>
                                        <td>{{ $data[0]->folioCBP ?? 'T'}}</td>
                                        <td style="text-align: left;">ESTATUS</td>
                                        <td>
                                            {{ $data[0]->statusLocalizacion }}
                                        </td>
                                        <td colspan="1">ALTA EN REGISTRO</td>
                                        @php
                                            $fecha = new DateTime($data[0]->fechaRegistro);
                                        @endphp
                                        <td colspan="2">{{ $fecha->format('d-m-Y') }}</td>
                                    </tr>
                                    <tr>
                                        <td>CARPETA DE INVESTIGACIÓN</td>
                                        <td>{{ $data[0]->folioFIPEDE  }}</td>
                                        <td>FOLIO LOCATEL</td>
                                        <td>{{ $data[0]->folioLocatel  }}</td>
                                        <td colspan="1" style="width: 10px;">ULTIMA MODIFICION</td>
                                        <td  colspan="2">{{Illuminate\Support\Carbon::parse($data[0]->ultimaActualizacion)->format('d-m-Y') }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    
                </tbody>
            </table>

            <table style="border: 3px solid #000; width: 100%; border-top: 0 !important">
                <tbody>
                    <tr>
                        <td>
                            <table style="width: 100%">
                                <tbody>
                                    <tr>
                                        <td colspan="3"><br><b>DATOS BASICOS DE LA PERSONA DESAPARECIDA</b><br>&nbsp;</td>
                                        <td colspan="2"><b>FECHA DE DESAPARICION</b></td>
                                        
                                        <td colspan="2" style="text-align: right">
                                            <b>
                                            {{ (empty($data[0]->fechaDesaparicion))?'':Illuminate\Support\Carbon::parse($data[0]->fechaDesaparicion)->format('d-m-Y') }}
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">NOMBRE DE LA PERSONA DESAPARECIDA</td>
                                        <td colspan="3">
                                            <b>
                                            {{ $data[0]->nombrePD }} {{ $data[0]->primerApellidoPD }} {{ $data[0]->segundoApellidoPD  }}
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">FECHA DE NACIMIENTO</td>
                                        <td>
                                            {{ (empty($data[0]->fechaNacimientoPD))?'':Illuminate\Support\Carbon::parse($data[0]->fechaNacimientoPD)->format('d-m-Y') }}
                                        </td>
                                        <td>EDAD</td>
                                        <td>{{ $data[0]->edadPD  }} AÑOS</td>
                                        <td>LUGAR DE NACIMIENTO</td>
                                        <td colspan="3">{{ $data[0]->lugarNacimientoPD  }}</td>
                                    </tr>
                                    <tr>
                                       
                                        <td>SEXO</td>
                                        <td>{{ $data[0]->nombreSexo  }}</td>
                                        <td>CURP</td>
                                        <td>{{ $data[0]->CURPPD  }}</td>
                                        <td>RFC</td>
                                        <td>{{ $data[0]->RFCPD  }}</td>
                                    </tr>
                                    <tr>
                                        <td>ESTADO CIVIL</td>
                                        <td>{{ $data[0]->nombreEstadoCivil  }}</td>
                                        <td>VULNERABILIDAD</td>
                                        <td colspan="2">{{ $data[0]->nombreVulnerabilidad  }}</td>
                                        <td>ESCOLARIDAD</td>
                                        <td>{{ $data[0]->nombreEscolaridad  }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table style="border: 3px solid #000; width: 100%; border-top: 0 !important">
                <tbody>
                    <tr>
                        <td>
                            <table style="width: 100%">
                                <tbody>
                                    <tr>
                                        <td colspan="6">
                                            <br>
                                            <b>DATOS DEL REPORTANTE</b>
                                            <br>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>NOMBRE COMPLETO</td>
                                        <td colspan="6">{{ $data[0]->nombreCompletoReportante  }}</td>
                                    </tr>
                                    <tr>
                                        <td>PARENTESCO</td>
                                        <td>{{ $data[0]->nombreParentesco  }}</td>
                                        <!--<td>EDAD</td>
                                        <td>{{ $data[0]->edadReportante  }} AÑOS</td>-->
                                        <td>ULTIMO CONTACTO</td>
                                        @php
                                            $fecha = new DateTime($data[0]->fechaDesaparicion);
                                        @endphp
                                        <td>{{ $fecha->format('d-m-Y') }}</td>
                                    </tr>
                                    <tr>
                                        <td>TELEFONO</td>
                                        <td>{{ $data[0]->telefonoReportante  }}</td>
                                        <td>RED SOCIAL</td>
                                        <td colspan="3">{{ $data[0]->redSocialReportante  }}</td>
                                    </tr>
                                    <tr>
                                        <td>TELEFONO 2</td>
                                        <td>{{ $data[0]->telefono2Reportante  }}</td>
                                        <td>RED SOCIAL</td>
                                        <td colspan="3">{{ $data[0]->redSocial2Reportante  }}</td>
                                    </tr>
                                    <tr>
                                        <td>CORREO</td>
                                        <td colspan="5">{{ $data[0]->correoReportante  }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table style="border: 3px solid #000; width: 100%; border-top: 0 !important">
                <tbody>
                    <tr>
                        <td>
                            <table style="width: 100%">
                                <tbody>
                                    <tr>
                                        <td colspan="8">
                                            <br>
                                            <b>HECHOS</b>
                                            <br>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="1">LUGAR DE DESAPARICION</td>
                                        <td colspan="3">{{ $data[0]->nombreLugarDesaparicion  }}</td>
                                        <td colspan="1">ESTADO</td>
                                        <td colspan="1">{{ Str::upper($data[0]->nombreEstado)  }}</td>
                                        <td colspan="1">MUNICIPIO</td>
                                        <td colspan="1"> {{ Str::upper($data[0]->nombreMuni)  }}</td>
                                    </tr>
                                   <tr width="50%">
                                        <td>COLONIA</td>
                                        <td colspan="2">
                                            {{ Str::upper($data[0]->nombre)  }}
                                        </td>
                                        <td>CALLE Y NUMERO</td>
                                        <td colspan="5">{{ $data[0]->calleNumeroDesaparicion  }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="1">CIRCUNSTANCIA DE DESAPARICION</td>
                                        <td colspan="7">{{ $data[0]->nombreCircunstanciaDesaparicion  }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="1">HIPOTESIS</td>
                                        <td colspan="7">{{ $data[0]->nombreHipotesisDesaparicion  }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="8">DESCRIPCION DE LOS HECHOS</td>
                                    </tr>
                                    <tr>
                                        <td colspan="8"style=" word-break:break-all; word-wrap:break-word;">
                                            <br>
                                            {{ $data[0]->descripcionHechosDesaparicion ?? ''}}
                                            <br><br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8">DESCRIPCION FOTOVOLANTE</td>
                                    </tr>
                                    <tr>
                                        <td  colspan="8"style="word-break:break-all; word-wrap:break-word;">
                                            <br>{{ $data[0]->fotovolanteHechosDesaparicion ?? ''}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table style="border: 3px solid #000; width: 100%; border-top: 0 !important">
                <tbody>
                    <tr>
                        <td colspan="3">
                            <table style="width: 100%">
                                <tbody>
                                    <tr>
                                        <td colspan="6">
                                            <br>
                                            <b>MEDIA FILIACIÓN</b>
                                            <br>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Frente:</td>
                                        <td colspan="2">{{ $data[0]->frente  }}</td>
                                        <td>Tipo cabello:</td>
                                        <td colspan="2">{{ $data[0]->tipoCabello  }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tez:</td>
                                        <td colspan="2">{{ $data[0]->tez  }}</td>
                                        <td>Longitud cabello:</td>
                                        <td colspan="2">{{ $data[0]->longitudCabello  }}</td>
                                    </tr>
                                    <tr>
                                        <td>Boca:</td>
                                        <td colspan="2">{{ $data[0]->boca  }}</td>
                                        <td>Estatura:</td>
                                        <td colspan="2">{{ $data[0]->estatura  }} cm</td>
                                    </tr>
                                    <tr>
                                        <td>Cejas:</td>
                                        <td colspan="2">{{ $data[0]->cejas  }}</td>
                                        <td>Cara:</td>
                                        <td colspan="2">{{ $data[0]->cara  }}</td>
                                    </tr>
                                    <tr>
                                        <td>Menton:</td>
                                        <td colspan="2">{{ $data[0]->menton  }}</td>
                                        <td>Nariz:</td>
                                        <td colspan="2">{{ $data[0]->nariz  }}</td>
                                    </tr>
                                    <tr>
                                        <td>Color ojos:</td>
                                        <td colspan="2">{{ $data[0]->colorOjos  }}</td>
                                        <td>Labios:</td>
                                        <td colspan="2">{{ $data[0]->labios  }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td style="width: 30%">
                            <center>
                                <img src="{{ asset("storage/RPD/{$RPD->idRPD}/{$data[0]->foto}") }}" style="width: 100px; height: 125px;">
                            </center>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table style="border: 3px solid #000; width: 100%;">
                <tbody>
                    <tr>
                        <td>
                            <table style="width: 100%">
                                <tbody>
                                    <tr>
                                        <td colspan="6">
                                            <br>
                                            <b>ACCIONES DE SEGUIMIENTO</b>
                                            <br>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>FUNICIONARIO REGISTRANTE</td>
                                        <td colspan="3">
                                            {{ $RPD->funcionarioRegistro }}
                                        </td>
                                        <td>FUNCIONARIO SEGUIMIENTO</td>
                                        <td colspan="3">
                                            {{ $RPD->funcionarioRegistro }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">CONTACTO</td>
                                        <td colspan="3">&nbsp;</td>
                                        <td style="text-align: center">FECHA</td>
                                        <td colspan="3">OBSERVACIONES</td>
                                    </tr>
                                    
                                    @foreach($RPD->seguimientosRPD as $key => $seguimiento)
                                        <tr>
                                            <td style="text-align: center">
                                                {{ $seguimiento->numeroContacto }}
                                            </td>
                                            <td colspan="3">
                                                ESTATUS: {{ $seguimiento->estatusSeguimientoRPD->nombreEstatusSeguimiento }}
                                            </td>
                                            <td style="text-align: center">
                                                {{ $seguimiento->fechaContactoSeguimientoFormat }}
                                            </td>
                                            <td colspan="3">NOTA: {{ $seguimiento->descripcionSeguimiento }} </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
 
            <table style="border: 3px solid #000; width: 100%; border-top: 0 !important">
                <tbody>
                    <tr>
                        <td>
                            <table style="width: 100%">
                                <tbody>
                                    
                                    <tr>
                                        <td colspan="6">
                                            <br>
                                            <b>CONDICIÓN DE LOCALIZACIÓN</b>
                                            <br>&nbsp;
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td colspan="3">ESTATUS LOCALIZACION</td>
                                        <td colspan="3">
                                            {{ optional($RPD->condicionLocalizacion->estatusLocalizacion)->nombreEstatusLocalizacion }}
                                        </td>
                                    </tr>

                                    @if(optional($RPD->condicionLocalizacion)->idEstatusLocalizacion==1)
                                        <tr>
                                            <td colspan="3">MOTIVO REFERIDO</td>
                                            <td colspan="3">
                                                {{ optional($RPD->condicionLocalizacion->motivoReferido)->nombreMotivoReferido }}
                                            </td>
                                        </tr>

                                          <tr>
                                            <td colspan="3">ESTADO DE LA PERSONA</td>
                                            <td colspan="3">
                                                {{ optional($RPD->condicionLocalizacion->estadoPersona)->nombreEstadoPersona }}
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="3" >TIPO DE LUGAR</td>
                                            <td colspan="3">
                                                {{ optional($RPD->condicionLocalizacion->tipoLugar)->nombreTipoLugar }}
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="3">CONSTANCIA</td>
                                            <td colspan="3">
                                                {{ optional($RPD->condicionLocalizacion->constancia)->nombreConstancia }}
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="3"> DELITO</td>
                                            <td colspan="3">
                                                {{ optional($RPD->condicionLocalizacion->delito)->nombreDelito }}
                                            </td>
                                        </tr>
                                    @endif

                                    @if(optional($RPD->condicionLocalizacion)->idEstatusLocalizacion==2)
                                        <tr>
                                            <td colspan="3">TIPO DE LUGAR</td>
                                            <td colspan="3">
                                                {{ optional($RPD->condicionLocalizacion->tipoLugar)->nombreTipoLugar }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" >CAUSA DEFUNCIÓN</td>
                                            <td colspan="3">
                                                {{ optional($RPD->condicionLocalizacion->causaDefuncion)->nombreCausaDefuncion }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">DELITO</td>
                                            <td colspan="3">
                                                {{ optional($RPD->condicionLocalizacion->delito)->nombreDelito }}
                                            </td>
                                        </tr>
                                    @endif

                                    <tr>
                                        <td colspan="6">OBSERVACIONES</td>
                                    </tr>

                                    <tr>
                                        <td  colspan="6"style="word-break:break-all; word-wrap:break-word;">
                                            {{ optional($RPD->condicionLocalizacion)->observacion }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table style="border: 3px solid #000; width: 100%; border-top: 0 !important">
                <tbody>
                    <tr>
                        <td>
                            <table style="width: 100%">
                                <tbody>
                                    <tr>
                                        <td colspan="3">
                                            CANALIZACIONES
                                        </td>
                                        <td colspan="3">
                                            CANALIZACIONES
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ENVIO DE SOLICITUDES DE APOYO A DEPENDENCIAS FIPED</td>
                                        <td colspan="2"></td>
                                        <td>
                                            {{ optional($RPD->canalizacionRPD)->solicitudApoyoText }}
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td>APERTURA DE CARPETA</td>
                                        <td colspan="2"></td>
                                        <td>
                                            {{ optional($RPD->canalizacionRPD)->aperturaCarpetaText }}
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td>BUSQUEDA SIRILO</td>
                                        <td colspan="2"></td>
                                        <td>
                                            {{ optional($RPD->canalizacionRPD)->busquedaSiriloText }}
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td>PLANES DE BUSQUEDA</td>
                                        <td colspan="2"></td>
                                        <td>
                                            {{ optional($RPD->canalizacionRPD)->planBusquedaText }}
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td>ENTREVISTA A FAMILIARES</td>
                                        <td colspan="2"></td>
                                        <td>
                                            {{ optional($RPD->canalizacionRPD)->entrevistaFamiliaresText }}
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td>AM/PM</td>
                                        <td colspan="2"></td>
                                        <td>
                                            {{ optional($RPD->canalizacionRPD)->AMPMText }}
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" style="text-align: center">HISTORIAL DE ACCIONES Y REGISTROS</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" style="text-align: justify;">
                                            @foreach($RPD->carpetasRPD()->with('usuario')->get() as $key =>$carpeta)
                                                {!! $carpeta->carpetaText !!}<br>
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" style="text-align: center">
                                            <br><br><br>
                                            {{ Date('d-m-Y h:m:s')}} / {{ $data[0]->funcionarioRegistro}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>

        </main>
    </body>
</html>
