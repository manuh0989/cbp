@if($errors->any())
	<div class="p-4 border-bottom">

		<div class="alert alert-danger alert-icon borders-left-lg" role="alert">
			<button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<div class="alert-icon-aside">
				<i class="far alert-triangle"></i>
			</div>
			<div class="alert-icon-content">
				<h6 class="alert-heading">Favor de validar los siguientes mensajes</h6>
				{{ session('message') }}
				<ul>
		        	@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
	        	</ul>
			</div>
		</div>

	</div>
@endif

