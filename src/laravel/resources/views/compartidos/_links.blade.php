<link rel="stylesheet" href="{{ asset('css/styles.css') }}"  />

<link rel="stylesheet" href="{{ asset('css/bootstrap/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/gijgo/gijgo.min.css') }}">

<link rel="icon" type="image/x-icon" href="{{ asset('assets/img/favicon.png') }}" />
