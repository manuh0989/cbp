<script src="{{ asset('js/jquery/jquery.js') }}" crossorigin="anonymous"></script>
        
<script src="{{ asset('js/bootstrap/bootstrap.bundle.min.js') }}"></script>

<script src="{{ asset('js/dataTable/jquery.dataTables.min.js') }}" crossorigin="anonymous"></script>
<script src="{{ asset('js/dataTable/dataTables.bootstrap4.min.js') }}" crossorigin="anonymous"></script>

<script src="{{ asset('js/jquery-mask/jquery.mask.js') }}" crossorigin="anonymous"></script>

<script src=" https://cdn.ckeditor.com/ckeditor5/12.3.1/classic/ckeditor.js"></script>

<script src="{{ asset('js/ckeditor/lang/es.js') }}"></script>



<!--<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>-->

<script src="{{ asset('js/gijgo/gijgo.js') }}" crossorigin="anonymous"></script>
<script data-search-pseudo-elements defer src="{{ asset('js/font-awesome/all.min.js') }}" crossorigin="anonymous"></script>
<script src="{{ asset('js/feather-icons/feather.min.js') }}" crossorigin="anonymous"></script>


<script>
	
  $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
   }); 

  function  mostrarEditarCatalogo(obj) {
      $('#span'+obj.id).hide();
      $('#frm'+obj.id).show();
      $('#txt'+obj.id).focus();
  }

  function editarCatalogo(obj){
    var frm =$('#frm'+obj.id);
    var url =frm.attr('action');
    
    $.ajax({
      url:url
      ,type:'POST'
      ,data:frm.serialize()
      ,dataType:'JSON'
      ,error:function(data){
        
      }
      ,success:function(data){
        $('#span'+obj.id).show();
        $('#frm'+obj.id).hide();

        $('#'+data.span).html(data.dato);
      } 
    });
  }

  function trashedCatalogo(obj){
    let idCatalogo =$(obj).attr('idCatalogo');
    let model      =$(obj).attr('model');
    let url        =$(obj).attr('url');
    $.ajax({
      url:url
      ,type:'PUT'
      ,data:{
        idCatalogo:idCatalogo
        ,model:model
      }
      ,dataType:'JSON'
      ,error:function(data){
        
      }
      ,success:function(data){
        mostrarCatalogos($('#catalogos'));
      } 
    });
  }

  function restoreCatalogo(obj){
    let idCatalogo =$(obj).attr('idCatalogo');
    let model      =$(obj).attr('model');
    let url        =$(obj).attr('url');
    $.ajax({
      url:url
      ,type:'PUT'
      ,data:{
        idCatalogo:idCatalogo
        ,model:model
      }
      ,dataType:'JSON'
      ,error:function(data){
        
      }
      ,success:function(data){
        mostrarCatalogos($('#catalogos'));
      } 
    });
  }

  function spinner(){
    return '<div class="spinner-border spinner-border-sm" role="status"><span class="sr-only">Loading...</span></div>';
  }

  function llenarToastInformacion(){
      $('#accordionDatoGeneral').show();
      
      var statusLocalizacion =$('#statusLocalizacion').val();
      var nombrePD           =$('#nombrePD').val();
      var primerApellidoPD   =$('#primerApellidoPD').val();
      var segundoApellidoPD  =$('#segundoApellidoPD').val();

    
      $('#statusList').html(statusLocalizacion);
      $('#nombreCompletoList').html(nombrePD+' '+primerApellidoPD+' '+segundoApellidoPD);
  }

  function restore(obj){
      var id         = $(obj).attr('id');
      var frmRestore = $(obj).attr('frmRestore');
      var frmRestore = $('#'+frmRestore);

      var url = frmRestore.attr('action');
        url =url.replace(':id',id);

      frmRestore.attr('action',url);
      frmRestore.submit();
  }

  function trash(obj){
      var id       = $(obj).attr('id');
      var frmTrash = $(obj).attr('frmTrash');
      var frmTrash = $('#'+frmTrash);

      var url = frmTrash.attr('action');
        url =url.replace(':id',id);

      frmTrash.attr('action',url);
      frmTrash.submit();
  }

  function trashFile(obj){
      var archivo  = $(obj).attr('archivo');
      var frmTrash = $(obj).attr('frmTrash');
      var frmTrash = $('#'+frmTrash);

      var url = frmTrash.attr('action');
      $('#RPDCarpetaArchivo').val(archivo);

      frmTrash.attr('action',url);
      frmTrash.submit(); 
  }

  function mostrarCatalogos(obj){

      var url   =$(obj).attr('url');
      var model = $(obj).val();

      $.ajax({
        url:url
        ,type:'POST'
        ,data:{
          model:model
        }
        ,dataType:'JSON'
        ,success:function(data){
          $('#divCatalogo').html(data.view);
        }
      });
  }


  function ajaxMunicipios(obj){

    var idEstado          =$(obj).val();
    var idMunicipio       =$(obj).attr('idMunicipio');
    var url               =$(obj).attr('url');
    var id                =$(obj).attr('id');
    var nameAttrMunicipio =$(obj).attr('nameAttrMunicipio');
    var idAttrMunicipio   =$(obj).attr('idAttrMunicipio');
    var idAttrLocalidad   =$(obj).attr('idAttrLocalidad');
    var divMunicipio      =$(obj).attr('divMunicipio');
    var divLocalidad      =$(obj).attr('divLocalidad');
    var idLocalidad       =$(obj).attr('idLocalidad');
    var nameAttrMunicipio =$(obj).attr('nameAttrMunicipio');
    var nameAttrLocalidad =$(obj).attr('nameAttrLocalidad');

    $.ajax({
      url:url
      ,type:'POST'
      ,data:{
        idEstado:idEstado
        ,idMunicipio:idMunicipio
        ,idLocalidad:idLocalidad
        ,nameAttrMunicipio:nameAttrMunicipio
        ,idAttrMunicipio:idAttrMunicipio
        ,idAttrLocalidad:idAttrLocalidad
        ,nameAttrLocalidad:nameAttrLocalidad
        ,divMunicipio:divMunicipio
        ,divLocalidad:divLocalidad

      }
      ,dataType:'JSON'
      ,beforeSend:function(){
        $("#"+divMunicipio).html(spinner());
      }
      ,success:function(data){
        $("#"+divMunicipio).html(data.view);
      }
      ,complete:function(){
        ajaxLocalidades($('#'+idAttrMunicipio));
      }
    });
  }

  function ajaxLocalidades(obj){
    var idMunicipio       =$(obj).val();
    var idLocalidad       =$(obj).attr('idLocalidad');
    var url               =$(obj).attr('url');
    var divLocalidad      =$(obj).attr('divLocalidad');
    var nameAttrLocalidad =$(obj).attr('nameAttrLocalidad');
    var idAttrLocalidad   =$(obj).attr('idAttrLocalidad');
    
    $.ajax({
      url:url
      ,type:'POST'
      ,data:{
        idMunicipio:idMunicipio
        ,idLocalidad:idLocalidad
        ,nameAttrLocalidad:nameAttrLocalidad
        ,idAttrLocalidad:idAttrLocalidad
      }
      ,dataType:'JSON'
      ,beforeSend:function(){
        $('#'+divLocalidad).html(spinner());
      }
      ,success:function(data){
        $('#'+divLocalidad).html(data.view);
      }
    });
  }

  function mostarLocalizacion(obj){

    let btnLocalizacion=$('#btnAcordionLocalizacion');
    let collapsebtn=btnLocalizacion.attr('data-target');
    if($(obj).val()=='LOCALIZADO'){
        btnLocalizacion.removeClass('disabled');
        $(collapsebtn).collapse('show');
        $('html,body').animate({
           scrollTop: btnLocalizacion.offset().top

        }, 'slow');
    }else{
        btnLocalizacion.addClass('disabled');
        $(collapsebtn).collapse('hide');
    }
  }

  function camposEstatusLocalizacion(obj){
    let statusLocalizacion = $(obj);
    $.ajax({
      url:statusLocalizacion.attr('url')
      ,type:'POST'
      ,data:{
        idEstatusLocalizacion:statusLocalizacion.val()
        ,idTipoLugar:statusLocalizacion.attr('idTipoLugar')
        ,idMotivoReferido:statusLocalizacion.attr('idMotivoReferido')
        ,idEstadoPersona:statusLocalizacion.attr('idEstadoPersona')
        ,idConstancia:statusLocalizacion.attr('idConstancia')
        ,idDelito:statusLocalizacion.attr('idDelito')
        ,idCausaDefuncion:statusLocalizacion.attr('idCausaDefuncion')
      }
      ,dataType:'JSON'
      ,beforeSend:function(){
        $('#conVidaSinVida').html(spinner());
      }
      ,success:function(data){
        $('#conVidaSinVida').html(data.view);
      }
    });
  }

  $( document ).ready(function(){

    $('.date-mask').mask('00-00-0000'); 
    $('.phone-mask').mask('(00) 0000-0000');
    
    $('.toUpperCase').keyup(function(event){
      $(this).val($(this).val().toUpperCase());
    });
      
  	$('.btn-submit').click(function(event) {
      	//event.preventDefault();
  	    $(this).prop('disabled','disabled');
        $(this).html(spinner());
        //window.open("/simInfPdfViewTest?nombre=" + nombre + "&nombreJugador1=" + jugador1, "width=500,height=300,scrollbars=NO");
  	    $(this).closest("form").submit();
      
  	});
    
  	$('#lnkLogout').click(function(event) {
      	$('#frmLogout').submit();
  	});



  	  "use strict";

    // Enable Bootstrap tooltips via data-attributes globally
    $('[data-toggle="tooltip"]').tooltip();

    // Enable Bootstrap popovers via data-attributes globally
    $('[data-toggle="popover"]').popover();

    $(".popover-dismiss").popover({
      trigger: "focus"
    });

    // Add active state to sidbar nav links
    var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
    $("#layoutSidenav_nav .sidenav a.nav-link").each(function() {
      if (this.href === path) {
        $(this).addClass("active");
      }
    });

    // Toggle the side navigation
    $("#sidebarToggle").on("click", function(e) {
      e.preventDefault();
      $("body").toggleClass("sidenav-toggled");
    });

    // Activate Feather icons
    feather.replace();

    // Activate Bootstrap scrollspy for the sticky nav component
    $("body").scrollspy({
      target: "#stickyNav",
      offset: 82
    });

    // Scrolls to an offset anchor when a sticky nav link is clicked
    $('.nav-sticky a.nav-link[href*="#"]:not([href="#"])').click(function() {
      if (
        location.pathname.replace(/^\//, "") ==
          this.pathname.replace(/^\//, "") &&
        location.hostname == this.hostname
      ) {
        var target = $(this.hash);
        target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
        if (target.length) {
          $("html, body").animate(
            {
              scrollTop: target.offset().top - 81
            },
            200
          );
          return false;
        }
      }
    });

    // Click to collapse responsive sidebar
    $("#layoutSidenav_content").click(function() {
      const BOOTSTRAP_LG_WIDTH = 992;
      if (window.innerWidth >= 992) {
        return;
      }
      if ($("body").hasClass("sidenav-toggled")) {
          $("body").toggleClass("sidenav-toggled");
      }
    });

    // Init sidebar
    let activatedPath = window.location.pathname.match(/([\w-]+\.html)/, '$1');

    if (activatedPath) {
      activatedPath = activatedPath[0];
    }
    else {
      activatedPath = 'index.html';
    }
      
    let targetAnchor = $('[href="' + activatedPath + '"]');
    let collapseAncestors = targetAnchor.parents('.collapse');
    
    targetAnchor.addClass('active');
    
    collapseAncestors.each(function() {
      $(this).addClass('show');
      $('[data-target="#' + this.id +  '"]').removeClass('collapsed');
       
    })
  });
</script>