<div class="datatable table-responsive">
	<table id="{{ $idTable }}" class=" table table-bordered  table-hover table-sm" style="width:100%">
		<thead>
			<tr>
				@foreach($encabezados as $key  => $encabezado)
					<th>
						{{ $encabezado }}
					</th>
				@endforeach
		</tr>
		</thead>
		<tbody>
			{{ $slot }}
		</tbody>
	</table>
</div>