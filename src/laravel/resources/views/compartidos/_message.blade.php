@if (session('message'))
	<div class="p-4 border-bottom">
		<div class="alert alert-primary alert-icon border-left-lg" role="alert">
			<button class="close" type="button" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<div class="alert-icon-aside">
				<i class="fas fa-check"></i>
			</div>
			<div class="alert-icon-content">
				{{ session('message') }}
			</div>
		</div>
	</div>
@endif