<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class EstadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::unprepared(File::get('storage/app/cat_localidad_seeder.sql'));
    }
}
