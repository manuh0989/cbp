<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(){
        //$this->truncateTable();
        
        $this->call([
			UsuariosSeeder::class
			,FuenteReporteSeeder::class
            ,EstadosSeeder::class
            ,LocalidadesCDMXSeeder::class
            ,CatalogosSeeder::class
            ,MasivoSeeder::class
        ]);
    }

    protected function truncateTable(){
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        
        foreach ($this->getTables() as $key =>$table) {
            DB::table($table->Tables_in_DEV_CBP)->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }

    protected function getTables(){
        return DB::select('SHOW TABLES;');
    }
}
