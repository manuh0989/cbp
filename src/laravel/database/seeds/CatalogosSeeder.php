<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class CatalogosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::unprepared(File::get('storage/app/CATALOGOS_DEV_CBP.sql'));
    }
}
