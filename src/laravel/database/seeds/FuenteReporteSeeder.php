<?php

use Illuminate\Database\Seeder;
use App\Models\{FuenteReporte
    ,OrigenNoticia
    ,Vulnerabilidad
    ,EstadoCivil
    ,Escolaridad
    ,Sexo
    ,CircunstanciaDesaparicion
    ,HipotesisDesaparicion
    ,Parentesco
    ,Complexion
    ,EstatusSeguimientoRPD
    ,SeguimientoRPD
    ,MotivoReferido
    ,EstatusLocalizacion
    ,TipoLugar,FolioCBP
};
class FuenteReporteSeeder extends Seeder{
    
    

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

       (new FolioCBP(FuenteReporte::COLABORADOR))->generarFolioCBP(); // COLABORADOR
       (new FolioCBP(FuenteReporte::NOTICIA))->generarFolioCBP(); //NOTICIA
       (new FolioCBP(FuenteReporte::REPORTE))->generarFolioCBP(); //REPORTE

    }
}
