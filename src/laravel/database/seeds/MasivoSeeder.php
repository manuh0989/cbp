<?php

use Illuminate\Database\Seeder;

class MasivoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::unprepared(File::get('storage/app/MASIVO.sql'));
    }
}
