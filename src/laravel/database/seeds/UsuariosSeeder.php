<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use App\User;
use App\Models\Role;
class UsuariosSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

        $roleAdmin=Role::create([
            'role'=>'admin'
        ]);

        $roleUsuario=Role::create([
            'role'=>'usuario'
        ]);

        $roleUsuario=Role::create([
            'role'=>'invitado'
        ]);
        
        $user=User::create([
            'idRole'             =>$roleAdmin->idRole
            ,'nombre'            =>'Ignacio Manuel'
            ,'apellido_paterno'  =>'Sanchez'
            ,'apellido_materno'  =>'Neri'
            ,'email'             =>'manuh0989@gmail.com'
            ,'username'          =>'manuh0989'
            ,'password'          =>Hash::make('0989nacho')
            ,'email_verified_at' =>Carbon::now()

        ]);

        


        User::create([
            'idRole'             =>$roleAdmin->idRole
            ,'nombre'            =>'Fabian'
            ,'apellido_paterno'  =>'Cajero'
            ,'apellido_materno'  =>'Cajero'
            ,'email'             =>'fcajero@gmail.com'
            ,'username'          =>'fcajero'
            ,'password'          =>Hash::make('fcajero')
            ,'email_verified_at' =>Carbon::now()

        ]);



        $user=User::create([
            'idRole'             =>$roleUsuario->idRole
            ,'nombre'            =>'user'
            ,'apellido_paterno'  =>'user'
            ,'apellido_materno'  =>'user'
            ,'email'             =>'user@gmail.com'
            ,'username'          =>'user'
            ,'password'          =>Hash::make('user')
            ,'email_verified_at' =>Carbon::now()
        ]);
    
        //$user->assign('usuario');
        //$user->assign('usuario');

        User::create([
            'idRole'             =>$roleAdmin->idRole
            ,'nombre'            =>'Francisco'
            ,'apellido_paterno' =>'Melchor'
            ,'apellido_materno' =>'Rodriguez'
            ,'email'            =>'franciscoshark385@gmail.com'
            ,'username'         =>'fmelcho'
            ,'password'         =>Hash::make('shark3856')
            ,'email_verified_at'=>Carbon::now()

        ]);

    }
}
