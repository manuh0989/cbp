<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class LocalidadesCDMXSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::unprepared(File::get('storage/app/cat_cdmx_seeder.sql'));
    }
}
