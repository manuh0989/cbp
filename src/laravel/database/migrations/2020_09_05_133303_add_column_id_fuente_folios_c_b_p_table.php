<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Carbon;
use App\Models\{FolioCBP,FuenteReporte};
class AddColumnIdFuenteFoliosCBPTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('foliosCBP', function (Blueprint $table) {
            $table->string('anio')->nullable()->change();
            $table->dropUnique('foliosCBP_anio_unique');
            $table->string('prefijo', 10)->change();
            $table->unsignedBigInteger('idFuente')->after('idFolioCBP')->nullable();
        });
        
        FolioCBP::where('idFolioCBP',1)->update(['idFuente'=>1]);
        
        Schema::table('foliosCBP', function (Blueprint $table) {
            $table->unsignedBigInteger('idFuente')->nullable(false)->change();
            $table->foreign('idFuente')->references('idFuente')->on('fuenteReporte');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
