<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRPDSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        Schema::create('nacionalidades', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idNacionalidad');
            $table->string('nombreNacionalidad',50);
            $table->timestamps();
            $table->softDeletes();
        });

         Schema::create('estatusLocalizacion', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idEstatusLocalizacion');            
            $table->string('nombreEstatusLocalizacion',50);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tipoLugar', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idTipoLugar');            
            $table->string('nombreTipoLugar',50);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('motivosReferido', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idMotivoReferido');            
            $table->string('nombreMotivoReferido',50);
            $table->timestamps();
            $table->softDeletes();
        });
  


        Schema::create('condicionLocalizacion', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idCondicionLocalizacion');
            $table->unsignedBigInteger('idRPD');         
            $table->unsignedBigInteger('idEstatusLocalizacion')->nullable();         
            $table->unsignedBigInteger('idTipoLugar')->nullable();         
            $table->unsignedBigInteger('idMotivoReferido')->nullable();
            $table->integer('idEstado')->nullable();
            $table->integer('idMunicipio')->nullable();
            $table->integer('idLocalidad')->nullable();
            
            $table->date('fechaLocalizacion')->nullable();
            $table->string('calleLugar')->nullable();
            $table->string('delitoCometido')->nullable();
            $table->boolean('bajaReporte')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });



        Schema::create('tiposBoca', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idTipoBoca');            
            $table->string('nombreTipoBoca',50);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('lugaresDesaparicion', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idLugarDesaparicion');            
            $table->string('nombreLugarDesaparicion',50);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tiposLongitudCabello', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idTipoLongitudCabello');            
            $table->string('nombreTipoLogitudCabello',50);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tiposCejas', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idTipoCejas');            
            $table->string('nombreTipoCejas',50);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tiposCara', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idTipoCara');            
            $table->string('nombreTipoCara',50);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tiposCeja', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idTipoCeja');            
            $table->string('nombreTipoCeja',50);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tiposColorOjos', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idTipoColorOjos');
            $table->string('nombreTipoColorOjos',50);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tiposFrente', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idTipoFrente');
            $table->string('nombreTipoFrente',50);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tiposLabio', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idTipoLabio');
            $table->string('nombreTipoLabio',50);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tiposLongitudOjos', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idTipoLongitudOjos');
            $table->string('nombreLongitudOjos',50);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tiposMenton', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idTipoMenton');
            $table->string('nombreTipoMenton',50);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tiposNariz', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idTipoNariz');
            $table->string('nombreTipoNariz',50);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tiposTez', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idTipoTez');
            $table->string('nombreTipoTez',50);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tiposCabello', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idTipoCabello');
            $table->string('nombreTipoCabello',50);
            $table->timestamps();
            $table->softDeletes();
        });



        Schema::create('foliosCBP', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idFolioCBP');
            $table->string('prefijo',5)->default('CBP');
            $table->string('consecutivo',10);
            $table->year('anio')->unique();
            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('complexiones', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idComplexion');
            $table->string('nombreComplexion',255);
            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('parentescos', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idParentesco');
            $table->string('nombreParentesco',255);
            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('circunstanciasDesapariciones', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idCircunstanciaDesaparicion');
            $table->string('nombreCircunstanciaDesaparicion',255);
            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('hipotesisDesapariciones', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idHipotesisDesaparicion');
            $table->string('nombreHipotesisDesaparicion',255)->unique();
            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('sexos', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idSexo');
            $table->string('nombreSexo',100)->unique();
            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('escolaridades', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idEscolaridad');
            $table->string('nombreEscolaridad',255)->unique();
            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('estadosCivil', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idEstadoCivil');
            $table->string('nombreEstadoCivil',255)->unique();
            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('vulnerabilidades', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idVulnerabilidad');
            $table->string('nombreVulnerabilidad',255)->unique();
            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('fuenteReporte', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idFuente');
            $table->string('nombreFuente',255)->unique();
            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('origenNoticias', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idOrigenNoticia');
            $table->string('nombreOrigenNoticia',255)->unique();
            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('RPD', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idRPD');
            $table->unsignedBigInteger('idFuente');
            $table->string('folioCBP')->unique();
            $table->string('statusLocalizacion');
            $table->string('funcionarioRegistro',255);
            $table->string('folioFIPEDE',50)->unique()->nullable();
            $table->string('folioLocatel',50)->unique()->nullable();

            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('fotografiasRPD', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idFotografia');
            $table->unsignedBigInteger('idRPD');
            $table->string('foto',255)->unique();
            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('personasDesaparecidas', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idPersonaDesaparecida');
            $table->unsignedBigInteger('idRPD');
            $table->unsignedBigInteger('idOrigenNoticia')->nullable();
            $table->unsignedBigInteger('idSexo')->nullable();
            $table->unsignedBigInteger('idEscolaridad')->nullable();
            $table->unsignedBigInteger('idEstadocivil')->nullable();
            $table->unsignedBigInteger('idVulnerabilidad')->nullable();
            $table->unsignedBigInteger('idNacionalidad')->nullable();

            $table->string('nombrePD',255);
            $table->string('primerApellidoPD',255);
            $table->string('segundoApellidoPD',255)->nullable();
            
            $table->date('fechaNacimientoPD')->nullable();
            $table->integer('edadPD')->nullable();
            
            $table->string('lugarNacimientoPD',255)->nullable();
            $table->string('CURPPD',255)->nullable();
            $table->string('RFCPD',255)->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('datosGeneralesRPD', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idDatoGeneralRPD');
            $table->unsignedBigInteger('idRPD');
            $table->unsignedBigInteger('idPersonaDesaparecida');
            $table->integer('idEstado')->nullable();
            $table->integer('idMunicipio')->nullable();
            $table->integer('idLocalidad')->nullable();
            
            
            $table->dateTime('fechaDesaparicion')->nullable();
            $table->time('horaDesaparicion', 0)->nullable();
            $table->string('calleNumeroDesaparicion',255)->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('hechosDesaparicionesRPD', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idHechoDesaparicion');
            $table->unsignedBigInteger('idRPD');
            $table->unsignedBigInteger('idPersonaDesaparecida');
            $table->unsignedBigInteger('idCircunstanciaDesaparicion')->nullable();
            $table->unsignedBigInteger('idHipotesisDesaparicion')->nullable();
            $table->unsignedBigInteger('idLugarDesaparicion')->nullable();
            
            $table->longText('descripcionHechosDesaparicion')->nullable();
            $table->longText('fotovolanteHechosDesaparicion')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('reportantesRPD', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idReportante');
            $table->unsignedBigInteger('idRPD');
            $table->unsignedBigInteger('idParentesco')->nullable();
            $table->unsignedBigInteger('idPersonaDesaparecida');
            
            $table->string('nombreCompletoReportante')->nullable();
            $table->integer('edadReportante')->nullable();
            $table->string('telefonoReportante',255)->nullable();
            $table->string('telefono2Reportante',255)->nullable();
            $table->string('telefono3Reportante',255)->nullable();
            $table->string('correoReportante',255)->nullable();
            $table->string('redSocialReportante',255)->nullable();
            $table->string('redSocial2Reportante',255)->nullable();
            $table->longText('otroContactoMedio')->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('mediaFiliacionesRPD', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idMediaFiliacion');
            $table->unsignedBigInteger('idRPD');
            $table->unsignedBigInteger('idGeneroReportado')->nullable();
            $table->unsignedBigInteger('idComplexion')->nullable();

            $table->string('tez',255)->nullable();
            $table->string('frente',255)->nullable();
            $table->string('boca',255)->nullable();
            $table->string('cejas',255)->nullable();
            $table->string('menton',255)->nullable();
            $table->string('colorOjos',255)->nullable();
            $table->string('tipoCabello',255)->nullable();
            $table->string('longitudCabello',255)->nullable();
            $table->double('estatura',10,2)->nullable();
            $table->string('cara',255)->nullable();
            $table->string('nariz',255)->nullable();
            $table->string('labios',255)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('transaccionesRPD', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idTransaccionRPD');
            $table->unsignedBigInteger('idRPD');
            $table->unsignedBigInteger('idUsuario');
            $table->string('accion',255)->nullable();
            $table->longText('datos')->nullable();

            $table->timestamps();
        });

        Schema::create('roles', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('idRole');
            $table->string('role',255);

            $table->timestamps();
            $table->softDeletes();
        });       


        Schema::create('estatusSeguimientoRPD', function (Blueprint $table) {
            $table->bigIncrements('idEstatusSeguimientoRPD');
            $table->string('nombreEstatusSeguimiento');
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create('seguimientosRPD', function (Blueprint $table) {
            $table->bigIncrements('idSeguimientoRPD');
            $table->unsignedBigInteger('idRPD');
            $table->unsignedBigInteger('idEstatusSeguimientoRPD');
            $table->integer('numeroContacto');
            $table->string('funcionarioSeguimiento');
            $table->date('fechaContactoSeguimiento');
            $table->longText('descripcionSeguimiento');
            $table->timestamps();
            $table->softDeletes();
        }); 

        Schema::create('canalizacionesRPD', function (Blueprint $table) {
            $table->bigIncrements('idCanalizacionRPD');
            $table->unsignedBigInteger('idRPD');
            $table->boolean('solicitudApoyo');
            $table->boolean('busquedaSirilo');
            $table->boolean('aperturaCarpeta');
            $table->boolean('planBusqueda');
            $table->boolean('entrevistaFamiliares');
            $table->boolean('AMPM');
            $table->timestamps();
            $table->softDeletes();
        }); 

          Schema::create('carpetasRPD', function (Blueprint $table) {
            $table->bigIncrements('idCarpetaRPD');
            $table->unsignedBigInteger('idRPD');
            $table->unsignedBigInteger('idUsuario');
            $table->longText('carpeta');
            
            $table->timestamps();
            $table->softDeletes();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RPD');
        Schema::dropIfExists('fuenteReporte');
    }
}
