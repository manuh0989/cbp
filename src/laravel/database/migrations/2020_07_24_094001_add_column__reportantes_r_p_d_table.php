<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnReportantesRPDTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reportantesRPD', function (Blueprint $table) {
            $table->boolean('RNPDNO')->nullable()->after('otroContactoMedio');
            $table->boolean('contactoInicial')->nullable()->after('RNPDNO');
        });

        Schema::table('mediaFiliacionesRPD', function (Blueprint $table) {
            $table->longText('descripcion')->nullable()->after('labios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reportantesRPD', function (Blueprint $table) {
            //
        });
    }
}
