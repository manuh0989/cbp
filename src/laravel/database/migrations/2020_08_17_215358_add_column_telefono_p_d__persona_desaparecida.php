<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTelefonoPDPersonaDesaparecida extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personasDesaparecidas', function (Blueprint $table) {
            $table->string('telefonoPD')->nullable()->after('RFCPD');
            $table->string('redSocialPD')->nullable()->after('telefonoPD');
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
