<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeginKeyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        
        Schema::table('usuarios',function(Blueprint $table){
            $table->foreign('idRole')->references('idRole')->on('roles')->onDelete('cascade');
        });            

        Schema::table('transaccionesRPD',function(Blueprint $table){
            $table->foreign('idRPD')->references('idRPD')->on('RPD')->onDelete('cascade');
            $table->foreign('idUsuario')->references('idUsuario')->on('usuarios')->onDelete('cascade');
        });            

        Schema::table('reportantesRPD',function(Blueprint $table){
            $table->foreign('idRPD')->references('idRPD')->on('RPD')->onDelete('cascade');
            $table->foreign('idParentesco')->references('idParentesco')->on('parentescos')->onDelete('cascade');
            $table->foreign('idPersonaDesaparecida')->references('idPersonaDesaparecida')->on('personasDesaparecidas')->onDelete('cascade');

        });

        Schema::table('datosGeneralesRPD',function(Blueprint $table){
            $table->foreign('idRPD')->references('idRPD')->on('RPD');
            $table->foreign('idPersonaDesaparecida')->references('idPersonaDesaparecida')->on('personasDesaparecidas')->onDelete('cascade');
            
            $table->foreign('idEstado')->references('id')->on('estados')->onDelete('cascade');
            $table->foreign('idMunicipio')->references('id')->on('municipios')->onDelete('cascade');
            $table->foreign('idLocalidad')->references('id')->on('localidades')->onDelete('cascade');

        });

        Schema::table('RPD',function(Blueprint $table){
            $table->foreign('idFuente')->references('idFuente')->on('fuenteReporte')->onDelete('cascade');

        });

        Schema::table('fotografiasRPD',function(Blueprint $table){
            $table->foreign('idRPD')->references('idRPD')->on('RPD')->onDelete('cascade');
        });


        Schema::table('personasDesaparecidas',function(Blueprint $table){

             $table->foreign('idOrigenNoticia')->references('idOrigenNoticia')->on('origenNoticias')->onDelete('cascade');
             $table->foreign('idSexo')->references('idSexo')->on('sexos')->onDelete('cascade');
             $table->foreign('idEscolaridad')->references('idEscolaridad')->on('escolaridades')->onDelete('cascade');
             $table->foreign('idEstadocivil')->references('idEstadocivil')->on('estadosCivil')->onDelete('cascade'); 
             $table->foreign('idVulnerabilidad')->references('idVulnerabilidad')->on('vulnerabilidades')->onDelete('cascade'); 

             $table->foreign('idNacionalidad')->references('idNacionalidad')->on('nacionalidades')->onDelete('cascade'); 
        });


        Schema::table('hechosDesaparicionesRPD',function(Blueprint $table){
            
            $table->foreign('idRPD')->references('idRPD')->on('RPD')->onDelete('cascade');
            $table->foreign('idPersonaDesaparecida')->references('idPersonaDesaparecida')->on('personasDesaparecidas')->onDelete('cascade');

            $table->foreign('idCircunstanciaDesaparicion')->references('idCircunstanciaDesaparicion')->on('circunstanciasDesapariciones')->onDelete('cascade');

            $table->foreign('idHipotesisDesaparicion')->references('idHipotesisDesaparicion')->on('hipotesisDesapariciones')->onDelete('cascade');

            $table->foreign('idLugarDesaparicion')->references('idLugarDesaparicion')->on('lugaresDesaparicion')->onDelete('cascade');

        });

        Schema::table('mediaFiliacionesRPD',function(Blueprint $table){
            $table->foreign('idRPD')->references('idRPD')->on('RPD')->onDelete('cascade');
            $table->foreign('idGeneroReportado')->references('idSexo')->on('sexos')->onDelete('cascade');
            $table->foreign('idComplexion')->references('idComplexion')->on('complexiones')->onDelete('cascade');
        });

        Schema::table('seguimientosRPD',function(Blueprint $table){
            $table->foreign('idEstatusSeguimientoRPD')->references('idEstatusSeguimientoRPD')->on('estatusSeguimientoRPD')->onDelete('cascade');

            $table->foreign('idRPD')->references('idRPD')->on('RPD')->onDelete('cascade');
        });

        Schema::table('canalizacionesRPD',function(Blueprint $table){
            $table->foreign('idRPD')->references('idRPD')->on('RPD')->onDelete('cascade');
        });

        Schema::table('carpetasRPD',function(Blueprint $table){
            $table->foreign('idRPD')->references('idRPD')->on('RPD')->onDelete('cascade');
            $table->foreign('idUsuario')->references('idUsuario')->on('usuarios')->onDelete('cascade');
        });

        Schema::table('condicionLocalizacion',function(Blueprint $table){
            $table->foreign('idRPD')->references('idRPD')->on('RPD')->onDelete('cascade');
            $table->foreign('idEstatusLocalizacion')->references('idEstatusLocalizacion')->on('estatusLocalizacion')->onDelete('cascade');
            $table->foreign('idTipoLugar')->references('idTipoLugar')->on('tipoLugar')->onDelete('cascade');
            $table->foreign('idMotivoReferido')->references('idMotivoReferido')->on('motivosReferido')->onDelete('cascade');

            $table->foreign('idEstado')->references('id')->on('estados')->onDelete('cascade');
            $table->foreign('idMunicipio')->references('id')->on('municipios')->onDelete('cascade');
            $table->foreign('idLocalidad')->references('id')->on('localidades')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
