<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstadoPersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        Schema::create('estadoPersona', function (Blueprint $table) {
            $table->bigIncrements('idEstadoPersona');
            $table->string('nombreEstadoPersona',50);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('constancia', function (Blueprint $table) {
            $table->bigIncrements('idConstancia');
            $table->string('nombreConstancia',50);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('delito', function (Blueprint $table) {
            $table->bigIncrements('idDelito');
            $table->string('nombreDelito',50);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('causaDefuncion', function (Blueprint $table) {
            $table->bigIncrements('idCausaDefuncion');
            $table->string('nombreCausaDefuncion',50);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('condicionLocalizacion', function (Blueprint $table) {

            $table->unsignedBigInteger('idEstadoPersona')->nullable()->after('idLocalidad');
            $table->unsignedBigInteger('idConstancia')->nullable()->after('idEstadoPersona');
            $table->unsignedBigInteger('idDelito')->nullable()->after('idConstancia');
            $table->unsignedBigInteger('idCausaDefuncion')->nullable()->after('idDelito');
            
            $table->foreign('idEstadoPersona')->references('idEstadoPersona')->on('estadoPersona')->onDelete('cascade');
            $table->foreign('idConstancia')->references('idConstancia')->on('constancia')->onDelete('cascade');
            $table->foreign('idDelito')->references('idDelito')->on('delito')->onDelete('cascade');
            $table->foreign('idCausaDefuncion')->references('idCausaDefuncion')->on('causaDefuncion')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_estado_persona');
    }
}
