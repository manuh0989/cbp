<?php

namespace App\Exports;

use App\Models\RPD;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;


class RPDExport implements FromQuery,WithHeadings{
	use Exportable;

	protected $request;

	public function __construct($request = null){
        
		$this->request=json_decode($request);
	}

    /**
    * @return \Illuminate\Support\Collection
    */
    public function query(){
        return (new RPD)->RPDExcelQuery($this->request);
    }

    public function headings() :array{
        return (new RPD)->encabezadoExcel;
    }
}
