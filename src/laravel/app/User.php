<?php

namespace App;

    
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;


use App\Models\Role;
class User extends Authenticatable implements MustVerifyEmail{
    use Notifiable;
    use SoftDeletes;

    protected $table      ="usuarios";
    protected $primaryKey ="idUsuario"; 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idRole'
        ,'nombre'
        ,'apellido_paterno'
        ,'apellido_materno'
        ,'email'
        ,'username'
        ,'password'
        ,'email_verified_at'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getNombreCompletoAttribute(){
        return "{$this->nombre} {$this->apellido_paterno} {$this->apellido_materno}";
    }

    public function role(){
        return $this->belongsTo(Role::class,'idRole','idRole');
    }

    public function isAdmin(){
        return $this->role->role === 'admin';
    }

    public function isUser(){
        return $this->role->role === 'usuario';
    }

    public function isInvitado(){
        return $this->role->role === 'invitado';
    }

    public function getNombreRoleAttribute(){
        return $this->role->role;
    }
}