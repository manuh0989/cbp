<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model{
    public $timestamps    = false;
	protected $table      ='estados';
	protected $primaryKey ='id';



	public static function getAllEstados(){
		return self::select(['id as idEstado','nombre as nombreEstado'])
		->get();
	}


}
