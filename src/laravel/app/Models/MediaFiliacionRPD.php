<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\MediaFiliacion\{Boca,Cara,Cejas,ColorOjos,Frente,Labios,LongitudOjos,Menton,Nariz,Tez,TipoCabello,LongitudCabello};
class MediaFiliacionRPD extends Model{
    use SoftDeletes;
    protected $table      ="mediaFiliacionesRPD";
	protected $primaryKey ='idMediaFiliacion';
	protected $fillable   =[
		'idRPD'
		,'idGeneroReportado'
		,'idComplexion'
		,'tez'
		,'frente'
		,'boca'
		,'cejas'
		,'menton'
		,'colorOjos'
		,'tipoCabello'
		,'longitudCabello'
		,'estatura'
		,'cara'
		,'nariz'
		,'labios'
		,'descripcion'
		,'otrosDatos'
	];

	public static function catalogos(){
		return [
			'boca'             =>Boca::all()
			,'cara'            =>Cara::all()
			,'cejas'           =>Cejas::all()
			,'colorOjos'       =>ColorOjos::all()
			,'frente'          =>Frente::all()
			,'labios'          =>Labios::all()
			,'longitudOjos'    =>LongitudOjos::all()
			,'longitudCabello' =>LongitudCabello::all()
			,'menton'          =>Menton::all()
			,'nariz'           =>Nariz::all()
			,'tez'             =>Tez::all()
			,'tipoCabello'     =>TipoCabello::all()
		];
	}

	public static function guardarActualizarMediaFiliacion($request,$idRPD){
		return self::updateOrCreate([
			'idRPD'=>$idRPD
		],[
			'idRPD'              =>$idRPD
			,'idGeneroReportado' =>$request->idGeneroReportado
			,'idComplexion'      =>$request->idComplexion
			,'tez'               =>$request->tez
			,'frente'            =>$request->frente
			,'boca'              =>$request->boca
			,'cejas'             =>$request->cejas
			,'menton'            =>$request->menton
			,'colorOjos'         =>$request->colorOjos
			,'tipoCabello'       =>$request->tipoCabello
			,'longitudCabello'   =>$request->longitudCabello
			,'estatura'          =>$request->estatura
			,'cara'              =>$request->cara
			,'nariz'             =>$request->nariz
			,'labios'            =>$request->labios
			,'descripcion'       =>$request->descripcion
			,'otrosDatos'        =>$request->otrosDatos
		]);
	}
}
