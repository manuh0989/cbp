<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\RPD;
class TransaccionRPD extends Model{
	protected $table      ="transaccionesRPD";
	protected $primaryKey ='idTransaccionRPD';
	protected $fillable   =[
	'idRPD'
	,'idUsuario'
	,'accion'
	];


	public function ultimoUsuarioActualizacion(RPD $RPD){
		return self::where('idRPD',$RPD->idRPD)->where('accion','updated')->get();
	}


	public static function transaccionRPD($RPD,$accion){
		return   TransaccionRPD::create([
            'idRPD'            =>$RPD->idRPD
            ,'idUsuario'       =>auth()->user()->idUsuario
            ,'accion'          =>$accion
            /*,'datos'           =>json_encode([
                ,'datoOriginal:'   =>$RPD->getOriginal()
                ,'datoActualziado' =>$RPD->getAttributes()
            ])*/
        ]);
	}
}
