<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class HechoDesaparicionRPD extends Model{
	use SoftDeletes;
    protected $table      ="hechosDesaparicionesRPD";
	protected $primaryKey ='idHechoDesaparicion';
	protected $fillable   =[
		'idRPD'
		,'idPersonaDesaparecida'
		,'idCircunstanciaDesaparicion'
		,'idHipotesisDesaparicion'
		,'idLugarDesaparicion'
		,'descripcionHechosDesaparicion'
		,'fotovolanteHechosDesaparicion'
	];

	public static function guardarActualizarHechosDesaparicion($request,$idRPD,$idPersonaDesaparecida){
		return self::updateOrCreate([
			'idRPD'=>$idRPD
		],[
			'idRPD'                         =>$idRPD
            ,'idPersonaDesaparecida'        =>$idPersonaDesaparecida
            ,'idLugarDesaparicion'          =>$request->idLugarDesaparicion
            ,'idCircunstanciaDesaparicion'  =>$request->idCircunstanciaDesaparicion
            ,'idHipotesisDesaparicion'      =>$request->idHipotesisDesaparicion
            ,'descripcionHechosDesaparicion' =>$request->descripcionHechosDesaparicion
            ,'fotovolanteHechosDesaparicion' =>$request->fotovolanteHechosDesaparicion
		]);
	}
}
