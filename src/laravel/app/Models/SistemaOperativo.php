<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SistemaOperativo extends Model{
    protected $userAgent;
    protected $plataformas;
    protected $plataforma;
    
    public function __construct(){
		
		$this->setUserAgent();
		$this->setPlataforma();
		$this->getPlataforma();
    }

    public function setUserAgent(){
    	$this->userAgent=$_SERVER['HTTP_USER_AGENT'];
    }

    public function getUserAgent(){
    	return $this->userAgent;
    }

    protected  function setPlataforma(){
		$this->plataformas =[
			'Windows 10'    => 'Windows NT 10.0+',
			'Windows 8.1'   => 'Windows NT 6.3+',
			'Windows 8'     => 'Windows NT 6.2+',
			'Windows 7'     => 'Windows NT 6.1+',
			'Windows Vista' => 'Windows NT 6.0+',
			'Windows XP'    => 'Windows NT 5.1+',
			'Windows 2003'  => 'Windows NT 5.2+',
			'Windows'       => 'Windows otros',
			'iPhone'        => 'iPhone',
			'iPad'          => 'iPad',
			'Mac OS X'      => '(Mac OS X+)|(CFNetwork+)',
			'Mac otros'     => 'Macintosh',
			'Android'       => 'Android',
			'BlackBerry'    => 'BlackBerry',
			'Linux'         => 'Linux',
		];
    }

    public function getPlataforma(){
    	foreach($this->plataformas as $plataforma=>$pattern){
			if (preg_match('/(?i)'.$pattern.'/', $this->userAgent)){
				return $this->plataforma=$plataforma;
			}
		}
		return $this->plataforma='otra';
    }

    public function isMovil(){
    	if($this->plataforma =='Android' || $this->plataforma=='iPhone'){
    		return true;
    	}
    	return false;
    }
}
