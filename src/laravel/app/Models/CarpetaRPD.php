<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\{Carbon,Str};
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\User;
class CarpetaRPD extends Model{
    use SoftDeletes;
    protected $table      ="carpetasRPD";
	protected $primaryKey ='idCarpetaRPD';
	protected $fillable   =[
		'idRPD'
		,'idUsuario'
		,'carpeta'
	];

	public function usuario(){
		return $this->belongsTo(User::class,'idUsuario','idUsuario');
	}

	public function getCarpetaTextAttribute(){
		return "{$this->carpeta} {$this->fechaRegistroFormat} {$this->usuario->nombreCompleto}";
	}
	public function getFechaRegistroFormatAttribute(){
		return Carbon::parse($this->created_at)->format('d-m-Y');
	}

	public  static function storeCarpeta(Request $request, RPD $RPD){
		return self::create([
			'idRPD'      =>$RPD->idRPD
			,'idUsuario' =>auth()->user()->idUsuario
			,'carpeta'   =>$request->carpeta
		]);
	}

	public  static function almacenarArchivos($file,$RPD){
        return Storage::disk('public')->putFileAs("{$RPD->setStorage()->storage}/carpeta/",$file,$file->getClientOriginalName());
    }
}
