<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Vulnerabilidad extends Model{
    use SoftDeletes;
	protected $table      ="vulnerabilidades";
	protected $primaryKey ='idVulnerabilidad';
	protected $fillable   =[
		'nombreVulnerabilidad'
	];

	public function getNombreAttribute(){
    	return $this->nombreVulnerabilidad;
    }

    public function getIdAttribute(){
    	return $this->idVulnerabilidad;
    }
}
