<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Parentesco extends Model{
    use SoftDeletes;

	protected $table      ="parentescos";
	protected $primaryKey ='idParentesco';
	protected $fillable   =[
		'nombreParentesco'
	];    

	public function getNombreAttribute(){
    	return $this->nombreParentesco;
    }

    public function getIdAttribute(){
    	return $this->idParentesco;
    }
}
