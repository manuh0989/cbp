<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class HipotesisDesaparicion extends Model{
    use SoftDeletes;
    
    protected $table      ="hipotesisDesapariciones";
	protected $primaryKey ='idHipotesisDesaparicion';
	protected $fillable   =[
		'nombreHipotesisDesaparicion'
	];

	public function getNombreAttribute(){
    	return $this->nombreHipotesisDesaparicion;
    }

    public function getIdAttribute(){
    	return $this->idHipotesisDesaparicion;
    }
}
