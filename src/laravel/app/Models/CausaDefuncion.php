<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class CausaDefuncion extends Model{
    use SoftDeletes;
    protected $table      ="causaDefuncion";
	protected $primaryKey ='idCausaDefuncion';
	protected $fillable   =[
		'nombreCausaDefuncion'
	];

    public function getNombreAttribute(){
    	return $this->nombreCausaDefuncion;
    }

    public function getIdAttribute(){
    	return $this->idCausaDefuncion;
    }
}
