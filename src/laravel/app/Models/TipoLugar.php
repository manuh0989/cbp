<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TipoLugar extends Model{
    use SoftDeletes;
    protected $table      ="tipoLugar";
	protected $primaryKey ='idTipoLugar';
	protected $fillable   =[
		'nombreTipoLugar'
	];

	
	public function getNombreAttribute(){
    	return $this->nombreTipoLugar;
    }

    public function getIdAttribute(){
    	return $this->idTipoLugar;
    }
}
