<?php

namespace App\Models;

use App\Models\FuenteReporte;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Carbon;
use DB;
class FolioCBP extends Model{

    use SoftDeletes;
	protected $table      ="foliosCBP";
	protected $primaryKey ='idFolioCBP';
	protected $fillable   =[
		'idFuente'
		,'consecutivo'
		,'anio'
		,'prefijo'
	];

	protected $folioCBP;
	protected $anioActual;
	protected $anioAnterior;
	protected $consecutivo_;
	protected $idFuente;

	public function __construct($idFuente=1){
		$this->anioActual   =Carbon::now()->year;
		//$this->anioActual =2021;
		$this->anioAnterior =($this->anioActual-1);
		if($idFuente==FuenteReporte::REPORTE){
			$idFuente=FuenteReporte::NOTICIA;
		}
		$this->idFuente     =$idFuente;
	}

	public function generarFolioCBP(){
		$this->validaAnio();
	
		self::updateOrCreate([
			'anio'      => $this->anioActual
			,'idFuente' => $this->idFuente
		],[
			'idFuente'    => $this->idFuente
			,'anio'         => $this->anioActual
			,'prefijo'     => $this->getPrefijo()
			,'consecutivo' => $this->getConsecutivo()
		]);

		return $this->getFolioCBP();
	}

	public function getPrefijo(){
		if($this->idFuente==FuenteReporte::COLABORADOR){
			return 'CBP-COL';
		}
		return 'CBP';
	}

	public function getConsecutivo(){
		$this->consecutivo_=optional(
			self::select(['consecutivo'])
			->where('anio',$this->anioActual)
			->where('idFuente',$this->idFuente)
			->first()
		)
			->consecutivo;

		if(is_null($this->consecutivo_)){
			$this->inicializarConsecutivo()->incrementarConsecutivo();
		}else{
			$this->incrementarConsecutivo();
		}

		return $this->consecutivo_;
	}

	protected function validaAnio(){
		if($this->getFolioAnioAnterior()){
			$this->trashFolio();
			return $this;
		}
		return $this;
	}

	protected function getFolioAnioAnterior(){
		return $this->folioCBP= DB::table('foliosCBP')
		->select(['idFolioCBP','anio'])
		->where('anio',$this->anioAnterior)
		->where('idFuente',$this->idFuente)
		->whereNull('deleted_at')
		->first();
	}

	protected function trashFolio(){

		DB::table('foliosCBP')
		->where('idFolioCBP',$this->folioCBP->idFolioCBP)
		->where('idFuente',$this->idFuente)
		->update([
			'deleted_at'=>Carbon::now()
		]);
		
		return $this;
	}

	protected function inicializarConsecutivo(){
		$this->consecutivo_="0";
		return $this;
	}

	protected function incrementarConsecutivo(){

		$this->consecutivo_++;
		$this->consecutivo_= str_pad($this->consecutivo_, 5, "0", STR_PAD_LEFT);
		return $this;
	}

	public  function getFolioCBP(){
		$folio= self::select(['prefijo','consecutivo','anio'])
		->where('anio',$this->anioActual) 
		->where('idFuente',$this->idFuente) 
		->whereNull('deleted_at')
		->get()
		->first();
		
		if($folio){
			$folio=$folio->getAttributes();
		}else{
			return $this->generarFolioCBP();
		}
		return "{$folio['prefijo']}/{$folio['consecutivo']}/{$folio['anio']}";
	}
}
