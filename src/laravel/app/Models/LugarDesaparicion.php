<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class LugarDesaparicion extends Model{
    use SoftDeletes;
    protected $table      ="lugaresDesaparicion";
	protected $primaryKey ='idLugarDesaparicion';
	protected $fillable   =[
		'nombreLugarDesaparicion'
	];


	public function getNombreAttribute(){
    	return $this->nombreLugarDesaparicion;
    }

    public function getIdAttribute(){
    	return $this->idLugarDesaparicion;
    }
}
