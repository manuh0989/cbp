<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
class CanalizacionRPD extends Model{
	use SoftDeletes;
    protected $table      ="canalizacionesRPD";
	protected $primaryKey ='idCanalizacionRPD';
	protected $fillable   =[
		'idRPD'
		,'solicitudApoyo'
		,'busquedaSirilo'
		,'aperturaCarpeta'
		,'planBusqueda'
		,'entrevistaFamiliares'
		,'AMPM'
	];
    
    public function getSolicitudApoyoTextAttribute(){
    	return ($this->solicitudApoyo ?? false) ? 'SI' : 'NO';
    }
    public function getBusquedaSiriloTextAttribute(){
    	return ($this->busquedaSirilo ?? false) ? 'SI' : 'NO';
    }
    public function getAperturaCarpetaTextAttribute(){
    	return ($this->aperturaCarpeta ?? false) ? 'SI' : 'NO';
    }
    public function getPlanBusquedaTextAttribute(){
    	return ($this->planBusqueda ?? false) ? 'SI' : 'NO';
    }
    public function getEntrevistaFamiliaresTextAttribute(){
    	return ($this->entrevistaFamiliares ?? false) ? 'SI' : 'NO';
    }
    public function getAMPMTextAttribute(){
    	return ($this->AMPM ?? false) ? 'SI' : 'NO';
    }


    public static function storeCanalizacion(Request $request,$RPD){
    	return self::updateOrCreate([
    		'idRPD'=>$RPD->idRPD
    	],[
			'idRPD'                 =>$RPD->idRPD
			,'solicitudApoyo'       =>$request->solicitudApoyo ?? false
			,'busquedaSirilo'       =>$request->busquedaSirilo ?? false
			,'aperturaCarpeta'      =>$request->aperturaCarpeta ?? false
			,'planBusqueda'         =>$request->planBusqueda ?? false
			,'entrevistaFamiliares' =>$request->entrevistaFamiliares ?? false
			,'AMPM'                 =>$request->AMPM ?? false
    	]);
    }
}
