<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Nacionalidad extends Model{
    use SoftDeletes;
	protected $table      ="nacionalidades";
	protected $primaryKey ='idNacionalidad';
	protected $fillable   =[
	'nombreNacionalidad'
	];

	public function getNombreAttribute(){
    	return $this->nombreNacionalidad;
    }

    public function getIdAttribute(){
    	return $this->idNacionalidad;
    }
}
