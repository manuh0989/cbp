<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class OrigenNoticia extends Model{
    use SoftDeletes;
	protected $table      ="origenNoticias";
	protected $primaryKey ='idOrigenNoticia';
	protected $fillable   =[
		'nombreOrigenNoticia'
	];

	public function getNombreAttribute(){
    	return $this->nombreOrigenNoticia;
    }

    public function getIdAttribute(){
    	return $this->idOrigenNoticia;
    }
}
