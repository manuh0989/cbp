<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model{
	public $timestamps    = false;
	protected $table      ='municipios';
	protected $primaryKey ='id';


	public static function getAllMunicipios(){
		return self::select(['id as idMunicipio','nombre as nombreMunicipio'])->get();
	}

	public  static function getMunicipioRPD($idEstado){
		return Municipio::select(['id as idMunicipio','nombre as nombreMunicipio'])
    		->where('estado_id',$idEstado)
    		->orderBy('nombre')
    		->get();
	}
}
