<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ReportanteRPD extends Model{
    use SoftDeletes;

    protected $table      ="reportantesRPD";
	protected $primaryKey ='idReportante';
	protected $fillable   =[
		'idRPD'
		,'idPersonaDesaparecida'
		,'idParentesco'
		,'nombreCompletoReportante'
		,'edadReportante'
		,'telefonoReportante'
		,'telefono2Reportante'
		,'telefono3Reportante'
		,'correoReportante'
		,'redSocialReportante'
		,'redSocial2Reportante'
		,'otroContactoMedio'
		,'RNPDNO'
		,'contactoInicial'
	];

	public static function guardarActualizarReportante($request,$idRPD,$idPersonaDesaparecida){
		return self::updateOrCreate([
			'idRPD'=>$idRPD
		],[
			'idRPD'                     =>$idRPD
			,'idPersonaDesaparecida'    =>$idPersonaDesaparecida
			,'idParentesco'             =>$request->idParentesco
			,'nombreCompletoReportante' =>$request->nombreCompletoReportante
			,'edadReportante'           =>$request->edadReportante??null
			,'telefonoReportante'       =>$request->telefonoReportante
			,'telefono2Reportante'      =>$request->telefono2Reportante
			,'telefono3Reportante'      =>$request->telefono3Reportante??null
			,'correoReportante'         =>$request->correoReportante
			,'redSocialReportante'      =>$request->redSocialReportante
			,'redSocial2Reportante'     =>$request->redSocial2Reportante??null
			,'otroContactoMedio'        =>$request->otroContactoMedio
			,'RNPDNO'                   =>$request->RNPDNO ?? false
			,'contactoInicial'          =>$request->contactoInicial ?? false
		]);
	}
}
