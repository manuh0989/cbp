<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Complexion extends Model{
	use SoftDeletes;

    protected $table      ="complexiones";
	protected $primaryKey ='idComplexion';
	protected $fillable   =[
		'nombreComplexion'
	];

	public function getNombreAttribute(){
    	return $this->nombreComplexion;
    }

    public function getIdAttribute(){
    	return $this->idComplexion;
    }
}