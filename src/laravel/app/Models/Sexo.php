<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Sexo extends Model{
    use SoftDeletes;

	protected $table      ="sexos";
	protected $primaryKey ='idSexo';
	protected $fillable   =[
		'nombreSexo'
	];
	
	public function getNombreAttribute(){
    	return $this->nombreSexo;
    }

    public function getIdAttribute(){
    	return $this->idSexo;
    }
}
