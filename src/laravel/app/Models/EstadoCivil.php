<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class EstadoCivil extends Model{
	use SoftDeletes;

	protected $table      ="estadosCivil";
	protected $primaryKey ='idEstadoCivil';
	protected $fillable   =[
		'nombreEstadoCivil'
	];

    public function getNombreAttribute(){
    	return $this->nombreEstadoCivil;
    }

    public function getIdAttribute(){
    	return $this->idEstadoCivil;
    }
}
