<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class MotivoReferido extends Model{
	use SoftDeletes;
    protected $table      ="motivosReferido";
	protected $primaryKey ='idMotivoReferido';
	protected $fillable   =[
		'nombreMotivoReferido'
	];

	public function getNombreAttribute(){
		return $this->nombreMotivoReferido;
	}

    public function getIdAttribute(){
    	return $this->idMotivoReferido;
    }
}
