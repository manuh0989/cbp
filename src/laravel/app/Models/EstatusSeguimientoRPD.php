<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class EstatusSeguimientoRPD extends Model{
    use SoftDeletes;
    protected $table      ="estatusSeguimientoRPD";
	protected $primaryKey ='idEstatusSeguimientoRPD';
	protected $fillable   =[
		'nombreEstatusSeguimiento'
	];
}
