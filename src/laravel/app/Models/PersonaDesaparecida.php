<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
class PersonaDesaparecida extends Model{
    use SoftDeletes;
    protected $table      ="personasDesaparecidas";
	protected $primaryKey ='idPersonaDesaparecida';
	protected $fillable   =[
		'idRPD'
		,'idOrigenNoticia'
		,'idSexo'
		,'idEscolaridad'
		,'idEstadocivil'
		,'idVulnerabilidad'
		,'idNacionalidad'
		,'nombrePD'
		,'primerApellidoPD'
		,'segundoApellidoPD'
		,'fechaNacimientoPD'
		,'edadPD'
		,'lugarNacimientoPD'
		,'CURPPD'
		,'RFCPD'
		,'telefonoPD'
		,'redSocialPD'
	];

	public function getNombreCompletoPDAttribute(){
		return "{$this->nombrePD} {$this->primerApellidoPD} {$this->segundoApellidoPD}";
	}


	public function getFechaNacimientoPDFormatAttribute(){
		if(is_null($this->fechaNacimientoPD)){
			return null;
		}
		return Carbon::parse($this->fechaNacimientoPD)->format('d-m-Y');
	}

	public static function guardarActualizarPersonaDesaparecida($request,$idRPD){
		
		return self::updateOrCreate([
			'idRPD'=>$idRPD
		],[
			'idRPD'              =>$idRPD
			,'idOrigenNoticia'   =>$request->idOrigenNoticia
			,'idSexo'            =>$request->idSexo
			,'idEscolaridad'     =>$request->idEscolaridad
			,'idEstadocivil'     =>$request->idEstadocivil
			,'idVulnerabilidad'  =>$request->idVulnerabilidad
			,'idNacionalidad'    =>$request->idNacionalidad
			,'nombrePD'          =>$request->nombrePD
			,'primerApellidoPD'  =>$request->primerApellidoPD
			,'segundoApellidoPD' =>$request->segundoApellidoPD
			,'fechaNacimientoPD' =>(is_null($request->fechaNacimientoPD))?null:setFechaIsNull($request->fechaNacimientoPD)
			,'edadPD'            =>$request->edadPD
			,'lugarNacimientoPD' =>$request->lugarNacimientoPD
			,'CURPPD'            =>$request->CURPPD
			,'RFCPD'             =>$request->RFCPD
			,'telefonoPD'        =>$request->telefonoPD
			,'redSocialPD'       =>$request->redSocialPD
		]);
	}
}
