<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\{Carbon,Str};
use Illuminate\Support\Facades\Storage;

use App\Models\{FotografiaRPD,PersonaDesaparecida,DatoGeneralRPD,HechoDesaparicionRPD,ReportanteRPD,MediaFiliacionRPD,FolioCBP,TransaccionRPD,SeguimientoRPD,CanalizacionRPD,CarpetaRPD};
use DB;

class RPD extends Model{
    use SoftDeletes;
	
	protected $table      ="RPD";
	protected $primaryKey ='idRPD';
	protected $fillable   =[
		'idFuente'
		,'folioCBP'
		,'statusLocalizacion'
		,'funcionarioRegistro'
		,'folioFIPEDE'
		,'folioLocatel'
	];

	public $encabezadoRPD=[
        "RPD.folioCBP as 'Folio CBP'"
        ,"RPD.folioFIPEDE as 'Carpeta investigacion'"
        ,"RPD.statusLocalizacion as 'Estatus localizacion'"
        ,"FR.nombreFuente as 'Fuente'"
        ,"RPD.folioLocatel as 'Folio locatel'"
        ,"RPD.created_at as 'Fecha alta'"
        ,"PD.nombrePD as Nombre 'Persona desaparecida'"
        ,"PD.primerApellidoPD as 'Apellido paterno persina desaparecida'"
        ,"PD.segundoApellidoPD as 'Apellido materno persona desaparecida'"
        ,"ON.nombreOrigenNoticia as 'Origen noticia'"
        ,"PD.fechaNacimientoPD as 'Fecha nacimiento'"
        ,"PD.edadPD as 'Edad persona desaparecida'"
        ,"nac.nombreNacionalidad as 'Nacionalidad'"
        ,"PD.lugarNacimientoPD as 'lugar de nacimiento persona desaparecida'"
        ,"PD.CURPPD as CURP"
        ,"PD.RFCPD AS RFC"
        ,"sexo.nombreSexo AS sexo"
        ,"ESC.nombreEscolaridad as Escolaridad"
        ,"EC.nombreEstadoCivil as 'Estado civil'"
        ,"VUL.nombreVulnerabilidad as 'Vulnerabilidad'"
        ,"estado.nombre as 'Estado'"
        ,"municipios.nombre as Municipio"
        ,"localidades.nombre as Localidad"
        ,"DG.calleNumeroDesaparicion as 'Calle y numero' "
        ,"CD.nombreCircunstanciaDesaparicion as 'Cirscuntancia desaparicion'"
        ,"HD.nombreHipotesisDesaparicion as Hipotesis"
        ,"LD.nombreLugarDesaparicion as 'Lugar de desaparición'"
        ,"hechos.descripcionHechosDesaparicion as 'Hechos desaparición'"
        ,"reportante.nombreCompletoReportante as Nombre reportante"
        ,"parentescos.nombreParentesco as Parentesco"
        
        ,"reportante.telefonoReportante as 'teléfono reportante'"
        ,"reportante.telefono2Reportante as 'teléfono 2 reportante'"
        
        ,"reportante.correoReportante as 'correo reportante'"
        ,"reportante.redSocialReportante as 'red social reportante'"
        
        ,"reportante.otroContactoMedio as 'otro medio de contacto'"
        ,"DG.fechaDesaparicion as 'Fecha desaparición'"
        ,"DG.horaDesaparicion  as 'Hora Desaparición'"
        ,"RPD.funcionarioRegistro as 'funcionario registro'"
        ,"reportante.RNPDNO as 'Canalización de datos de contacto'"
        ,"reportante.contactoInicial as 'No fue posible el contacto inicial con reportante'"
        ,"sexoMedia.nombreSexo as 'Genero reportado'"
        ,"complexiones.nombreComplexion as 'Complexión'"
        ,"media.tez as 'Tez'"
        ,"media.frente as 'Frente'"
        ,"media.boca as 'Boca'"
        ,"media.menton as 'Menton'"
        ,"media.tipoCabello as 'Tipo cabello'"
        ,"media.estatura as 'Estatura'"
        ,"media.nariz as 'Nariz'"
        ,"media.cejas as 'Cejas'"
        ,"media.colorOjos as 'Color ojos'"
        ,"media.longitudCabello as 'Longitud cabello'"
        ,"media.cara as 'Cara'"
        ,"media.labios as 'Labios'"
        ,"RPD.statusLocalizacion as 'Estatus de localización'"
        ,"condicion.fechaLocalizacion as 'Fecha localización'"
        ,"estadoCondicion.nombre as 'Entidad'"
        ,"municipioCondicion.nombre as 'Municipio'"
        
        ,"loc.nombreEstatusLocalizacion as 'Estatus de localización'"
        ,"motivosReferido.nombreMotivoReferido as 'Motivo referido'"
        ,"ep.nombreEstadoPersona as 'Estado de la persona'"
        ,"tipoLugar.nombreTipoLugar as 'Tipo de lugar'"
        ,"constancia.nombreConstancia as 'Constancia'"
        ,"delito.nombreDelito as 'Delito cometido'"
        ,"cd.nombreCausaDefuncion as 'Causa de defunción'"
        ,"condicion.observacion as 'Observación'"
        
    ];

    public $encabezadoExcel=[
    	"Folio CBP"
        ,"Carpeta investigacion"
        ,"Estatus localizacion"
        ,"Fuente"
        ,"Folio locatel"
        ,"Fecha alta"
        ,"Persona desaparecida"
        ,"Apellido paterno persina desaparecida"
        ,"Apellido materno persona desaparecida"
        ,"Origen noticia"
        ,"Fecha nacimiento"
        ,"Edad persona desaparecida"
        ,"Nacionalidad"
        ,"lugar de nacimiento persona desaparecida'"
        ,"CURP"
        ,"RFC"
        ,"sexo"
        ,"Escolaridad"
        ,"Estado civil"
        ,"Vulnerabilidad"
        ,"Estado"
        ,"Municipio"
        ,"Localidad"
        ,"Calle y numero"
        ,"Cirscuntancia desaparicion"
        ,"Hipotesis"
        ,"Lugar de desaparición"
        ,"Hechos desaparición"
        ,"Nombre reportante"
        ,"Parentesco"
        
        ,"teléfono reportante"
        ,"teléfono 2 reportante"
        
        ,"correo reportante"
        ,"red social reportante"
        
        ,"otro medio de contacto"
        ,"Fecha desaparición"
        ,"Hora Desaparición"
        ,"funcionario registro"
        ,"Canalización de datos de contacto"
        ,"No fue posible el contacto inicial con reportante"
        ,"Genero reportado"
        ,"Complexión"
        ,"Tez"
        ,"Frente"
        ,"Boca"
        ,"Menton"
        ,"Tipo cabello"
        ,"Estatura'"
        ,"Nariz"
        ,"Cejas"
        ,"Color ojos"
        ,"Longitud cabello"
        ,"Cara"
        ,"Labios"
        ,"Estatus de localización"
        ,"Fecha localización"
        ,"Entidad"
        ,"Municipio"

        ,"Motivo referido"
        ,"Estado de la persona"
        ,"Tipo de lugar"
        ,"Constancia"
        ,"Delito cometido"
        ,"Causa de defunción"
        ,"Observación"
    ];

	public $storage;
	public $nombreImagen;

	public  function setNombreImagen(){
		$this->nombreImagen=Str::random(20).'.jpg';
		return $this;
	}

	public function getNombreImagenAttribute(){
		return $this->nombreImagen;
	}

	public  function setStorage(){
		$this->storage="RPD/{$this->idRPD}";
		return $this;
	}

	public function getStorageAttribute(){
		return $this->storage;
	}

	public function fotografiaRPD(){
		return $this->belongsTo(FotografiaRPD::class,'idRPD','idRPD');
	}

	public function personaDesaparecida(){
		return $this->belongsTo(PersonaDesaparecida::class,'idRPD','idRPD');
	}

	public function datoGeneralRPD(){
		return $this->belongsTo(DatoGeneralRPD::class,'idRPD','idRPD');
	}

	public function hechoDesaparicionRPD(){
		return $this->belongsTo(HechoDesaparicionRPD::class,'idRPD','idRPD');
	}

	public function reportanteRPD(){
		return $this->belongsTo(ReportanteRPD::class,'idRPD','idRPD');
	}

	public function mediaFiliacionRPD(){
		return $this->belongsTo(MediaFiliacionRPD::class,'idRPD','idRPD');
	}

	public function seguimientosRPD(){
		return $this->hasMany(SeguimientoRPD::class,'idRPD','idRPD');
	}

    public function fuenteRPD(){
        return $this->belongsTo(FuenteReporte::class,'idFuente','idFuente');   
    }

	public function canalizacionRPD(){
		return $this->belongsTo(CanalizacionRPD::class,'idRPD','idRPD')->withDefault([
			'solicitudApoyo'        =>false
			,'busquedaSirilo'       =>false
			,'aperturaCarpeta'      =>false
			,'planBusqueda'         =>false
			,'entrevistaFamiliares' =>false
			,'AMPM'                 =>false
		]);
	}

    public function getRutaCarpetaAttribute(){
        return "RPD/{$this->idRPD}/carpeta";
    }

	public function condicionLocalizacion(){
		return $this->belongsTo(CondicionLocalizacion::class,'idRPD','idRPD');
	}

	public function carpetasRPD(){
		return $this->hasMany(CarpetaRPD::class,'idRPD','idRPD');
	}

    public function getAllFilesCarpetaAttribute(){
        return Storage::disk('public')->allFiles($this->rutaCarpeta);
    }

    public function getNameFileCarpeta($archivo){
        return explode("/", $archivo)[3];
    }

	public function getRutaFotografiaRPDAttribute(){
		return asset("storage/RPD/{$this->idRPD}/{$this->fotografiaRPD->foto}");
	}

	public function getFechaActualizacionFormatAttribute(){
		return Carbon::parse($this->updated_at)->format('d-m-Y');
	}

	public function getFechaRegistroFormatAttribute(){
		return Carbon::parse($this->created_at)->format('d-m-Y');
	}

	public function getListaRPD($request){

		return self::with('personaDesaparecida','datoGeneralRPD','mediaFiliacionRPD','fotografiaRPD')
		->leftjoin('datosGeneralesRPD as DG','DG.idRPD','=','RPD.idRPD')
		->leftjoin('personasDesaparecidas as PD','PD.idRPD','=','RPD.idRPD')
		->filtros($request)
        ->orderBy('RPD.idRPD','DESC')
		->paginate(15)
		;
	}

	public function scopeFiltros($query,$request){
		
		if( ($request->buscar??null) && ($request->opcionBusqueda ?? null)  =='nombrePersona'){
			$request->nombrePersona       =$request->buscar;
			$request->concatNombrePersona ="CONCAT(nombrePD,' ', primerApellidoPD,' ', COALESCE(segundoApellidoPD,'') )";
			$request->buscar              ='';
		}

		if($request){
			($request->buscar??null)
				?$query->where($request->opcionBusqueda,'like',"%{$request->buscar}%")
				:$query;
			($request->concatNombrePersona ?? null)
				?$query->where(DB::raw($request->concatNombrePersona),'like',"%$request->nombrePersona%")
				:$query;
			($request->chkFecha ?? null =='on')
				?$query->whereBetween('DG.fechaDesaparicion',[
						Carbon::parse($request->fechaDesaparicionIni)->format('Y-m-d')
						,Carbon::parse($request->fechaDesaparicionFin)->format('Y-m-d')
				])
				:$query;
		}
	}

	public function deleteRPD(){
		$this->delete();
        optional($this->fotografiaRPD)->delete();
        optional($this->datoGeneralRPD)->delete();
        optional($this->hechoDesaparicionRPD)->delete();
        optional($this->reportanteRPD)->delete();
        optional($this->mediaFiliacionRPD)->delete();
        $this->seguimientosRPD()->delete();
        $this->canalizacionRPD->delete();
        
        return $this;
	}    

    public function validaMediaFiliacion(){
        $mediaFiliacionRPD   =$this->mediaFiliacionRPD;
        $personaDesaparecida =$this->personaDesaparecida;

    	return (
              $this->fotografiaRPD                   == NULL
              || $personaDesaparecida->edadPD        == NULL
              || $personaDesaparecida->idSexo        == NULL
              || $mediaFiliacionRPD->estatura        == NULL
              || $mediaFiliacionRPD->idComplexion    == NULL
              || $mediaFiliacionRPD->tez             == NULL
              || $mediaFiliacionRPD->tipoCabello     == NULL
              || $mediaFiliacionRPD->longitudCabello == NULL
              
    		)? false : true;
    }

    public function disabledVolante(){
    	return ($this->validaMediaFiliacion() == true)? '' : 'disabled';
    }

    public function getLocalizadoAttribute(){
    	return $this->statusLocalizacion === 'LOCALIZADO';
    }

    public function getNoLocalizadoAttribute(){
    	return $this->statusLocalizacion === 'DESAPARECIDO';
    }
    
    public function validaSeguimiento(Request $request){
		return (SeguimientoRPD::validaSeguimiento($this,$request)->count() == 1)?true:false;
	}

	public  function RPDExcelQuery($request){
		return self::select($this->encabezadoRPD)
        ->leftJoin('fuenteReporte as FR','FR.idFuente','=','RPD.idFuente')
        ->leftJoin('datosGeneralesRPD as DG','DG.idRPD','=','RPD.idRPD')
        ->leftJoin('personasDesaparecidas as PD','PD.idRPD','=','RPD.idRPD')
        ->leftJoin('sexos as sexo','sexo.idSexo','=','PD.idSexo')
        ->leftJoin('escolaridades as ESC','ESC.idEscolaridad','=','PD.idEscolaridad')
        ->leftJoin('origenNoticias as ON','ON.idOrigenNoticia','=','PD.idOrigenNoticia')
        ->leftJoin('nacionalidades as nac','nac.idNacionalidad','=','PD.idNacionalidad')
        ->leftJoin('estadosCivil as EC','EC.idEstadoCivil','=','PD.idEstadoCivil')
        ->leftJoin('vulnerabilidades as VUL','VUL.idVulnerabilidad','=','PD.idVulnerabilidad')
        ->leftJoin('estados as estado','estado.id','=','DG.idEstado')
        ->leftJoin('municipios as municipios','municipios.id','=','DG.idMunicipio')
        ->leftJoin('localidades as localidades','localidades.id','=','DG.idLocalidad')
        ->leftJoin('hechosDesaparicionesRPD as hechos','hechos.idRPD','=','RPD.idRPD')
        ->leftJoin('circunstanciasDesapariciones as CD','CD.idCircunstanciaDesaparicion','=','hechos.idCircunstanciaDesaparicion')
        ->leftJoin('hipotesisDesapariciones as HD','HD.idHipotesisDesaparicion','=','hechos.idHipotesisDesaparicion')
        ->leftJoin('lugaresDesaparicion as LD','LD.idLugarDesaparicion','=','hechos.idLugarDesaparicion')
        ->leftJoin('reportantesRPD as reportante','reportante.idRPD','=','RPD.idRPD')
        ->leftJoin('mediaFiliacionesRPD as media','media.idRPD','=','RPD.idRPD')
        ->leftJoin('sexos as sexoMedia','sexoMedia.idSexo','=','media.idGeneroReportado')
        ->leftJoin('complexiones as complexiones','complexiones.idComplexion','=','media.idComplexion')
        ->leftJoin('condicionLocalizacion as condicion','condicion.idRPD','=','RPD.idRPD')
        
        ->leftJoin('estados as estadoCondicion','estadoCondicion.id','=','condicion.idEstado')
        ->leftJoin('municipios as municipioCondicion','municipioCondicion.id','=','condicion.idMunicipio')
        
        ->leftJoin('tipoLugar as tipoLugar','tipoLugar.idTipoLugar','=','condicion.idTipoLugar')
        ->leftJoin('motivosReferido as motivosReferido','motivosReferido.idMotivoReferido','=','condicion.idMotivoReferido')
        ->leftJoin('parentescos as parentescos','parentescos.idParentesco','=','reportante.idParentesco')
        ->orderBy('RPD.folioCBP','desc')


        ->leftJoin('estatusLocalizacion as loc', 'loc.idEstatusLocalizacion','=','condicion.idEstatusLocalizacion')
        ->leftJoin('estadoPersona as ep', 'ep.idEstadoPersona','=','condicion.idEstadoPersona')
        ->leftJoin('constancia as constancia', 'constancia.idConstancia','=','condicion.idConstancia')
        ->leftJoin('delito as delito', 'delito.idDelito','=','condicion.idDelito')
        ->leftJoin('causaDefuncion as cd', 'cd.idCausaDefuncion','=','condicion.idCausaDefuncion')
        


        ->filtros($request);
	}

    public function getIdRPD(){
        return $this->idRPD;
    }

    public static  function guardarActualizarRPD($request,$idRPD=NULL){
        $rpd=self::updateOrCreate([
            'idRPD'=>$idRPD
        ],[
            'idFuente'             =>$request->idFuente
            ,'folioCBP'            =>$request->folioCBP//(new FolioCBP())->getFolioCBP()
            ,'statusLocalizacion'  =>$request->statusLocalizacion
            ,'funcionarioRegistro' =>$request->funcionarioRegistro
            ,'folioFIPEDE'         =>$request->folioFIPEDE
            ,'folioLocatel'        =>$request->folioLocatel
        ]);
        return $rpd;
    }
}
