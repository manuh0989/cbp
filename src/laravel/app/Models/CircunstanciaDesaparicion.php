<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class CircunstanciaDesaparicion extends Model{
    use SoftDeletes;
    protected $table      ="circunstanciasDesapariciones";
	protected $primaryKey ='idCircunstanciaDesaparicion';
	protected $fillable   =[
		'nombreCircunstanciaDesaparicion'
	];

	public function getNombreAttribute(){
    	return $this->nombreCircunstanciaDesaparicion;
    }

    public function getIdAttribute(){
    	return $this->idCircunstanciaDesaparicion;
    }
}
