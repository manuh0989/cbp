<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class FuenteReporte extends Model{
    use SoftDeletes;
    const COLABORADOR =3;
    const REPORTE     =2;
    const NOTICIA     =1;
    protected $table="fuenteReporte";
    protected $primaryKey='idFuente';
    protected $fillable=[
    	'nombreFuente'
    ];

    public function getNombreAttribute(){
    	return $this->nombreFuente;
    }

    public function getIdAttribute(){
    	return $this->idFuente;
    }
}
