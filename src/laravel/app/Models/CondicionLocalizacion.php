<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\{Carbon,Str};
use App\Models\{EstatusLocalizacion,MotivoReferido,EstadoPersona,TipoLugar,Constancia,Delito,CausaDefuncion};
class CondicionLocalizacion extends Model{
	use SoftDeletes;

    protected $table      ="condicionLocalizacion";
	protected $primaryKey ='idCondicionLocalizacion';
	protected $fillable   =[
		'idRPD'
		,'idEstatusLocalizacion'	
		,'idTipoLugar'	
		,'idMotivoReferido'
		,'idEstadoPersona'
		,'idConstancia'
		,'idDelito'
		,'idCausaDefuncion'
		,'fechaLocalizacion'
		,'idEstado'
		,'idMunicipio'
		,'idLocalidad'
		,'calleLugar'
		,'delitoCometido'
		,'bajaReporte'
		,'observacion'

	];

	public function estatusLocalizacion(){
		return $this->belongsTo(EstatusLocalizacion::class,'idEstatusLocalizacion','idEstatusLocalizacion');
	}

	public function motivoReferido(){
		return $this->belongsTo(MotivoReferido::class,'idMotivoReferido','idMotivoReferido');
	}

	public function estadoPersona(){
		return $this->belongsTo(EstadoPersona::class,'idEstadoPersona','idEstadoPersona');
	}

	public function tipoLugar(){
		return $this->belongsTo(TipoLugar::class,'idTipoLugar','idTipoLugar');
	}

	public function constancia(){
		return $this->belongsTo(Constancia::class,'idConstancia','idConstancia');
	}

	public function delito(){
		return $this->belongsTo(Delito::class,'idDelito','idDelito');
	}

	public function causaDefuncion(){
		return $this->belongsTo(CausaDefuncion::class,'idCausaDefuncion','idCausaDefuncion');
	}

	public function getFechaLocalizacionFormatAttribute(){
		if(is_null($this->fechaLocalizacion)){
			return null;
		}
		return Carbon::parse($this->fechaLocalizacion)->format('d-m-Y');
	}

	public static  function guardarActualizarCondicionLocalizacion($request,$idRPD){
		$fechaLocalizacion=null;
		if(is_null($request->fechaLocalizacion)){
			$fechaLocalizacion=null;
		}else{
			$fechaLocalizacion=setFechaIsNull($request->fechaLocalizacion);
		}
		return self::updateOrCreate([
			'idRPD'=>$idRPD
		],[
			'idRPD'                  =>$idRPD
			,'idEstatusLocalizacion' =>$request->idEstatusLocalizacion
			,'idTipoLugar'           =>$request->idTipoLugar??null
			,'idMotivoReferido'      =>$request->idMotivoReferido??null
			,'fechaLocalizacion'     =>$fechaLocalizacion
			,'idEstado'              =>$request->idEstado??null
			,'idMunicipio'           =>$request->idMunicipio??null
			,'idLocalidad'           =>$request->idLocalidad??null
			,'calleLugar'            =>$request->calleLugar??null
			
			,'delitoCometido'        =>$request->delitoCometido??null
			,'idEstadoPersona'       =>$request->idEstadoPersona??null
			,'idConstancia'          =>$request->idConstancia??null
			,'idDelito'              =>$request->idDelito??null
			,'idCausaDefuncion'      =>$request->idCausaDefuncion??null
			,'observacion'           =>$request->observacion
			//,'bajaReporte'           =>$request->bajaReporte ?? false
		]);
	}
   
}
