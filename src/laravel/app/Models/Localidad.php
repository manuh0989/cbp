<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Localidad extends Model{
	
    public $timestamps    = false;
	protected $table      ='localidades';
	protected $primaryKey ='id';

	public static function getLocalidadRPD($idMunicipio){
		return self::select(['id as idLocalidad','nombre as nombreLocalidad'])
            ->where('municipio_id',$idMunicipio)
            ->orderBy('nombre')
            ->get();
	}
}
