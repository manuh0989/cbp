<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class EstatusLocalizacion extends Model{
    use SoftDeletes;
    protected $table      ="estatusLocalizacion";
	protected $primaryKey ='idEstatusLocalizacion';
	protected $fillable   =[
		'nombreEstatusLocalizacion'
	];

	public function getNombreAttribute(){
    	return $this->nombreEstatusLocalizacion;
    }

    public function getIdAttribute(){
    	return $this->idEstatusLocalizacion;
    }
}
