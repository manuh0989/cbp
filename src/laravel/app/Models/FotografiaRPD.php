<?php

namespace App\Models;


use Intervention\Image\Facades\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\{Carbon,Str};

class FotografiaRPD extends Model{
    use SoftDeletes;

    protected $table="fotografiasRPD";
    protected $primaryKey='idFotografia';
    protected $fillable=[
    	'idRPD'
    	,'foto'
    ];

    protected $fotografiaRPD;
    protected $storage;



    public static  function storeFotoRPD($request,$RPD){
        $nombreImagen=Str::random(20).".jpg";
        
        foreach($request->fotos ?? [] as $key =>$file){
            self::updateOrCreate([
                'idRPD' =>$RPD->idRPD
            ],[
                'idRPD' =>$RPD->idRPD
                ,'foto' =>$nombreImagen
            ])
            ->deleteFoto($RPD)
            ->almacenarFoto($file,$nombreImagen,$RPD);
            ;
        }
    }

    protected function almacenarFoto($file,$nombreImagen, $RPD){
        return Storage::disk('public')
                ->put("{$RPD->setStorage()->storage}/{$nombreImagen}",$this->encodeImage($file)->stream());
    }

    protected function deleteFoto($RPD){
        $dir   =$RPD->setStorage()->storage;
        $files =Storage::allFiles($dir);
        Storage::disk('public')->delete($files);
        return $this;
    }

    protected function encodeImage($file){
        return Image::make($file)->encode('jpg',75)
        ->resize(350,450,function($contst){
            $contst->upsize();
        });
    }
}
