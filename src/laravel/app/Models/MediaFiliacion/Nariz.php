<?php

namespace App\Models\MediaFiliacion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Nariz extends Model
{
     use SoftDeletes;
    protected $table      ="tiposNariz";
	protected $primaryKey ='idTipoNariz';
	protected $fillable   =[
		'nombreTipoNariz'
	];

	public function getNombreAttribute(){
    	return $this->nombreTipoNariz;
    }

    public function getIdAttribute(){
    	return $this->idTipoNariz;
    }
}
