<?php

namespace App\Models\MediaFiliacion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Cejas extends Model{
     use SoftDeletes;
    protected $table      ="tiposCejas";
	protected $primaryKey ='idTipoCejas';
	protected $fillable   =[
		'nombreTipoCejas'
	];

	public function getNombreAttribute(){
    	return $this->nombreTipoCejas;
    }

    public function getIdAttribute(){
    	return $this->idTipoCejas;
    }
}
