<?php

namespace App\Models\MediaFiliacion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Labios extends Model
{
     use SoftDeletes;
    protected $table      ="tiposLabio";
	protected $primaryKey ='idTipoLabio';
	protected $fillable   =[
		'nombreTipoLabio'
	];

	public function getNombreAttribute(){
    	return $this->nombreTipoLabio;
    }

    public function getIdAttribute(){
    	return $this->idTipoLabio;
    }
}
