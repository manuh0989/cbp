<?php

namespace App\Models\MediaFiliacion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class LongitudOjos extends Model
{
     use SoftDeletes;
    protected $table      ="tiposLongitudOjos";
	protected $primaryKey ='idTipoLongitudOjos';
	protected $fillable   =[
		'nombreLongitudOjos'
	];

	public function getNombreAttribute(){
    	return $this->nombreLongitudOjos;
    }

    public function getIdAttribute(){
    	return $this->idTipoLongitudOjos;
    }
}
