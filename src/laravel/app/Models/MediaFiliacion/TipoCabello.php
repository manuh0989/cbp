<?php

namespace App\Models\MediaFiliacion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TipoCabello extends Model
{
     use SoftDeletes;
    protected $table      ="tiposCabello";
	protected $primaryKey ='idTipoCabello';
	protected $fillable   =[
		'nombreTipoCabello'
	];
	public function getNombreAttribute(){
    	return $this->nombreTipoCabello;
    }

    public function getIdAttribute(){
    	return $this->idTipoCabello;
    }
}
