<?php

namespace App\Models\MediaFiliacion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ColorOjos extends Model
{
     use SoftDeletes;
    protected $table      ="tiposColorOjos";
	protected $primaryKey ='idTipoColorOjos';
	protected $fillable   =[
		'nombreTipoColorOjos'
	];

	public function getNombreAttribute(){
    	return $this->nombreTipoColorOjos;
    }

    public function getIdAttribute(){
    	return $this->idTipoColorOjos;
    }
}
