<?php

namespace App\Models\MediaFiliacion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Boca extends Model{
     use SoftDeletes;
    protected $table      ="tiposBoca";
	protected $primaryKey ='idTipoBoca';
	protected $fillable   =[
		'nombreTipoBoca'
	];

	public function getNombreAttribute(){
    	return $this->nombreTipoBoca;
    }

    public function getIdAttribute(){
    	return $this->idTipoBoca;
    }
}
