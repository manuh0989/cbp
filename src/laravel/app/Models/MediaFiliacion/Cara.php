<?php

namespace App\Models\MediaFiliacion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Cara extends Model{
     use SoftDeletes;
    protected $table      ="tiposCara";
	protected $primaryKey ='idTipoCara';
	protected $fillable   =[
		'nombreTipoCara'
	];

	public function getNombreAttribute(){
    	return $this->nombreTipoCara;
    }

    public function getIdAttribute(){
    	return $this->idTipoCara;
    }
}
