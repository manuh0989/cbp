<?php

namespace App\Models\MediaFiliacion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class LongitudCabello extends Model{

    use SoftDeletes;
    protected $table      ="tiposLongitudCabello";
	protected $primaryKey ='idTipoLongitudCabello';
	protected $fillable   =[
		'nombreTipoLogitudCabello'
	];

	public function getNombreAttribute(){
    	return $this->nombreTipoLogitudCabello;
    }

    public function getIdAttribute(){
    	return $this->idTipoLongitudCabello;
    }
}
