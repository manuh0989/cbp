<?php

namespace App\Models\MediaFiliacion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Tez extends Model
{
     use SoftDeletes;
    protected $table      ="tiposTez";
	protected $primaryKey ='idTipoTez';
	protected $fillable   =[
		'nombreTipoTez'
	];

	public function getNombreAttribute(){
    	return $this->nombreTipoTez;
    }

    public function getIdAttribute(){
    	return $this->idTipoTez;
    }
}
