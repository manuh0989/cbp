<?php

namespace App\Models\MediaFiliacion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Menton extends Model
{
     use SoftDeletes;
    protected $table      ="tiposMenton";
	protected $primaryKey ='idTipoMenton';
	protected $fillable   =[
		'nombreTipoMenton'
	];

	public function getNombreAttribute(){
    	return $this->nombreTipoMenton;
    }

    public function getIdAttribute(){
    	return $this->idTipoMenton;
    }
}
