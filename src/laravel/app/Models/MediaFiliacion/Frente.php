<?php

namespace App\Models\MediaFiliacion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Frente extends Model
{
     use SoftDeletes;
    protected $table      ="tiposFrente";
	protected $primaryKey ='idTipoFrente';
	protected $fillable   =[
		'nombreTipoFrente'
	];

	public function getNombreAttribute(){
    	return $this->nombreTipoFrente;
    }

    public function getIdAttribute(){
    	return $this->idTipoFrente;
    }
}
