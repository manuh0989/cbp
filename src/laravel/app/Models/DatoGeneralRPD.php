<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
class DatoGeneralRPD extends Model{
    use SoftDeletes;
    protected $table      ="datosGeneralesRPD";
	protected $primaryKey ='idDatoGeneralRPD';
	protected $fillable   =[
		'idRPD'
		,'idPersonaDesaparecida'
		,'fechaDesaparicion'
		,'horaDesaparicion'
		,'idEstado'
		,'idMunicipio'
		,'idLocalidad'
		,'calleNumeroDesaparicion'
	];

	public function getFechaDesaparicionFormatAttribute(){
		return (is_null($this->fechaDesaparicion))?null:Carbon::parse($this->fechaDesaparicion)->format('d-m-Y');
	}

	public function getHoraDesaparicionFormatAttribute(){
		return setHoraIsNull($this->horaDesaparicion);
	}

	public static function setFechaDesaparicionTimestamp($fechaDesaparicion,$horaDesaparicion){
		return Carbon::createFromTimestamp(strtotime($fechaDesaparicion.$horaDesaparicion))->format('Y-m-d H:m:s');
	}

	public static function guardarActualizarDatosGenerales($request,$idRPD,$idPersonaDesaparecida){
		return self::updateOrCreate([
				'idRPD'=>$idRPD
			],[
				'idRPD'                    =>$idRPD
	            ,'idPersonaDesaparecida'   =>$idPersonaDesaparecida
	            ,'fechaDesaparicion'       =>setFechaIsNull($request->fechaDesaparicion)
	            ,'horaDesaparicion'        =>setHoraIsNull($request->horaDesaparicion)
	            ,'idEstado'                =>$request->idEstado??null
	            ,'idMunicipio'             =>$request->idMunicipio??null
	            ,'idLocalidad'             =>$request->idLocalidad??null
	            ,'calleNumeroDesaparicion' =>$request->calleNumeroDesaparicion
			]);
	}
}
