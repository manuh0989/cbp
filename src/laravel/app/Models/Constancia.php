<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Constancia extends Model{
	use SoftDeletes;
    protected $table      ="constancia";
	protected $primaryKey ='idConstancia';
	protected $fillable   =[
		'nombreConstancia'
	];

    public function getNombreAttribute(){
    	return $this->nombreConstancia;
    }

    public function getIdAttribute(){
    	return $this->idConstancia;
    }
    //
}
