<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class EstadoPersona extends Model{
    use SoftDeletes;
    protected $table      ="estadoPersona";
	protected $primaryKey ='idEstadoPersona';
	protected $fillable   =[
		'nombreEstadoPersona'
	];

    public function getNombreAttribute(){
    	return $this->nombreEstadoPersona;
    }

    public function getIdAttribute(){
    	return $this->idEstadoPersona;
    }
}
