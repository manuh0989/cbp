<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Delito extends Model{
	use SoftDeletes;
    protected $table      ="delito";
	protected $primaryKey ='idDelito';
	protected $fillable   =[
		'nombreDelito'
	];

    public function getNombreAttribute(){
    	return $this->nombreDelito;
    }

    public function getIdAttribute(){
    	return $this->idDelito;
    }
    
}
