<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Escolaridad extends Model{
	use SoftDeletes;

	protected $table      ="escolaridades";
	protected $primaryKey ='idEscolaridad';
	protected $fillable   =[
		'nombreEscolaridad'
	];

    public function getNombreAttribute(){
    	return $this->nombreEscolaridad;
    }

    public function getIdAttribute(){
    	return $this->idEscolaridad;
    }
}
