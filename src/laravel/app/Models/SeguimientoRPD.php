<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\{Carbon,Str};
use App\Models\{EstatusSeguimientoRPD,RPD};
class SeguimientoRPD extends Model{
    use SoftDeletes;

    protected $table      ="seguimientosRPD";
	protected $primaryKey ='idSeguimientoRPD';
	protected $fillable   =[
		'idRPD'
		,'idEstatusSeguimientoRPD'
		,'numeroContacto'
		,'fechaContactoSeguimiento'
		,'descripcionSeguimiento'
		,'funcionarioSeguimiento'
	];


	public static function storeSeguimiento(Request $request){
		return self::updateOrCreate([
			'idRPD'           =>$request->idRPD
			,'numeroContacto' =>$request->numeroContacto
		]
		,[
			'idRPD'                     =>$request->idRPD
			,'idEstatusSeguimientoRPD'  =>$request->idEstatusSeguimientoRPD
			,'fechaContactoSeguimiento' =>Carbon::parse($request->fechaContactoSeguimiento)->format('Y-m-d')
			,'descripcionSeguimiento'   =>$request->descripcionSeguimiento
			,'numeroContacto'           =>$request->numeroContacto
			,'funcionarioSeguimiento'   =>Str::upper(auth()->user()->nombreCompleto)
		]);
	}

	public function getFechaContactoSeguimientoFormatAttribute(){
		return Carbon::parse($this->fechaContactoSeguimiento)->format('d-m-Y');
	}

	public function estatusSeguimientoRPD(){
		return $this->belongsTo(EstatusSeguimientoRPD::class,'idEstatusSeguimientoRPD','idEstatusSeguimientoRPD');
	}

	public static function validaSeguimiento(RPD $RPD, Request $request){
		return self::where('idRPD',$RPD->idRPD)
				->where('numeroContacto',$request->contacto -1)
				->get();
	}
}
