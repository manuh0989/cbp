<?php

namespace App\Policies;

use App\Models\RPD;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Policies\Traits\TraitPolicy;
class RPDPolicy
{
    use HandlesAuthorization,TraitPolicy;

    /**
     * Determine whether the user can view any r p d s.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the r p d.
     *
     * @param  \App\User  $user
     * @param  \App\RPD  $rPD
     * @return mixed
     */
    public function view(User $user, RPD $rPD)
    {
        //
    }

    /**
     * Determine whether the user can create r p d s.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the r p d.
     *
     * @param  \App\User  $user
     * @param  \App\RPD  $rPD
     * @return mixed
     */
    public function update(User $user, RPD $rPD)
    {
        //
    }

    /**
     * Determine whether the user can delete the r p d.
     *
     * @param  \App\User  $user
     * @param  \App\RPD  $rPD
     * @return mixed
     */
    public function delete(User $user){
        return $this->isAdmin($user);
    }

    /**
     * Determine whether the user can restore the r p d.
     *
     * @param  \App\User  $user
     * @param  \App\RPD  $rPD
     * @return mixed
     */
    public function restore(User $user, RPD $rPD)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the r p d.
     *
     * @param  \App\User  $user
     * @param  \App\RPD  $rPD
     * @return mixed
     */
    public function forceDelete(User $user, RPD $rPD)
    {
        //
    }
}
