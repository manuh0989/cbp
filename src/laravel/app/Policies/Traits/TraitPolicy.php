<?php

namespace App\Policies\Traits;
use App\User;
use Illuminate\Auth\Access\Response;
trait TraitPolicy{
	protected function isAdmin(User $user){
        return $user->isAdmin()===true
                ?Response::allow()
                :Response::deny('Usted no es administrador');
    }

    protected function isUser(User $user){
    	return $user->isUser()===true
                ?Response::allow()
                :Response::deny('Usted no cuenta con permisos para realizar esta accion');
    }
}