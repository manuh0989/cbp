<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RPDActualizarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        
        return [
            /*DATOS BASICOS DEL REPORTE*/
            'RPD.folioCBP'                                      =>['required',Rule::unique('RPD')->ignore($this->RPD['folioCBP'],'folioCBP')]
            ,'RPD.idFuente'                                     =>['required','exists:fuenteReporte,idFuente']
            ,'RPD.fotos'                                        =>['nullable']
            //,'fotos.*'                                        =>['mimes:jpg,png','max:2048']
            ,'RPD.statusLocalizacion'                           =>['required']
            ,'RPD.funcionarioRegistro'                          =>['required']
            ,'RPD.folioFIPEDE'                                  =>['nullable',Rule::unique('RPD')->ignore($this->RPD['folioFIPEDE'],'folioFIPEDE'),'max:255']
            ,'RPD.folioLocatel'                                 =>['nullable',Rule::unique('RPD')->ignore($this->RPD['folioLocatel'],'folioLocatel'),'max:50']
            ,'personaDesaparecida.idOrigenNoticia'              =>['nullable','exists:origenNoticias,idOrigenNoticia','max:50']
            /*DATOS BASICOS DEL REPORTE*/
            
            /*DATOS PERSONA DESAPARECIDA*/
            ,'personaDesaparecida.nombrePD'                     =>['required','max:255']
            ,'personaDesaparecida.primerApellidoPD'             =>['required','max:255']
            ,'personaDesaparecida.segundoApellidoPD'            =>['nullable','max:255']
            ,'datosPD.fechaNacimientoPD'                        =>['nullable','date']
            ,'personaDesaparecida.edadPD'                       =>['nullable','numeric','digits_between:1,3']
            ,'personaDesaparecida.lugarNacimientoPD'            =>['nullable']
            ,'personaDesaparecida.CURPPD'                       =>['nullable','max:18']
            ,'personaDesaparecida.RFCPD'                        =>['nullable','max:13']
            ,'personaDesaparecida.idSexo'                       =>['nullable','exists:sexos,idSexo']
            ,'personaDesaparecida.idEscolaridad'                =>['nullable','exists:escolaridades,idEscolaridad']
            ,'personaDesaparecida.idEstadocivil'                =>['nullable','exists:estadosCivil,idEstadocivil']
            ,'personaDesaparecida.idVulnerabilidad'             =>['nullable','exists:vulnerabilidades,idVulnerabilidad']
            ,'personaDesaparecida.idNacionalidad'               =>['nullable','exists:nacionalidades,idNacionalidad']
            ,'personaDesaparecida.redSocialPD'                   =>['nullable','max:255']
            //,'personaDesaparecida.segundoApellidoPD'            =>['nullable']
            //,'personaDesaparecida.segundoApellidoPD'            =>['nullable']
            /*DATOS PERSONA DESAPARECIDA*/
            
            
            /*DATOS GENERALES DESAPARICION*/
            ,'datosGenerales.fechaDesaparicion'                 =>['nullable','date']
            ,'datosGenerales.horaDesaparicion'                  =>['nullable','date_format:H:i']
            ,'datosGenerales.idEstado'                          =>['nullable','exists:estados,id']
            ,'datosGenerales.idMunicipio'                       =>['nullable','exists:municipios,id']
            ,'datosGenerales.idLocalidad'                       =>['nullable','exists:localidades,id']
            /*DATOS GENERALES DESAPARICION*/
            
            /*HECHOS DESAPARICION*/
            ,'hechosDesaparicion.idLugarDesaparicion'           =>['nullable','exists:lugaresDesaparicion,idLugarDesaparicion']
            ,'hechosDesaparicion.idCircunstanciaDesaparicion'   =>['nullable','exists:circunstanciasDesapariciones,idCircunstanciaDesaparicion']
            ,'hechosDesaparicion.idHipotesisDesaparicion'       =>['nullable','exists:hipotesisDesapariciones,idHipotesisDesaparicion']
            ,'hechosDesaparicion.descripcionHechosDesaparicion' =>['nullable','string','max:600']
            ,'hechosDesaparicion.fotovolanteHechosDesaparicion' =>['nullable','string','max:300']
            /*HECHOS DESAPARICION*/
            
            /*DATOS REPORTANTE*/
            ,'reportante.nombreCompletoReportante'              =>['nullable','max:255']
            ,'reportante.idParentesco'                          =>['nullable','exists:parentescos,idParentesco']
            ,'reportante.edadReportante'                        =>['nullable','numeric']
            ,'reportante.telefonoReportante'                    =>['nullable','string']
            ,'reportante.telefono2Reportante'                   =>['nullable','string']
            ,'reportante.telefono3Reportante'                   =>['nullable','string']
            ,'reportante.correoReportante'                      =>['nullable','email']
            ,'reportante.redSocialReportante'                   =>['nullable',]
            ,'reportante.redSocial2Reportante'                  =>['nullable',]
            ,'reportante.otroContactoMedio'                     =>['nullable','string','max:500']
            ,'reportante.RNPDNO'                                =>['boolean','nullable']
            ,'reportante.contactoInicial'                       =>['boolean','nullable']
            /*DATOS REPORTANTE*/
            
            /*MEDIA FILIACIÓN*/
            ,'mediaFiliacion.idGeneroReportado'                 =>['nullable','exists:sexos,idSexo']
            ,'mediaFiliacion.idComplexion'                      =>['nullable','exists:complexiones,idComplexion']
            ,'mediaFiliacion.tez'                               =>['nullable','string']
            ,'mediaFiliacion.frente'                            =>['nullable','string']
            ,'mediaFiliacion.boca'                              =>['nullable','string']
            ,'mediaFiliacion.cejas'                             =>['nullable','string']
            ,'mediaFiliacion.menton'                            =>['nullable','string']
            ,'mediaFiliacion.colorOjos'                         =>['nullable','string']
            ,'mediaFiliacion.tipoCabello'                       =>['nullable','string']
            ,'mediaFiliacion.longitudCabello'                   =>['nullable','string']
            ,'mediaFiliacion.estatura'                          =>['nullable','numeric']
            ,'mediaFiliacion.cara'                              =>['nullable','string']
            ,'mediaFiliacion.nariz'                             =>['nullable','string']
            ,'mediaFiliacion.labios'                            =>['nullable','string']
            ,'mediaFiliacion.descripcion'                       =>['nullable','string','max:300']
            ,'mediaFiliacion.otrosDatos'                        =>['nullable','string','max:300']
            /*MEDIA FILIACIÓN*/
            
            /*LOCALIZACION*/
            ,'localizacion.idEstatusLocalizacion'               =>['nullable','exists:estatusLocalizacion,idEstatusLocalizacion']
            ,'localizacion.idTipoLugar'                         =>['nullable','exists:tipoLugar,idTipoLugar']
            ,'localizacion.idMotivoReferido'                    =>['nullable','exists:motivosReferido,idMotivoReferido']
            ,'localizacion.fechaLocalizacion'                   =>['nullable','date']
            ,'localizacion.iEstado'                             =>['nullable','exists:estados,id']
            ,'localizacion.idMunicipio'                         =>['nullable','exists:municipios,id']
            ,'localizacion.idLocalidad'                         =>['nullable','exists:localidades,id']
            ,'localizacion.calleLugar'                          =>['nullable']
            ,'localizacion.delitoCometido'                      =>['nullable']
            ,'localizacion.bajaReporte'                         =>['nullable','boolean']

            ,'localizacion.delitoCometido'                      =>['nullable']
            ,'localizacion.idEstadoPersona'                     =>['nullable']
            ,'localizacion.idConstancia'                        =>['nullable']
            ,'localizacion.idDelito'                            =>['nullable']
            ,'localizacion.idCausaDefuncion'                    =>['nullable']
            ,'localizacion.observacion'                         =>['nullable','max:300']
            /*LOCALIZACION*/
            
        ];
    }

    public function messages(){
        return [
            'RPD.folioCBP.required'       => 'El campo Folio CBP es obligatorio'
            ,'RPD.folioCBP.unique'        => 'El folio CBP ya ha sido registrado'
            ,'nombrePD.required'          => 'El campo nombre de la persona desaparecida  es obligatorio'
            ,'primerApellidoPD.required'  => 'El campo primer apellido  de la persona desaparecida  es obligatorio'
            ,'idGeneroReportado.required' =>'El campo genero es obligatorio'
            ,'idComplexion'               =>'El campo complexion es obligatorio'
            ,'RPD.folioFIPEDE.max'        =>'Carpeta de investigación no debe ser mayor que :max'
            ,'RPD.folioLocatel.max'       =>'Folio locatel no debe ser mayor que :max'
            //,'estatura.digits'=>'La estatura debe de estar entre 1.00 y 2.89 metros'
        ];
    }
}
