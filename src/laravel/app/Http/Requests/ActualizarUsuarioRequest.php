<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class ActualizarUsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'            => ['required', 'string', 'max:255']
            ,'username'         => ['required',Rule::unique('usuarios')->ignore($this->username,'username')]
            ,'apellido_paterno' => ['required']
            ,'apellido_materno' => ['required']
            ,'email'            => ['required', 'string', 'email', 'max:255', Rule::unique('usuarios')->ignore($this->email,'email')]
            ,'idRole'           => ['required','exists:roles,idRole']
            ,'password'         => ['required', 'string', 'min:8', 'confirmed']
        ];
    }
}
