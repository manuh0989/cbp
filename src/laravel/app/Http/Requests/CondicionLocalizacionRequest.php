<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CondicionLocalizacionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'idEstatusLocalizacion' =>['required','exists:estatusLocalizacion,idEstatusLocalizacion']
            ,'idTipoLugar'          =>['required','exists:tipoLugar,idTipoLugar']
            ,'idMotivoReferido'     =>['required','exists:motivosReferido,idMotivoReferido']
            ,'fechaLocalizacion'    =>['required','date']
            ,'iEstado'              =>['nullable','exists:estados,id']
            ,'idMunicipio'          =>['nullable','exists:municipios,id']
            ,'idLocalidad'          =>['nullable','exists:localidades,id']
            ,'calleLugar'           =>['nullable']
            ,'delitoCometido'       =>['nullable']
            ,'bajaReporte'          =>['nullable','boolean']
            ];
    }
}
