<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Hash,Validator};
use App\Http\Requests\ActualizarUsuarioRequest;
use DB;
class UserController extends Controller{

    protected $usuario;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $encabezados=[
            '#'
            ,'Nombre'
            ,'correo'
            ,'Fecha creación'
            ,'Fecha ultima actualización'
            ,'Estatus'
            ,'Role'
            ,'Acciones'
        ];
        $usuarios=User::with('role')->withTrashed()->get();
        return view('admin.usuario.index',compact('encabezados','usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $usuario=new User();
        return view('admin.usuario.crear',compact('usuario'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    public function restore($id){
        $user = User::withTrashed()->find($id)->restore();
        return redirect()->route('usuario.index')
            ->with('message','Usuario restaurado correctamente');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $usuario){
        return view('admin.usuario.editar',compact('usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(ActualizarUsuarioRequest $request, User $usuario){
        DB::beginTransaction();
        try {
            
            $data=$request->validated();
            $usuario->update([
                'idRole'            => $data['idRole']
                ,'nombre'           => $data['nombre']
                ,'username'         => $data['username']
                ,'apellido_paterno' => $data['apellido_paterno']
                ,'apellido_materno' => $data['apellido_materno']
                ,'email'            => $data['email']
                ,'password'         => Hash::make($data['password'])
            ]);

            DB::commit();
            return redirect()->route('usuario.index')
            ->with('message','Usuario actualizado correctamente');
        }catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $usuario){
        DB::beginTransaction();
        try {
            $usuario->delete();
            DB::commit();
            return redirect()->route('usuario.index')->with('message','Usuario eliminado logicamente');
        }catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
