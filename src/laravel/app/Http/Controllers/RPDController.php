<?php

namespace App\Http\Controllers;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\{Storage,Cookie};

use Illuminate\Support\{Carbon,Str};
use Illuminate\Http\Request;
use DataTables;
use App\Models\{RPD,FotografiaRPD,PersonaDesaparecida
    ,DatoGeneralRPD
    ,ReportanteRPD
    ,FuenteReporte,OrigenNoticia,Vulnerabilidad,EstadoCivil,Escolaridad,Sexo
    ,CircunstanciaDesaparicion
    ,HipotesisDesaparicion
    ,Parentesco
    ,HechoDesaparicionRPD
    ,Complexion
    ,MediaFiliacionRPD
    ,FolioCBP
    ,Estado
    ,Municipio
    ,Localidad
    ,LugarDesaparicion
    ,TransaccionRPD
    ,EstatusSeguimientoRPD
    ,SeguimientoRPD
    ,CanalizacionRPD
    ,CarpetaRPD
    ,EstatusLocalizacion
    ,CondicionLocalizacion
    ,Nacionalidad
};
use App\Exports\RPDExport;
use App\Http\Requests\{
    RPDActualizarRequest
    ,RPDRegistroRequest
    ,SeguimientoRPDRequest
    ,CanalizacionRPDRequest
    ,CarpetaRPDRequest
    ,CondicionLocalizacionRequest
};
use DB;

class RPDController extends Controller{

    protected $RPD;
    protected $fotografiaRPD;
    protected $personaDesaparecida;
    protected $datoGeneralRPD;
    protected $hechoDesaparicionRPD;
    protected $reportanteRPD;
    protected $mediaFiliacionRPD;

    protected $encabezados=[
        '#'
        ,'Folio CBP'
        ,'Carpeta investigación'
        ,'Persona desaparecida'
        ,'Fecha desaparición'
        ,'Fecha Actualización'
        ,'Estatus'
        ,'Acciones'
        ,'Seguimiento'
    ];

    protected $seguimiento;
    protected $canalizacion;
    protected $carpeta;
    
    public function index(Request $request){
        $RPD      =new RPD();
        
        Cookie::queue('filtros', json_encode($request->all()) ?? '' ,60);

        $listaRPD =$RPD->getListaRPD($request);
        
        $listaRPD->appends($request->only(
            ['buscar','opcionBusqueda','fechaDesaparicionIni','fechaDesaparicionFin','chkFecha']
        )); // para que la paginacion respete los filtros

        $encabezados=$this->encabezados;
        return view('RPD.index',compact('listaRPD','RPD','encabezados'));
    }

    public function create(){
        return $this->formView('RPD.crear',new RPD());
    }

    public function store(RPDRegistroRequest $request){
        DB::beginTransaction();

        try {
            $this->guardarRPD($request)
            ->guardarFotos($request)
            ->guardarPersonaDesaparecida($request)
            ->guardarDatosGenerales($request)
            ->guardarHechosDesaparicion($request)
            ->guardarReportante($request)
            ->guardarMediaFiliacion($request)
            ->guardarActualizarCondicionLocalizacion($request)
            ->transaccionRPD($this->RPD,'created');

            DB::commit();
            return redirect()->route('RPD.editar',$this->RPD)
            ->with('message',"Reporte RPD registrado correctamente con folioCBP: {$this->RPD->folioCBP} ");

        }catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function show(RPD $RPD){
        //$this->authorize('update',User::class);
        return $this->formView('RPD.mostrar',$RPD);
    }

    public function edit(RPD $RPD){
        return $this->formView('RPD.editar',$RPD);
    }

    public function seguimiento(RPD $RPD){

        return view('RPD.seguimiento.index',[
            'RPD'                    =>$RPD
            ,'estados'               =>Estado::getAllEstados()
            ,'municipios'            =>Municipio::getMunicipioRPD(optional($RPD->condicionLocalizacion)->idEstado)
            ,'localidades'           =>Localidad::getLocalidadRPD(optional($RPD->condicionLocalizacion)->idMunicipio)
            ,'estatusLocalizaciones' =>EstatusLocalizacion::all()
        ]);
    }


    public function carpeta(RPD $RPD){
        return $this->formView('RPD.carpeta.index',$RPD);
    }

    public function deleteCarpeta(Request $request){
        Storage::disk('public')->delete($request->archivo);
        return redirect()
        ->back()
        ->with(['message'=>"Archivo ".(new RPD())->getNameFileCarpeta($request->archivo)." borrado correctamente"]);
    }

    public function canalizar(RPD $RPD){
        return $this->formView('RPD.canalizar.index',$RPD);   
    }

    public function storeSeguimiento(SeguimientoRPDRequest $request,RPD $RPD){
        DB::beginTransaction();
        try {
            $this->seguimiento=SeguimientoRPD::storeSeguimiento($request);
            DB::commit();
            return response()->json([
                'message'=>'Seguimiento creado exitosamente'
            ]);
        }catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function storeCanalizacion(CanalizacionRPDRequest $request, RPD $RPD){
        DB::beginTransaction();
        try {

            $this->canalizacion=CanalizacionRPD::storeCanalizacion($request,$RPD);

            DB::commit();
            return redirect()->route('RPD.seguimiento',$RPD)->with([
                'message'=>"Diligencia creada exitosamente RPD {$RPD->FolioCBP}"
            ]);
        }catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function storeCarpeta(CarpetaRPDRequest $request, RPD $RPD){
        DB::beginTransaction();
        try{
            if($request->ajax()){
                $this->carpeta=CarpetaRPD::storeCarpeta($request,$RPD);
                DB::commit();
                return response()->json([
                    'message'=>'Carpeta creada exitosamente'
                    ,'carpeta'=>$this->carpeta->CarpetaText
                ]);
            }

            foreach($request->file()['archivos'] ?? []  as $key =>$file){
                CarpetaRPD::almacenarArchivos($file,$RPD);
            }

            return redirect()
            ->route('RPD.seguimiento',$RPD)
            ->with(['message'=>'Archivos cargados exitosamente']);

        }catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }

    }

    public function update(RPDActualizarRequest $request, RPD $RPD){
        $this->RPD=$RPD;
        DB::beginTransaction();
        try {
            $this->guardarRPD($request)
            ->guardarFotos($request)
            ->guardarPersonaDesaparecida($request)
            ->guardarDatosGenerales($request)
            ->guardarHechosDesaparicion($request)
            ->guardarReportante($request)
            ->guardarMediaFiliacion($request)
            ->guardarActualizarCondicionLocalizacion($request)
            ->transaccionRPD($this->RPD,'updated');

            DB::commit();
            //return redirect()->route('RPD.index')->with('message','Actualizar ok');
            return redirect()
            ->back()
            ->with('message','Reporte RPD actualizado correctamente con folioCBP: '.$this->RPD->folioCBP);
        }catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function destroy(RPD $RPD){
        DB::beginTransaction();
        try {
            $this->deleteRPD($RPD)
            ->transaccionRPD($this->RPD,'deleted');
            DB::commit();
            return redirect()->route('RPD.index')->with('message','Reporte eliminado correctamente');
        }catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
    
    protected function deleteRPD(RPD $RPD){
        $this->RPD=$RPD->deleteRPD();
        return $this;
    }

    protected function guardarRPD( $request){
        
        if($request->isMethod('post')){
            (new FolioCBP($request->RPD['idFuente']))->generarFolioCBP();   
        }
        $this->RPD= RPD::guardarActualizarRPD((object)$request->RPD,optional($this->RPD)->idRPD);
        return $this;
    }

    protected function actulizarFotos(Request $request){
        FotografiaRPD::storeFotoRPD((object)$request->mediaFiliacion,$this->RPD);
        return $this;
    }

    protected function guardarFotos(Request $request){
        FotografiaRPD::storeFotoRPD((object)$request->mediaFiliacion,$this->RPD);
        return $this;
    }

    protected function guardarPersonaDesaparecida($request){
        $this->personaDesaparecida=PersonaDesaparecida::guardarActualizarPersonaDesaparecida(
            (object)$request->personaDesaparecida
            ,$this->RPD->idRPD
        );
        return $this;
    }

    protected function guardarDatosGenerales($request){
        $this->datosGeneralesRPD=DatoGeneralRPD::guardarActualizarDatosGenerales(
            (object)$request->personaDesaparecida
            ,$this->RPD->idRPD
            ,$this->personaDesaparecida->idPersonaDesaparecida
        );
        return $this;
    }

    protected function guardarHechosDesaparicion($request){

        $this->hechoDesaparicionRPD= HechoDesaparicionRPD::guardarActualizarHechosDesaparicion(
            (object)$request->hechosDesaparicion
            ,$this->RPD->idRPD
            ,$this->personaDesaparecida->idPersonaDesaparecida
        );
        return $this;
    }

    protected function guardarReportante($request){

        $this->reportanteRPD=ReportanteRPD::guardarActualizarReportante(
            (object) $request->reportante
            ,$this->RPD->idRPD
            ,$this->personaDesaparecida->idPersonaDesaparecida
        );
        return $this;
    }

    protected function guardarMediaFiliacion($request){
        $this->mediaFiliacionRPD=MediaFiliacionRPD::guardarActualizarMediaFiliacion(
            (object) $request->mediaFiliacion
            ,$this->RPD->idRPD
        );
        return $this;
    }

    protected function formView($view,RPD $RPD){
        $folio=new FolioCBP(optional($RPD->fuenteRPD)->idFuente??FuenteReporte::NOTICIA);
        return view($view,[
            'RPD'                        =>$RPD
            ,'fuentes'                   =>FuenteReporte::all()
            ,'origenNoticias'            =>OrigenNoticia::all()
            ,'vulnerabilidades'          =>Vulnerabilidad::all()
            ,'estadosCivil'              =>EstadoCivil::all()
            ,'escolaridades'             =>Escolaridad::all()
            ,'sexos'                     =>Sexo::all()
            ,'estados'                   =>Estado::getAllEstados()
            ,'municipios'                =>Municipio::getMunicipioRPD(optional($RPD->datoGeneralRPD)->idEstado)
            ,'localidades'               =>Localidad::getLocalidadRPD(optional($RPD->datoGeneralRPD)->idMunicipio)
            ,'circunstanciaDesaparicion' =>CircunstanciaDesaparicion::all()
            ,'hipotesisDesaparicion'     =>HipotesisDesaparicion::all()
            ,'complexiones'              =>Complexion::all()
            ,'parentescos'               =>Parentesco::all()
            ,'lugaresDesaparicion'       =>LugarDesaparicion::all()
            ,'folioCBP'                  =>(!$RPD->folioCBP)?$folio->getFolioCBP() : $RPD->folioCBP
            ,'mediaFiliacion'            =>MediaFiliacionRPD::catalogos()
            ,'estatusLocalizaciones'     =>EstatusLocalizacion::all()
            ,'nacionalidades'            =>Nacionalidad::all()
            ]);
    }

    protected function actualizarRPD(RPDActualizarRequest $request){
        $this->RPD->updateOrCreate([
            'idRPD'=>$this->RPD->idRPD
        ],[
            'idFuente'             =>$request->idFuente
            ,'folioCBP'            =>$request->folioCBP
            ,'statusLocalizacion'  =>$request->statusLocalizacion
            ,'funcionarioRegistro' =>$request->funcionarioRegistro
            ,'folioFIPEDE'         =>$request->folioFIPEDE
            ,'folioLocatel'        =>$request->folioLocatel
            ,'updated_at'          =>Carbon::now()
        ]);
        return $this;
    }



    protected function actualizarDatosGenerales(RPDActualizarRequest $request){
        
        $this->datoGeneralRPD=$this->RPD->DatoGeneralRPD->updateOrCreate([

            'idPersonaDesaparecida' =>$this->RPD->personaDesaparecida->idPersonaDesaparecida
            ,'idRPD'                =>$this->RPD->idRPD
        ],[
            'idRPD'                    =>$this->RPD->idRPD
            ,'idPersonaDesaparecida'   =>$this->personaDesaparecida->idPersonaDesaparecida
            ,'fechaDesaparicion'       =>setFechaIsNull($request->fechaDesaparicion)
            ,'horaDesaparicion'        =>$request->horaDesaparicion
            
            ,'idEstado'                =>$request->idEstado
            ,'idMunicipio'             =>$request->idMunicipio
            ,'idLocalidad'             =>$request->idLocalidad
            ,'calleNumeroDesaparicion' =>$request->calleNumeroDesaparicion
            ,'updated_at'              =>Carbon::now()
            
        ]);
        
        return $this;
    }

    protected function actualizarHechosDesaparicion(RPDActualizarRequest $request){

        $this->hechoDesaparicionRPD= $this->RPD->hechoDesaparicionRPD->updateOrCreate([
            'idRPD'                  =>$this->RPD->idRPD
            ,'idPersonaDesaparecida' =>$this->personaDesaparecida->idPersonaDesaparecida
        ],[
            'idRPD'                         =>$this->RPD->idRPD
            ,'idPersonaDesaparecida'        =>$this->RPD->personaDesaparecida->idPersonaDesaparecida
            ,'idLugarDesaparicion'          =>$request->idLugarDesaparicion
            ,'idCircunstanciaDesaparicion'  =>$request->idCircunstanciaDesaparicion
            ,'idHipotesisDesaparicion'      =>$request->idHipotesisDesaparicion
            ,'descripcionHechosDesaparicion' =>$request->descripcionHechosDesaparicion
            ,'fotovolanteHechosDesaparicion' =>$request->fotovolanteHechosDesaparicion
            ,'updated_at'                   =>Carbon::now()
        ]);

        return $this;
    }

    protected function actualizarReportante(RPDActualizarRequest $request){
        $this->reportanteRPD=$this->RPD->reportanteRPD->updateOrCreate([
            'idRPD'                =>$this->RPD->idRPD
            ,'idPersonaDesaparecida' =>$this->personaDesaparecida->idPersonaDesaparecida
        ]
        ,[
            'idRPD'                     =>$this->RPD->idRPD
            ,'idPersonaDesaparecida'    =>$this->RPD->personaDesaparecida->idPersonaDesaparecida
            ,'idParentesco'             =>$request->idParentesco
            ,'nombreCompletoReportante' =>$request->nombreCompletoReportante
            ,'edadReportante'           =>$request->edadReportante
            ,'telefonoReportante'       =>$request->telefonoReportante
            ,'telefono2Reportante'      =>$request->telefono2Reportante
            ,'telefono3Reportante'      =>$request->telefono3Reportante
            ,'correoReportante'         =>$request->correoReportante
            ,'redSocialReportante'      =>$request->redSocialReportante
            ,'redSocial2Reportante'     =>$request->redSocial2Reportante
            ,'otroContactoMedio'        =>$request->otroContactoMedio
            ,'updated_at'          =>Carbon::now()
        ]);

        return $this;
    }

    protected function actualizarMediaFiliacion(RPDActualizarRequest $request){

        $this->mediaFiliacionRPD=$this->RPD->mediaFiliacionRPD->updateOrCreate([
            'idRPD' =>$this->RPD->idRPD
            ]
            ,[
                'idRPD'             =>$this->RPD->idRPD
                ,'idGeneroReportado' =>$request->idGeneroReportado
                ,'idComplexion'      =>$request->idComplexion
                ,'tez'               =>$request->tez
                ,'frente'            =>$request->frente
                ,'boca'              =>$request->boca
                ,'cejas'             =>$request->cejas
                ,'menton'            =>$request->menton
                ,'colorOjos'         =>$request->colorOjos
                ,'tipoCabello'       =>$request->tipoCabello
                ,'longitudCabello'   =>$request->longitudCabello
                ,'estatura'          =>$request->estatura
                ,'cara'              =>$request->cara
                ,'nariz'             =>$request->nariz
                ,'labios'            =>$request->labios
                ,'updated_at'          =>Carbon::now()
        ]);
        
        return $this;
    }
    
    public function transaccionRPD(RPD $RPD,$accion){
        TransaccionRPD::transaccionRPD($RPD,$accion);
        return $this;
    }

    public function getPDF(RPD $RPD){
        $data = DB::table('RPD')
        ->select('RPD.*','RPD.updated_at as ultimaActualizacion','RPD.created_at as fechaRegistro', 'sexos.*', 'origenNoticias.*', 'escolaridades.*', 'estadosCivil.*','vulnerabilidades.*', 'hechosDesaparicionesRPD.*', 'circunstanciasDesapariciones.*', 'hipotesisDesapariciones.*', 'mediaFiliacionesRPD.*', 'datosGeneralesRPD.*','estados.*', 'estados.nombre as nombreEstado', 'municipios.nombre as nombreMuni', 'localidades.*', 'personasDesaparecidas.*', 'fotografiasRPD.*', 'reportantesRPD.*', 'parentescos.*','lugaresDesaparicion.*')
        ->leftJoin('personasDesaparecidas', 'RPD.idRPD', '=', 'personasDesaparecidas.idRPD')
        ->leftJoin('sexos', 'sexos.idSexo', '=', 'personasDesaparecidas.idSexo')
        ->leftJoin('origenNoticias', 'personasDesaparecidas.idOrigenNoticia', '=', 'origenNoticias.idOrigenNoticia')
        ->leftJoin('escolaridades', 'personasDesaparecidas.idEscolaridad', '=', 'escolaridades.idEscolaridad')
        ->leftJoin('estadosCivil', 'personasDesaparecidas.idEstadocivil', '=', 'estadosCivil.idEstadoCivil')
        ->leftJoin('vulnerabilidades', 'personasDesaparecidas.idVulnerabilidad', '=', 'vulnerabilidades.idVulnerabilidad')
        ->leftJoin('hechosDesaparicionesRPD', 'RPD.idRPD', '=' ,'hechosDesaparicionesRPD.idRPD')
        ->leftjoin('lugaresDesaparicion','lugaresDesaparicion.idLugarDesaparicion','=','hechosDesaparicionesRPD.idLugarDesaparicion')
        ->leftJoin('circunstanciasDesapariciones', 'hechosDesaparicionesRPD.idCircunstanciaDesaparicion', '=', 'circunstanciasDesapariciones.idCircunstanciaDesaparicion')
        ->leftJoin('hipotesisDesapariciones', 'hechosDesaparicionesRPD.idHipotesisDesaparicion', '=', 'hipotesisDesapariciones.idHipotesisDesaparicion')
        ->leftJoin('mediaFiliacionesRPD', 'RPD.idRPD', '=', 'mediaFiliacionesRPD.idRPD')
        ->leftJoin('datosGeneralesRPD', 'RPD.idRPD', '=', 'datosGeneralesRPD.idRPD')
        ->leftJoin('estados', 'datosGeneralesRPD.idEstado', '=', 'estados.id')
        ->leftJoin('municipios', 'datosGeneralesRPD.idMunicipio', '=', 'municipios.id')
        ->leftJoin('localidades', 'datosGeneralesRPD.idLocalidad', '=', 'localidades.id')
        ->leftJoin('fotografiasRPD', 'RPD.idRPD', '=', 'fotografiasRPD.idRPD')
        ->leftJoin('reportantesRPD', 'RPD.idRPD', '=', 'reportantesRPD.idRPD')
        ->leftJoin('parentescos', 'reportantesRPD.idParentesco', '=', 'parentescos.idParentesco')

        ->leftJoin('condicionLocalizacion as cl', 'RPD.idRPD','=','cl.idRPD')
        ->leftJoin('estatusLocalizacion as loc', 'loc.idEstatusLocalizacion','=','cl.idEstatusLocalizacion')
        ->leftJoin('motivosReferido as mot', 'mot.idMotivoReferido','=','cl.idMotivoReferido')
        ->leftJoin('estadoPersona as ep', 'ep.idEstadoPersona','=','cl.idEstadoPersona')
        ->leftJoin('tipoLugar as tp', 'tp.idTipoLugar','=','cl.idTipoLugar')

        ->where('RPD.idRPD', '=', $RPD->idRPD)
        ->get();

        $nombre = $data[0]->nombrePD;

        $pdf = \PDF::loadView('PDF.pdf', compact('data', 'RPD'));
        return $pdf->stream($nombre . '_Archivo.pdf');
        
    }

    public function ftVolante(Request $request){
        $id = $request->id;

        $data = DB::table('RPD as a')
        ->select('a.idRPD', 'a.folioCBP', 'b.nombreFuente','c.descripcionHechosDesaparicion', 'd.tez', 'd.frente', 'd.boca', 'd.cejas', 'd.menton', 'd.colorOjos', 'd.tipoCabello', 'd.longitudCabello', 'd.estatura', 'd.cara', 'd.nariz', 'd.labios', 'e.nombreComplexion','f.fechaDesaparicion', 'g.foto', 'h.edadPD', 'h.nombrePD', 'h.primerApellidoPD', 'h.segundoApellidoPD', 'i.nombreSexo', 'j.nombre','municipios.nombre as nombreMunicipio','estados.nombre as nombreEstado','j.nombre as nombreLocalidad','c.fotovolanteHechosDesaparicion')
        ->leftjoin('fuenteReporte as b', 'a.idFuente', '=', 'b.idFuente')
        ->leftjoin('hechosDesaparicionesRPD as c', 'a.idRPD', '=', 'c.idRPD')
        ->leftjoin('mediaFiliacionesRPD as d', 'a.idRPD', '=', 'd.idRPD')
        ->leftjoin('complexiones as e', 'd.idComplexion', '=', 'e.idComplexion')
        ->leftjoin('datosGeneralesRPD as f', 'a.idRPD', '=', 'f.idRPD')
        ->leftjoin('fotografiasRPD as g', 'a.idRPD', '=', 'g.idRPD')
        ->leftJoin('personasDesaparecidas as h', 'a.idRPD', '=', 'h.idRPD')
        ->leftJoin('sexos as i', 'h.idSexo', '=', 'i.idSexo')
         ->leftJoin('estados', 'f.idEstado', '=', 'estados.id')
        ->leftJoin('municipios', 'f.idMunicipio', '=', 'municipios.id')
        ->leftjoin('localidades as j', 'f.idLocalidad', '=', 'j.id')
        ->where('a.idRPD', '=', $id)
        ->get();

        $nombre = $data[0]->nombrePD;
        
        //return $data;
        //return view('PDF.ftVolante', compact('id', 'data'));
        $pdf = \PDF::loadView('PDF.ftVolante', compact('id', 'data'));
        $pdf->setPaper('letter', 'landscape');
        return $pdf->stream('Volante_' . $nombre . '.pdf');
    }


    protected function guardarActualizarCondicionLocalizacion($request){
        $this->localizacion=CondicionLocalizacion::guardarActualizarCondicionLocalizacion((object) $request->localizacion, $this->RPD->idRPD);

        return $this;
    }
}