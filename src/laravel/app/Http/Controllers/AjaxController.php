<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\{Localidad,Municipio,RPD,EstatusSeguimientoRPD,FolioCBP};

class AjaxController extends Controller{
    
    public function getFolioCBP(Request $request){
        return response()->json([
            'folioCBP'=>(new FolioCBP($request->idFuente))->getFolioCBP()
        ]);
    }

    public function municipios(Request $request, RPD $RPD){
    	if($request->ajax()){
            $municipios        =Municipio::getMunicipioRPD($request->idEstado);
            $idMunicipio       =$request->idMunicipio;
            $idLocalidad       =$request->idLocalidad;
            $nameAttrMunicipio =$request->nameAttrMunicipio;
            $idAttrMunicipio   =$request->idAttrMunicipio;
            $idAttrLocalidad   =$request->idAttrLocalidad;
            $nameAttrLocalidad =$request->nameAttrLocalidad;
            $divMunicipio      =$request->divMunicipio;
            $divLocalidad      =$request->divLocalidad;

    		return response()->json([
    			'view'=>view('RPD.ajax._selectMunicipios'
                    ,compact('municipios','idMunicipio','idLocalidad','RPD','idAttrMunicipio','nameAttrMunicipio','idAttrLocalidad','nameAttrLocalidad','divMunicipio','divLocalidad'))->render()
    			,'municipios'=>$municipios
    		]);
    	}
    }

    public function localidades(Request $request , RPD $RPD){
    	if($request->ajax()){
            $idMunicipio       =$request->idMunicipio;
            $idLocalidad       =$request->idLocalidad;
            $localidades       =Localidad::getLocalidadRPD($request->idMunicipio);
            $nameAttrLocalidad =$request->nameAttrLocalidad;
            $idAttrLocalidad   =$request->idAttrLocalidad;

            return response()->json([
                'view'=>view('RPD.ajax._selectLocalidades',compact('localidades','idMunicipio','RPD','idLocalidad','nameAttrLocalidad','idAttrLocalidad'))->render()
                ,'localidades'=>$localidades
            ]);
        }
    }

    public function calcularEdad (Request $request){
        return response()->json([
            'edad'=>Carbon::createFromDate($request->fecha)->age
        ]);
    }

    public function catalogos(Request $request){
        $view=view('admin.catalogo.ajax._catalogos'
        ,[
                'datos'        =>$request->model::withTrashed()->get()
                ,'model'       =>$request->model
                ,'idTable'     =>"tbl{$request->model}"
                ,'encabezados' =>[
                    '#'
                    ,'Nombre'
                    ,'Estatus'
                    ,'Acciones'
                 ]
            ])->render();
        return response()->json([
            'view'=>$view
        ]);
    }

    public function trashedCatalogo(Request $request){
        $model=new $request->model;
        return response()->json([
            'model'   =>$request->model
            ,'result' =>$model->find($request->idCatalogo)->delete()
            ,'code'   =>200
        ]);
    }

    public function restoredCatalogo(Request $request){
        $model=new $request->model;
        return response()->json([
            'model'   =>$request->model
            ,'result' =>$model->withTrashed()->find($request->idCatalogo)->restore()
            ,'code'   =>200
        ]);
    }

    public function seguimientoContacto(Request $request,RPD $RPD){
        if(!$RPD->validaSeguimiento($request) && $request->contacto>1){
            $view=view('RPD.ajax500',['message'=>'Debe capturar un seguimiento anterior'])->render();
            return response()->json([
                'message'   =>'Debe capturar un seguimiento anterior'
                ,'view'     =>$view
                ,'contacto' =>$request->contacto
            ],500);
        }
        $view=view('RPD.seguimiento._camposSeguimiento',[
            'RPD'          =>$RPD
            ,'estatusSeguimientosRPD'=>EstatusSeguimientoRPD::all()
            ,'contacto'    =>$request->contacto
            ,'seguimiento' =>$RPD->seguimientosRPD[$request->contacto-1]??[]
        ])->render();

         return response()->json([
            'view'      =>$view
            ,'contacto' =>$request->contacto
        ]);
    }

    public function estatusLocalizacion(Request $request, RPD $RPD){
        $idEstatusLocalizacion =
        $idTipoLugar           =$request->idTipoLugar;
        $view=view('RPD.condicionLocalizacion._ajaxCamposEstatusLocalizacion',[
                'idEstatusLocalizacion' =>$request->idEstatusLocalizacion
                ,'idTipoLugar'          =>$request->idTipoLugar
                ,'idMotivoReferido'     =>$request->idMotivoReferido
                ,'idEstadoPersona'      =>$request->idEstadoPersona
                ,'idConstancia'         =>$request->idConstancia
                ,'idDelito'             =>$request->idDelito
                ,'idCausaDefuncion'     =>$request->idCausaDefuncion
                ,'RPD'                  =>$RPD
            ]
        )
        ->render();
        return response()->json([
            'view'=>$view
        ]);
    }
}
