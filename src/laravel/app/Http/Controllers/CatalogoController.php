<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CatalogoController extends Controller{
    
    public function index(){
    	$catalogos=[
			'App\Models\FuenteReporte'                   =>'Fuente Reporte'
			,'App\Models\OrigenNoticia'                  =>'Origen de noticia'
			,'App\Models\Sexo'                           =>'Sexos '
			,'App\Models\Escolaridad'                    =>'Escolaridades'
			,'App\Models\EstadoCivil'                    =>'Estados Civil'
			,'App\Models\Vulnerabilidad'                 =>'Vulnerablilidades'
			,'App\Models\CircunstanciaDesaparicion'      =>'Circunstancia de desaparicion'
			,'App\Models\LugarDesaparicion'              =>'Lugar desaparicion'
			,'App\Models\HipotesisDesaparicion'          =>'Hipotesis de desaparicion'
			,'App\Models\Parentesco'                     =>'Parentescos'
			,'App\Models\Complexion'                     =>'Complexiones'
			,'App\Models\EstatusLocalizacion'            =>'Estatus localizacion'
			,'App\Models\TipoLugar'                      =>'Tipo lugar'
			,'App\Models\MotivoReferido'                 =>'Motivo referido'
			,'App\Models\Nacionalidad'                   =>'Nacionalidades'
			
			,'App\Models\CausaDefuncion'                 =>'Causa de defunción'
			,'App\Models\Delito'                         =>'Delito'
			,'App\Models\Constancia'                     =>'Constancia'
			,'App\Models\EstadoPersona'                  =>'Estado de la persona'
			
			
			,'App\Models\MediaFiliacion\Boca'            =>'Boca'
			,'App\Models\MediaFiliacion\Cara'            =>'Cara'
			,'App\Models\MediaFiliacion\Cejas'           =>'Cejas'
			,'App\Models\MediaFiliacion\ColorOjos'       =>'ColorOjos'
			,'App\Models\MediaFiliacion\Frente'          =>'Frente'
			,'App\Models\MediaFiliacion\Labios'          =>'Labios'
			,'App\Models\MediaFiliacion\LongitudCabello' =>'Longitud Cabello'
			,'App\Models\MediaFiliacion\Menton'          =>'Menton'
			,'App\Models\MediaFiliacion\Nariz'           =>'Nariz'
			,'App\Models\MediaFiliacion\Tez'             =>'Tez'
			,'App\Models\MediaFiliacion\TipoCabello'     =>'TipoCabello'
			,'App\Models\MediaFiliacion\TipoCabello'     =>'TipoCabello'
			
    	];
    	return view('admin.catalogo.index',compact('catalogos'));
    }

    public function store(Request $request){
    	if($request->ajax()){
	
			$model = new $request->catalogos;
			$campo =$model->getFillable()[0];
	    	$validatedData = $request->validate([
				'nombre'     => ['required','max:255',"unique:{$model->getTable()},{$campo}"]
				,'catalogos' => ['required']

	    	]);
	    	$model->create([
	    		$campo=>$request->nombre
	    	]);

	    	return response()->json([
				'data'   =>$validatedData
				,'model' =>$model
				,'message'=>'Catalogo registrado correctamente'
	    	]);
		}	    
    }

    public function update( Request $request, $id){
		$txtModel ="model{$id}";
		$dato     ="txt{$id}";
    	
		$model    =$request->$txtModel;
		
		$model    =new $model;
		$model    =$model->find($id);
		$campo    =$model->getFillable()[0];

	
		$update=$model->update([
			$campo=>$request->$dato
		]);
    	return response()->json([
    		'span'=>"span{$id}"
    		,'dato'=>$request->$dato
    	]);
    }
}
