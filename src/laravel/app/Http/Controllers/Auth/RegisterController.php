<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre'              => ['required', 'string', 'max:255']
            ,'username'         => ['required','unique:usuarios,username']
            ,'apellido_paterno' => ['required']
            ,'apellido_materno' => ['required']
            ,'email'            => ['required', 'string', 'email', 'max:255', 'unique:usuarios,email']
            ,'password'         => ['required', 'string', 'min:8', 'confirmed']
            ,'idRole'           => ['required','exists:roles,idRole']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data){
        return User::create([
            'idRole'            => $data['idRole']
            ,'nombre'           => $data['nombre']
            ,'username'         => $data['username']
            ,'apellido_paterno' => $data['apellido_paterno']
            ,'apellido_materno' => $data['apellido_materno']
            ,'email'            => $data['email']
            ,'password'         => Hash::make($data['password'])
            ,'email_verified_at'=> Carbon::now()
        ]);
    }

    public function register(Request $request){
        $this->validator($request->all())->validate();
        $user = $this->create($request->all());
        

        return back()->with('message','Usuario reigstrado exitosamente ');
        //event(new Registered($user = $this->create($request->all())));

        //$this->guard()->login($user);
        
        /*return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
        */
    }
}