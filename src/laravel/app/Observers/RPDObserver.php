<?php

namespace App\Observers;

use App\Models\{TransaccionRPD,RPD};

class RPDObserver{

    /**
     * Handle the r p d "created" event.
     *
     * @param  \App\RPD  $RPD
     * @return void
     */
    public function created(RPD $RPD){
        TransaccionRPD::create([
            'idRPD'            =>$RPD->idRPD
            ,'idUsuario'       =>auth()->user()->idUsuario
            ,'accion'          =>'created'
            /*,'datos'           =>json_encode([
                ,'datoOriginal:'   =>$RPD->getOriginal()
                ,'datoActualziado' =>$RPD->getAttributes()
            ])*/
        ]);
    }

    /**
     * Handle the r p d "updated" event.
     *
     * @param  \App\RPD  $RPD
     * @return void
     */
    public function updated(RPD $RPD){
        TransaccionRPD::create([
            'idRPD'            =>$RPD->idRPD
            ,'idUsuario'       =>auth()->user()->idUsuario
            ,'accion'          =>'updated'
            /*,'datos'           =>json_encode([
                ,'datoOriginal:'   =>$RPD->getOriginal()
                ,'datoActualziado' =>$RPD->getAttributes()
            ])*/
        ]);
    }

    /**
     * Handle the r p d "deleted" event.
     *
     * @param  \App\RPD  $RPD
     * @return void
     */
    public function deleted(RPD $RPD)
    {
        //
    }

    /**
     * Handle the r p d "restored" event.
     *
     * @param  \App\RPD  $RPD
     * @return void
     */
    public function restored(RPD $RPD)
    {
        //
    }

    /**
     * Handle the r p d "force deleted" event.
     *
     * @param  \App\RPD  $RPD
     * @return void
     */
    public function forceDeleted(RPD $RPD)
    {
        //
    }
}
