<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(){
        Blade::include('compartidos._inputErrors', 'inputErrors');

        Blade::component('compartidos._table','table');
        Blade::component('compartidos._card','card');
    }
}
