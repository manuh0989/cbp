<?php

namespace App\Providers;


use Illuminate\Support\ServiceProvider;
use App\Models\RPD;
use App\Observers\RPDObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('path.public', function() {
            return base_path('../public_html/CBP.libreria.aalmac.org');
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(){
        
    }
}
