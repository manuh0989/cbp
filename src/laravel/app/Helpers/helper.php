<?php
use Illuminate\Support\{Carbon};

function selected($value,$selectData){
	return ($value == $selectData) ? 'selected' : '';
}

function checked($value){
	return ($value) ? 'checked' : '';
}

function setFechaIsNull($fecha){
	return  (is_null($fecha))?$fecha:Carbon::parse($fecha)->format('Y-m-d');
}

function setHoraIsNull($hora){
	return (is_null($hora))?null:Carbon::parse($hora)->format('H:i');	
}