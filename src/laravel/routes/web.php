<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify'=>true]);

Route::get('/', function () {
	return view('welcome');
});
Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth','verified'])
->prefix('RPD')
->group(function(){

	Route::get('/','RPDController@index')->name('RPD.index');
	Route::get('/filtrar','RPDController@filtrar')->name('RPD.filtrar');
	Route::get('/registro','RPDController@create')->name('RPD.registro');
	Route::get('/editar/{RPD}','RPDController@edit')->name('RPD.editar')->middleware('can:update,App\User');
	Route::get('/mostrar/{RPD}','RPDController@show')->name('RPD.ver');
	Route::get('/seguimiento/{RPD}','RPDController@seguimiento')->middleware('can:seguimiento,App\User')->name('RPD.seguimiento');
	Route::get('/canalizar/{RPD}','RPDController@canalizar')->middleware('can:seguimiento,App\User')->name('RPD.canalizar');

	Route::get('/carpeta/{RPD}','RPDController@carpeta')
	->middleware('can:seguimiento,App\User')->name('RPD.carpeta');

	Route::post('/carpeta/delete/','RPDController@deleteCarpeta')
	->middleware('can:seguimiento,App\User')->name('RPD.borrarFileCarpeta');
	

	Route::put('/trash/{RPD}','RPDController@destroy')->name('RPD.trash');	
	Route::put('/editar/{RPD}','RPDController@update')->name('RPD.editar');
	
	Route::post('/registro','RPDController@store')->name('RPD.registro');
	Route::post('/seguimiento/store/{RPD}','RPDController@storeSeguimiento')->name('RPD.storeSeguimiento');
	Route::post('/canalizar/store/{RPD}','RPDController@storeCanalizacion')->name('RPD.storeCanalizacion');
	Route::post('/carpeta/store/{RPD}','RPDController@storeCarpeta')->name('RPD.storeCarpeta');
	Route::post('/condicionLocalizacion/store/{RPD}','RPDController@storeCondicionLocalizacion')
			->name('RPD.storeCondicionLocalizacion');


	Route::get('/getPDF/{RPD}', 'RPDController@getPDF')->name('pdf.reporteRPD');
	Route::get('/ftVolante/{id}', 'RPDController@ftVolante')->name('pdf.volante');


	Route::get('/exportExcel/',function(){
		return (new App\Exports\RPDExport(request()->cookie('filtros')))
		->download('RPD.xlsx',\Maatwebsite\Excel\Excel::XLSX);
	})
	
	->name('RPD.exportExcel');
});

Route::middleware(['auth','verified','can:viewAny,App\User'])->prefix('admin')
->group(function(){
	
	Route::resource('usuario', 'UserController')
	->names([
		'create'   =>'usuario.crear'
		,'index'   =>'usuario.index'
		,'edit'    =>'usuario.editar'
		,'update'  =>'usuario.update'
	]);

	Route::put('usuario/trash/{usuario}','UserController@destroy')->name('usuario.trash');

	Route::put('usuario/restore/{id}','UserController@restore')
	->where('id', '[0-9]+')
	->name('usuario.restore');

	Route::resource('catalogo', 'CatalogoController')
	->names([
		'create'   =>'catalogo.crear'
		,'store'   =>'catalogo.store'
		,'index'   =>'catalogo.index'
		,'edit'    =>'catalogo.editar'
	]);

	Route::put('catalogo','CatalogoController@updateCatalogo')->name('catalogo.updateCatalogo');


	Route::get('index','adminController@index')->name('admin.index');

});


Route::post('ajax/folioCBP','AjaxController@getFolioCBP')->name('ajax.folioCBP');
Route::post('ajax/municipios','AjaxController@municipios')->name('ajax.municipios');
Route::post('ajax/localidades','AjaxController@localidades')->name('ajax.localidades');
Route::post('ajax/calcularEdad','AjaxController@calcularEdad')->name('ajax.calcularEdad');
Route::post('ajax/catalogo','AjaxController@catalogos')->name('ajax.catalogo');
Route::put('ajax/catalogo/trash','AjaxController@trashedCatalogo')->name('ajax.trashedCatalogo');
Route::put('ajax/catalogo/restore','AjaxController@restoredCatalogo')->name('ajax.restoreCatalogo');
Route::post('ajax/seguimiento/contacto/{RPD}','AjaxController@seguimientoContacto')->name('ajax.segimientoContacto');

Route::post('ajax/estatusLocalizacion/{RPD?}','AjaxController@estatusLocalizacion')->name('ajax.estatusLocalizacion');

Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; 
});

Route::get('/config-clear', function() {
    $exitCode = Artisan::call('config:clear');
    return 'DONE'; 
});

Route::get('/migrate-fresh', function() {
    $exitCode = Artisan::call('migrate:fresh --seed');
    return 'DONE'; 
});

Route::get('/resetCBP', function() {
	//$migrate =Artisan::call('migrate:fresh --seed');
    $clear      =Artisan::call('config:clear');
    $cache      =Artisan::call('config:cache');
    $cacheClear =Artisan::call('cache:clear');
    
    return response()->json([
    	//'migrate' =>$migrate
        'clear'        =>$clear
        ,'cache'       =>$cache
        ,'cache-clear' =>$cacheClear
    ]);
});